import json
import numpy as np
import pandas as pd

#
#   Open original xls, copy everything to a text file (Notepad++ preferable)
#   
#   In the file:
#       - replace \t -> ', '
#       - replace ^ -> ['
#       - replace $ -> '],
#       - add in the beginning A = [
#       - add in the end ]


#
#	open the page, open js console
#       - paste text file content ("A = [...")
#       - paste main.js content
#       - run J=0; DO(1)
#
#
#

# when everything's finished,
# 
# in JS console do 
# >> console.log(JSON.stringify(RR))
#
#	then copy-paste that to file D:\temp\json.json (for example)
#    , use utf-8 encoding in the text editor


with open(r'D:\temp\json.json','r', encoding='utf-8')as f:
  R = json.load(f)
 
 
pd.DataFrame(np.array(R)).to_excel(r'D:\temp\test.xls', index=False)

#
#	now you can open xls file and copy column into original file
#



# %%