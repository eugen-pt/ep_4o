-- INIT:
-------------------


RR = []

wait = (ms) => {
    const start = Date.now();
    let now = start;
    while (now - start < ms) {
      now = Date.now();
    }
}

-- RUN
---------


J = 575
//J = 574


function DO(recurse=0){ 


	console.log('J = ' + J);
	if(J>A.length){
		return 'J>A.length'
	}

	if(J>10){
		// return 'J>10'
	}

	r = A[J];
	console.log(r)

	if(r.length<2){
		return 'r.length<2'
	}


	if(r[0].length==0){
		return 'r[0].length==0'
	}

	names =  r[0].split(' ')
	document.getElementById('last-name').value = names[0];
	document.getElementById('first-name').value = names[1];

	if (names.length > 2){
	document.getElementById('middle-name').value =names[2] 
	} else {
		document.getElementById('middle-name').value = ''
	}


	splits = ['--','--','--']
	if(r[1].length>0){
		splits = r[1].split('.')
	}

	document.getElementById('bdd').value = splits[0];
	document.getElementById('bdm').value = splits[1];
	document.getElementById('bdy').value = splits[2];

	search_name()


	RR[J] = '?';

	console.log('Starting timeout..')
	setTimeout(function(){
		RR[J] = '';


		result = document.getElementById('results')
		if(result){
			trs = [...document.getElementById('results').getElementsByTagName('tr')];

			if(trs.length>1){

				RR[J] = trs.slice(1)
						   .map(
					  (tr)
					  =>
					  [...tr.getElementsByTagName('td')][0].innerText 
					  + ' (' 
					  + [...tr.getElementsByTagName('td')][2].innerText
					  + ')'
						   ).join('\n')

			}
		}
		console.log('delayed thing finished, RR['+J+'] = ' + RR[J]);

		if(recurse){
			J = J + 1;
			console.log('DO=' + DO());
		}
	}, 1000);

	return 'ok';
}