## Gillespie's algorithm, implemented in Python by Paras Chopra (www.paraschopra.com)

## Implementation of Gillespie's algorithm

import random, math
import matplotlib.pyplot as plt
import time

def combination(n,r): ## implements nCr
    numerator = 1.0
    denominator = 1.0

    for i in range(1,r):
        numerator = numerator * (n-i)
        denominator = denominator * i

    return numerator/denominator

class Species:

    def __init__(self, name=None, quantity=0):
        self.name=name
        self.quantity=quantity

class Reaction:

    #def __init__(self, reactants=[], products=[], rate=0):
    def __init__(self, reactants=[], products=[],r_type=0, rate=0):
        self.reactants=reactants
        self.products=products
        self.r_type=r_type
        self.rate=rate

class Gillespie:

    def __init__(self, reactions=[], all_species=[]):
        self.reactions=reactions
        self.all_species=all_species
        ## set initial values
        self.calculate_parameters()
        #print self.reactions, 'gill react'
        
    def calculate_parameters(self):
        
        self.h_mu = []
        self.a_mu = []
        self.a_0 = 0.0


        for reaction in self.reactions:
            #print reaction
            #print reaction.reactants
            reaction_types={} ## Holds how many reactants are of the same type in 1 reaction
            ## E.g. if 2S + P --> R, then type -= {"S":2,"P":1}
            
            for reactant in reaction.reactants: 
                if not reaction_types.has_key(reactant):
                    reaction_types[reactant]=1
                else:
                    reaction_types[reactant]=reaction_types[reactant]+1
            
            h=1

            for key in reaction_types.keys():
                h=h*key.quantity
            
            #print h, 'h'
            
            self.h_mu.append(h)
            #print reaction.rate, 'reaction.rate'
            self.a_mu.append(h*reaction.rate)            
            self.a_0=self.a_0+h*reaction.rate
            #print self.a_0, 2
            
        #print self.a_0, 3

    def run(self, iterations=100, verbose=True):

        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        t=0.0

        output_first_line.append("Time")
        for one_species in self.all_species:
            output_first_line.append(one_species.name)

        
        if verbose:
            to_print=""
            for string in output_first_line:
                to_print=to_print+string+"\t"

            #print to_print

        output.append(output_first_line)

        ## Output at time=0
        output_single=[]
        output_single=[t]
        
        for one_species in self.all_species:
            output_single.append(one_species.quantity)

        
        if verbose:
            to_print=""
            for string in output_single:
                to_print=to_print+str(string)+"\t"

            #print to_print

        output.append(output_single)

        ##Start simulation
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0
        for i in range(iterations):
            #print self.a_0
            
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1/self.a_0)*math.log((1/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]

            ## ith reaction will occur

            

            for one_species in self.reactions[i].reactants:
                #one_species.quantity=one_species.quantity-1
                #print  self.reactions[i].r_type           
                if self.reactions[i].r_type == 1:                
                    one_species.quantity=one_species.quantity-1

            for one_species in self.reactions[i].products:
                one_species.quantity=one_species.quantity+1

            
            ## Output
            output_single=[t]
        
            for one_species in self.all_species:
                output_single.append(one_species.quantity)

        
            if verbose:
                to_print=""
                for string in output_single:
                    to_print=to_print+str(string)+"\t"

                #print to_print

            output.append(output_single)

            ##Recalculate parameters -- Procedure can be optimized

            self.calculate_parameters()
        
        return output
        

def HCV_cell_curve():
    random.seed()

    ## Example Reaction: Isomerization reaction from Gillespie's paper

    ## X ---c---> Z


    V = Species("Vis_w",0)
    p = Species("Proteins_w", 0)    
    pcf = Species("Cell factor + proteins_w",0)
    R = Species("RNA_w",100)
    polyp = Species("Polyprotein_w",0)
    cf = Species("Cell factor",0)
    
    Vm1 = Species("Vis_m1", 0)
    pm1 = Species("Proteins_m1", 0)    
    pcfm1 = Species("Cell factor + proteins_m1",0)
    Rm1 = Species("RNA_m1",0)
    polypm1 = Species("Polyprotein_m1",0)

    Vm2 = Species("Vis_m2", 0)
    pm2 = Species("Proteins_m2", 0)    
    pcfm2 = Species("Cell factor + proteins_m2",0)
    Rm2 = Species("RNA_m2",0)
    polypm2 = Species("Polyprotein_m2",0)
    
    Vm3 = Species("Vis_m3", 0)
    pm3 = Species("Proteins_m3",0)    
    pcfm3 = Species("Cell factor + proteins_m3",0)
    Rm3 = Species("RNA_m3",0)
    polypm3 = Species("Polyprotein_m3",0)
    
    kv = 3
    mv = 0.0578
    kout = 1.177
    mr = 0.5
    kt = 0.1
    kc = 1
    kcm = 0.42
    kp = 0.75
    kcf = 5
    mp = 1
    mpolyp = 0.3 
    kout_v_vm = 0.0002
    kout_vm_v = 0.0002
    ki = 0.12
    ki_rev = 84
    ki2 = 0
    ki_rev2 = 0 
    mcf = 1
  
    
    Polyp_transl_w = Reaction([R],[polyp],0,kt)
    Vesicle_form_w = Reaction([pcf, R],[V],1,kv)
    Vesicle_deg_w = Reaction([V],[],1,mv)
    CF_prod = Reaction([],[cf],1,kcf)
    CF_deg = Reaction([cf],[],1,mcf)
    Polyp_proc_w = Reaction([polyp],[p],1,kc)
    Replic_form_w = Reaction([cf,p],[pcf],1,kp)
    RNA_deg_w = Reaction([R],[],1,mr)
    P_deg_w = Reaction([p],[],1,mp)
    Replic_deg_w = Reaction([pcf],[],1,mv)
    Polyp_deg_w = Reaction([polyp],[],1,mpolyp)
    RNA_prod_w = Reaction([V],[R],0,kout)
    
    RNA_prod_m1_w = Reaction([V],[Rm1],0,kout_v_vm)    
    Vesicle_form_m1 = Reaction([pcfm1, Rm1],[Vm1],1,kv)
    Vesicle_deg_m1 = Reaction([Vm1],[],1,mv)
    RNA_prod_m1 = Reaction([Vm1],[Rm1],0,kout)
    RNA_deg_m1 = Reaction([Rm1],[],1,mr)
    Polyp_proc_m1 = Reaction([polypm1],[pm1],1,kcm)
    Replic_form_m1 = Reaction([cf,pm1],[pcfm1],1,kp)
    P_deg_m1 = Reaction([pm1],[],1,mp)
    Replic_deg_m1 = Reaction([pcfm1],[],1,mv)
    Polyp_transl_m1 = Reaction([Rm1],[polypm1],0,kt)
    Polyp_deg_m1 = Reaction([polypm1],[],1,mpolyp)
    RNA_prod_w_m1 = Reaction([Vm1],[R],0,kout_vm_v)
 
    RNA_prod_m2_w = Reaction([V],[Rm2],0,kout_v_vm)
    Vesicle_form_m2 = Reaction([pcfm2, Rm2],[Vm2],1,kv)
    Vesicle_deg_m2 = Reaction([Vm2],[],1,mv)
    RNA_prod_m2 = Reaction([Vm2],[Rm2],0,kout)
    RNA_deg_m2 = Reaction([Rm2],[],1,mr)
    Polyp_proc_m2 = Reaction([polypm2],[pm2],1,kcm)
    Replic_form_m2 = Reaction([cf,pm2],[pcfm2],1,kp)
    P_deg_m2 = Reaction([pm2],[],1,mp)
    Replic_deg_m2 = Reaction([pcfm2],[],1,mv)
    Polyp_transl_m2 = Reaction([Rm2],[polypm2],0,kt)
    Polyp_deg_m2 = Reaction([polypm2],[],1,mpolyp)
    RNA_prod_w_m2 = Reaction([Vm2],[R],0,kout_vm_v)

    
    RNA_prod_m3_w = Reaction([V],[Rm3],0,kout_v_vm)
    Vesicle_form_m3 = Reaction([pcfm3, Rm3],[Vm3],1,kv)
    Vesicle_deg_m3 = Reaction([Vm3],[],1,mv)
    RNA_prod_m3 = Reaction([Vm3],[Rm3],0,kout)
    RNA_deg_m3 = Reaction([Rm3],[],1,mr)
    Polyp_proc_m3 = Reaction([polypm3],[pm3],1,kcm)
    Replic_form_m3 = Reaction([cf,pm3],[pcfm3],1,kp)
    P_deg_m3 = Reaction([pm3],[],1,mp)
    Replic_deg_m3 = Reaction([pcfm3],[],1,mv)
    Polyp_transl_m3 = Reaction([Rm3],[polypm3],0,kt)
    Polyp_deg_m3 = Reaction([polypm3],[],1,mpolyp)
    RNA_prod_w_m3 = Reaction([Vm3],[R],0,kout_vm_v)

    
    example=Gillespie([Vesicle_form_w,	Vesicle_form_m1,	Vesicle_form_m2,	Vesicle_form_m3,	Vesicle_deg_w,	Vesicle_deg_m1,	Vesicle_deg_m2,	Vesicle_deg_m3,	RNA_prod_w,	RNA_prod_m1,	RNA_prod_m2,	RNA_prod_m3,	RNA_deg_w,	RNA_deg_m1,	RNA_deg_m2,	RNA_deg_m3,	Polyp_proc_w,	Polyp_proc_m1,	Polyp_proc_m2,	Polyp_proc_m3,	Replic_form_w,	Replic_form_m1,	Replic_form_m2,	Replic_form_m3,	P_deg_w,	P_deg_m1,	P_deg_m2,	P_deg_m3,	Replic_deg_w,	Replic_deg_m1,	Replic_deg_m2,	Replic_deg_m3,	Polyp_transl_w,	Polyp_transl_m1,	Polyp_transl_m2,	Polyp_transl_m3,	Polyp_deg_w,	Polyp_deg_m1,	Polyp_deg_m2,	Polyp_deg_m3,	RNA_prod_w_m1,	RNA_prod_w_m2,	RNA_prod_w_m3,	RNA_prod_m1_w,	RNA_prod_m2_w,	RNA_prod_m3_w,	CF_prod,	CF_deg],[V,	p,	pcf,	R,	polyp,	cf,	Vm1,	pm1,	pcfm1,	Rm1,	polypm1,	Vm2,	pm2,	pcfm2,	Rm2,	polypm2,	Vm3,	pm3,	pcfm3,	Rm3,	polypm3])


    
    output = example.run(20000)
    return output
    
    
    

t0 = time.clock()
random.seed()

if (1):
    curves = {}
    for i in range(1,50):
        curves[i] = HCV_cell_curve()



CH = Species("Healthy cells",0)
Cinf = Species("Infected wild cells",0)
Cinfm1 = Species("Infected mut1 cells",0)
Cinfm2 = Species("Infected mut2 cells",0)
Cinfm3 = Species("Infected mut3 cells",0)
Vir = Species("Virion wild",0)
Virm1 = Species("Virion mut1",0)
Virm2 = Species("Virion mut2",0)
Virm3 = Species("Virion mut3",0) 



#Conc = {}
#for i in range(len(curves[0][0])):
#    Conc[i] = []
#for i in range(1,len(curves[0])):
#    for j in range(len(curves[0][i])):
#        Conc[j].append(curves[0][i][j])
#        
## V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3
#plt.plot(Conc[0], Conc[4], 'g',
#         Conc[0], Conc[10], 'r',
#         Conc[0], Conc[15], 'b',
#         Conc[0], Conc[20], 'y')            
         
print time.clock() - t0, "seconds process time" 