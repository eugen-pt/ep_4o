## Gillespie's algorithm, implemented in Python by Paras Chopra (www.paraschopra.com)

## Implementation of Gillespie's algorithm

import random, math
import matplotlib.pyplot as plt
import time
import numpy


def combination(n,r): ## implements nCr
    numerator = 1.0
    denominator = 1.0

    for i in range(1,r):
        numerator = numerator * (n-i)
        denominator = denominator * i

    return numerator/denominator

class Species:

    def __init__(self, name=None, quantity=0):
        self.name=name
        self.quantity=quantity

class Reaction:

    #def __init__(self, reactants=[], products=[], rate=0):
    def __init__(self, name=None, reactants=[], products=[],r_type=0, rate=0):
        self.name=name
        self.reactants=reactants
        self.products=products
        self.r_type=r_type
        self.rate=rate

class Gillespie:

    def __init__(self, reactions=[], all_species=[]):
        self.reactions=reactions
        self.all_species=all_species
        ## set initial values
        self.calculate_parameters()
        #print self.reactions,'gill react'
        
    def calculate_parameters(self):
        
        self.h_mu = []
        self.a_mu = []
        self.a_0 = 0.0


        for reaction in self.reactions:
            #print reaction
            #print reaction.reactants
            reaction_types={} ## Holds how many reactants are of the same type in 1 reaction
            ## E.g. if 2S + P --> R, then type -= {"S":2,"P":1}
            
            for reactant in reaction.reactants: 
                if not reaction_types.has_key(reactant):
                    reaction_types[reactant]=1
                else:
                    reaction_types[reactant]=reaction_types[reactant]+1
            
            h=1

            for key in reaction_types.keys():
                h=h*key.quantity
            
            #print h,'h'
            
            self.h_mu.append(h)
            #print reaction.rate,'reaction.rate'
            
            self.a_mu.append(h*reaction.rate)            
            self.a_0=self.a_0+h*reaction.rate
            #print self.a_0, 2
            
        #print self.a_0, 3

    def run(self, Global_time=10, verbose=True):

        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        t=0.0

        output_first_line.append("Time")
        for one_species in self.all_species:
            output_first_line.append(one_species.name)

        
        if verbose:
            to_print=""
            for string in output_first_line:
                to_print=to_print+string+"\t"

            #print to_print

        output.append(output_first_line)

        ## Output at time=0
        output_single=[]
        output_single=[t]
        
        for one_species in self.all_species:
            output_single.append(one_species.quantity)

        
        if verbose:
            to_print=""
            for string in output_single:
                to_print=to_print+str(string)+"\t"

            #print to_print

        output.append(output_single)

        ##Start simulation
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0
        Infected_Curves = {}
        Inf_curves_counter = 0
        while t < Global_time:
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1/self.a_0)*math.log((1/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]

            ## ith reaction will occur
            if len(Infected_Curves) == 0:
                i = 5
            
            #print self.reactions[i].name
            if self.reactions[i].name =='Cell_infection_w' and self.all_species[5].quantity > 0:                
                curve_number = random.choice(curves_w.keys())
                if len(Infected_Curves) == 0:
                    curve_number = 67
                #print curves_w[curve_number]        
                with open(curves_w[curve_number],'r') as f:    
                    f.readline
                    infected_curve = {}
                    inf_curve_k = 0
                    for line in f.readlines():
                        if 'T' not in line:
                            line = line.replace('\n','')           
                            words = line.split(',')
                            infected_curve[inf_curve_k] = []
                            for n in words:
                                infected_curve[inf_curve_k].append(float(n))
                            inf_curve_k += 1
                
                Infected_Curves[Inf_curves_counter] = infected_curve, t, 0, curve_number
                Inf_curves_counter += 1

            if self.reactions[i].name =='Cell_infection_m1':      
                curve_number = random.choice(curves_m1.keys())
                
                with open(curves_m1[curve_number],'r') as f:    
                    f.readline
                    infected_curve = {}
                    inf_curve_k = 0
                    for line in f.readlines():
                        if'T'not in line:
                            line = line.replace('\n','')         
                            words = line.split(',')
                            infected_curve[inf_curve_k] = []
                            for n in words:
                                infected_curve[inf_curve_k].append(float(n))
                            inf_curve_k += 1
                            
                Infected_Curves[Inf_curves_counter] = infected_curve, t, 1, curve_number
                Inf_curves_counter += 1

            if self.reactions[i].name =='Cell_infection_m2':      
                curve_number = random.choice(curves_m2.keys())
                
                with open(curves_m2[curve_number],'r') as f:    
                    f.readline
                    infected_curve = {}
                    inf_curve_k = 0
                    for line in f.readlines():
                        if'T'not in line:
                            line = line.replace('\n','')           
                            words = line.split(',')
                            infected_curve[inf_curve_k] = []
                            for n in words:
                                infected_curve[inf_curve_k].append(float(n))
                            inf_curve_k += 1
                            
                Infected_Curves[Inf_curves_counter] = infected_curve, t, 2, curve_number
                Inf_curves_counter += 1

            if self.reactions[i].name =='Cell_infection_m3':      
                curve_number = random.choice(curves_m3.keys())
                
                with open(curves_m3[curve_number],'r') as f:    
                    f.readline
                    infected_curve = {}
                    inf_curve_k = 0
                    for line in f.readlines():
                        if'T'not in line:
                            line = line.replace('\n','')           
                            words = line.split(',')
                            infected_curve[inf_curve_k] = []
                            for n in words:
                                infected_curve[inf_curve_k].append(float(n))
                            inf_curve_k += 1
                            
                Infected_Curves[Inf_curves_counter] = infected_curve, t, 3, curve_number
                Inf_curves_counter += 1



            # TIME: Infected_Curves[0][0][1][0]

        #   time V  p pcf R  polyp cf  Vm1, pm1,pcfm1,Rm1, polypm1 Vm2, pm2, pcfm2, Rm2, polypm2, Vm3,pm3,pcfm3,Rm3,polypm3
        #     0  1  2  3  4    5   6    7   8     9   10    11     12   13    14    15     16     17   18   19  20    21

            vir_w = 0 
            vir_m1 = 0
            vir_m2 = 0
            vir_m3 = 0
            curves_to_del = []
            curves_to_cure = []
            for curve in Infected_Curves:
                time = t - Infected_Curves[curve][1]
                counter = min(Infected_Curves[curve][0].keys())
                max_key = max(Infected_Curves[curve][0].keys())
                if time > Infected_Curves[curve][0][max(Infected_Curves[curve][0].keys())][0]:
                    if (Infected_Curves[curve][0][max_key][1] == 0) and (Infected_Curves[curve][0][max_key][2] == 0) and  (Infected_Curves[curve][0][max_key][3] == 0) and (Infected_Curves[curve][0][max_key][4] == 0) :
                        curves_to_cure.append(curve)
                    else:
                        curves_to_del.append(curve)
                else:
                    while time > Infected_Curves[curve][0][counter][0]:
                        counter = counter + 1
                        
                vir_w = vir_w + Infected_Curves[curve][0][counter][5]
                vir_m1 = vir_m1 + Infected_Curves[curve][0][counter][6]
                vir_m2 = vir_m2 + Infected_Curves[curve][0][counter][7]
                vir_m3 = vir_m3 + Infected_Curves[curve][0][counter][8]
                Infected_Curves[curve][0][counter][5] = 0
                Infected_Curves[curve][0][counter][6] = 0
                Infected_Curves[curve][0][counter][7] = 0
                Infected_Curves[curve][0][counter][8] = 0
                
                for i3 in range(min(Infected_Curves[curve][0].keys()),counter-2):
                    del Infected_Curves[curve][0][i3]

                                       
#                print      curves_to_del, ' curves_to_del'
#                print      curves_to_cure   , 'curves_to_cure'
            for curves in curves_to_del:
                
                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
                    self.all_species[1].quantity = self.all_species[1].quantity - 1
                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
                    self.all_species[2].quantity = self.all_species[2].quantity - 1
                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
                    self.all_species[3].quantity = self.all_species[3].quantity - 1
                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
                    self.all_species[4].quantity = self.all_species[4].quantity - 1
                #print 'cur_end'    
                del Infected_Curves[curves]
                
            for curves in curves_to_cure:   
#                    if curve not in Infected_Curves:
#                        print curve ,'cur'
#                        print 'alarm!'
                self.all_species[0].quantity = self.all_species[0].quantity + 1
                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
                    self.all_species[1].quantity = self.all_species[1].quantity - 1
                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
                    self.all_species[2].quantity = self.all_species[2].quantity - 1
                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
                    self.all_species[3].quantity = self.all_species[3].quantity - 1
                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
                    self.all_species[4].quantity = self.all_species[4].quantity - 1
 
                #print 'cur_cure'
                del Infected_Curves[curves]
                
        # CH Cinf Cinfm1 Cinfm2 Cinfm3 Vir Virm1 Virm2 Virm3
        # 0   1     2      3      4     5    6     7     8     
     
            self.all_species[5].quantity = self.all_species[5].quantity + vir_w
            self.all_species[6].quantity = self.all_species[6].quantity + vir_m1
            self.all_species[7].quantity = self.all_species[7].quantity + vir_m2
            self.all_species[8].quantity = self.all_species[8].quantity + vir_m3
        
                   

            for one_species in self.reactions[i].reactants:
                #one_species.quantity=one_species.quantity-1
                #print  self.reactions[i].r_type           
                if self.reactions[i].r_type == 1:                
                    one_species.quantity=one_species.quantity-1

            for one_species in self.reactions[i].products:
                one_species.quantity=one_species.quantity+1

            
            ## Output
            output_single=[t]
        
            for one_species in self.all_species:
                output_single.append(one_species.quantity)

        
            if verbose:
                to_print=""
                for string in output_single:
                    to_print=to_print+str(string)+"\t"

               # print to_print

            output.append(output_single)

            ##Recalculate parameters -- Procedure can be optimized

            self.calculate_parameters()
        
        return output,Infected_Curves

        
t0 = time.clock()



curves_w = {}
curves_m1 = {}
curves_m2 = {}
curves_m3 = {}
k = k1 = k2 = k3 = 0

with open('D:\Work\HCV\Curves\w_list.txt','r') as f:
    for line in f.readlines():
        curves_w[k] = line.replace('\n','')
        k += 1
with open('D:\Work\HCV\Curves\m1_list.txt','r') as f:
    for line in f.readlines():
        curves_m1[k1] = line.replace('\n','')
        k1 += 1
with open('D:\Work\HCV\Curves\m2_list.txt','r') as f:
    for line in f.readlines():
        curves_m2[k2] = line.replace('\n','')
        k2 += 1
with open('D:\Work\HCV\Curves\m3_list.txt','r') as f:
    for line in f.readlines():
        curves_m3[k3] = line.replace('\n','')
        k3 += 1



organizm_curves = {}

for ik in range(0,200):        
    CH = Species("Healthy cells",1000)
    Cinf = Species("Infected wild cells",0)
    Cinfm1 = Species("Infected mut1 cells",0)
    Cinfm2 = Species("Infected mut2 cells",0)
    Cinfm3 = Species("Infected mut3 cells",0)
    Vir = Species("Virion wild", 1)
    Virm1 = Species("Virion mut1",0)
    Virm2 = Species("Virion mut2",0)
    Virm3 = Species("Virion mut3",0) 
    
    # CH Cinf Cinfm1 Cinfm2 Cinfm3 Vir Virm1 Virm2 Virm3
    # 0   1     2      3      4     5    6     7     8
    
    kinf = 3
    mch = 0.0005
    mvir = 0.1
    pch = 0.7
    
    Cell_infection_w = Reaction('Cell_infection_w', [Vir, CH],[Cinf],1, kinf)
    Cell_infection_m1 = Reaction('Cell_infection_m1', [Virm1, CH],[Cinfm1],1, kinf)
    Cell_infection_m2 = Reaction('Cell_infection_m2', [Virm2, CH],[Cinfm2],1, kinf)
    Cell_infection_m3 = Reaction('Cell_infection_m3', [Virm3, CH],[Cinfm3],1, kinf)
    CH_formation = Reaction('CH_formation', [],[CH],1, pch)
    
    CH_degradation = Reaction('CH_degradation', [CH],[],1, mch)
    #Cinf_degradation = Reaction('Cinf_degradation', [Cinf],[],1, mcinf)
    #Cinfm1_degradation = Reaction('Cinfm1_degradation', [Cinfm1],[],1, mcinf)
    #Cinfm2_degradation = Reaction('Cinfm2_degradation', [Cinfm2],[],1, mcinf)
    #Cinfm3_degradation = Reaction('Cinfm3_degradation', [Cinfm3],[],1, mcinf)
    Vir_degradation = Reaction('Vir_degradation', [Vir],[],1, mvir)
    Virm1_degradation = Reaction('Virm1_degradation', [Virm1],[],1, mvir)
    Virm2_degradation = Reaction('Virm2_degradation', [Virm2],[],1, mvir)
    Virm3_degradation = Reaction('Virm3_degradation', [Virm3],[],1, mvir)
    
    ex = Gillespie([CH_degradation, Virm3_degradation,Virm2_degradation,Virm1_degradation,Vir_degradation,Cell_infection_w,Cell_infection_m1, Cell_infection_m2, Cell_infection_m3,CH_formation],[CH, Cinf, Cinfm1, Cinfm2, Cinfm3, Vir, Virm1, Virm2, Virm3])
    output, curves = ex.run(2000)
    
    Conc = {}
    for i in range(len(output[0])):
        Conc[i] = []
    for j in range(1,len(output)):
        for i in range(len(output[j])):
            Conc[i].append(output[j][i])
    #     V  p pcf R polyp cf Vm1, pm1,pcfm1,Rm1, polypm1 Vm2, pm2, pcfm2, Rm2, polypm2, Vm3,pm3,pcfm3,Rm3,polypm3 Vir Virm1 Virm2 Virm3 Viro Virm1o Virm2o Virm3o
    #     0  1  2  3  4    5   6    7   8     9     10    11   12    13    14     15     16  17   18   19   20     21   22    23    24   25    26      27    28
    plt.plot( Conc[0], Conc[2], 'r',
              Conc[0], Conc[6], 'b')     
    
    file_of_stst = open('D:\Work\HCV\stst_outputs.txt','a') 
    
    for curve in curves:
        time_for_stat =  curves[curve][0][min(curves[curve][0].keys())][0]
        temp = curves_w[curves[curve][3]].split('Curves')
        file_name = temp[0] + 'Curves_full' + temp[1]
        with open(file_name,'r') as f:    
            line = f.readline()
            while str(time_for_stat) not in line:
                line = f.readline()
                if not line: break
      
            file_of_stst.writelines(line)
    file_of_stst.close()    
    organizm_curves[ik]  = Conc

points = []
values = []
grid_x = numpy.linspace(0,1500,num = 1500)

summ = numpy.zeros(len(grid_x))

for curve in organizm_curves:
    Y = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][2] )
    for i in range(len(Y)):
        summ[i] = summ[i] + Y[i]
summ = summ/len(organizm_curves)

plt.plot(grid_x,summ)

print time.clock() - t0, "seconds process time" 

