%module ep_Gillespie2_cL
%{
#include "ep_Gillespie2_cL.h"
%}
%include "std_vector.i"
namespace std {
  /* On a side note, the names VecDouble and VecVecdouble can be changed, but the order of first the inner vector matters !*/
  %template(VecFloat) vector<float>;
  %template(VecVecfloat) vector< vector<float> >;
  %template(VecInt) vector<int>;
  %template(VecVecint) vector< vector<int> >;
  %template(VecBool) vector<bool>;
}

%include "ep_Gillespie2_cL.h"