//
// ep
// 





/*

*/

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <ctime>
#include <string>
#include <cstring>
	
#include <vector>
#include <stdarg.h>     /* va_list, va_start, va_arg, va_end */
//#include <iostream>

#include <math.h>       /* log */

#include <unistd.h>   /* getpid */

using namespace std;

struct Species{
  char* name;
  float quantity;
};

Species* makeSpecies(char* _name,float _q){
  Species* a = new Species;//(Species*)malloc(sizeof(Species));
  a->name = _name;
  a->quantity=_q;
  return a;
}

struct Reaction{
  char* name;
  vector<Species*> reactants;
  vector<Species*> products;
  int r_type;
  float rate;
};

Reaction* makeReaction(char* _name,vector<Species*> _reactants,vector<Species*> _products,int _r_type,float _rate){
  Reaction* R = new Reaction;//(Reaction*)malloc(sizeof(Reaction));

  R->name = _name;
  R->reactants = _reactants;
  R->products = _products;
  R->r_type = _r_type;
  R->rate = _rate;

  return R;
}

float sum(int n,float* a){
  float r = 0;
  for(int j=0;j<n;j++)
    r+=a[j];
  return r;
}

int GC=0;


void outSpecies(Species* s){
//  printf("Species of name:\n");
  printf("species of name %s, quantity %i\n",s->name,s->quantity);
}

void outManyArr(int n, Species ** ss ){

  for(int j=0;j<n;j++)
    outSpecies(ss[j]);

}
void outManyVec(vector<Species*> sv ){

  for(int j=0;j<sv.size();j++)
    outSpecies(sv[j]);

}

void outMany(int n, ... ){
  va_list vl;
  va_start(vl,n);
  Species* val;
  for(int j=0;j<n;j++){
    val=va_arg(vl,Species*);
    outSpecies(val);
  }

  va_end(vl);
}

vector<Species*> makeSpeciesVec(int n,...){
  vector<Species*> r(n);
  
  va_list vl;
  va_start(vl,n);
  Species* val;
  for(int j=0;j<n;j++){
    val=va_arg(vl,Species*);
    r[j]=val;
  }

  va_end(vl);

  return r;
}

vector<Reaction*> makeReactionsVec(int n,...){
  vector<Reaction*> r(n);
  
  va_list vl;
  va_start(vl,n);
  Reaction* val;
  for(int j=0;j<n;j++){
    val=va_arg(vl,Reaction*);
    r[j]=val;
  }

  va_end(vl);

  return r;
}

void outReaction(Reaction* r){
  printf("_____\nReaction [%s]\n r_type: %i\n rate: %f\nReactants:\n",r->name,r->r_type,r->rate);
  outManyVec(r->reactants);
  printf("Products:\n");
  outManyVec(r->products);
}



class Gillespie_2{
  public:
    int Nreactions;
    int Nspecies;
    vector<Species*> all_species;
    vector<Reaction*> reactions;
    
    float* a_mu;
    float a_0;

    void out(){
      printf("*******************\nGillespie_2\n");
      for(int j=0;j<Nreactions;j++){
        outReaction(reactions[j]);      
      }
      printf("****\nall_species:\n");
      outManyVec(all_species);
      printf("*******************\n\n");
    }
    void sout(){
//      for(int j=0;j<Nspecies;j++){
//        printf("%s : %i | ",all_species[j]->name,all_species[j]->quantity);
//      }
      for(int j=0;j<Nspecies;j++){
        printf("%6f | ",all_species[j]->quantity);
      }
      printf("\n");
    }
    void sout_bin(){
      float* temp = new int[Nspecies];
      for(int j=0;j<Nspecies;j++)
        temp[j] = all_species[j]->quantity;
      size_t n = fwrite((void*)temp, 1, Nspecies*sizeof(float), stdout);
    }
    int findSpecies(char* _name){
      int r=-1;
      for(int j=0;j<Nspecies;j++)
        if(strcmp(all_species[j]->name,_name)){
          r=j;
          break;
        }
      return r;
    }
    void addSpecies(char* name,int q){
      int ix = findSpecies(name);
      if(ix>=0){
        all_species[ix]->quantity=q;
        return;
      }
      Species* S = makeSpecies(name,q);
      all_species.push_back(S);
      Nspecies++;
    }
    void addReaction(vector<char*>reactant_names,vector<char*>product_names,int _r_type,int _rate){
      
    }
    void calculate_parameters(){
//      printf("calculate_parameters\n");
//      self.a_mu = [0.0]*self.Nreactions
      int reactionj=0;
      int reactantj;
      float h;
      Reaction* reaction;
      while(reactionj < Nreactions){
        reaction = reactions[reactionj];
        h=1.0;
        for(reactantj=0;reactantj<reaction->reactants.size();reactantj++)
          h=h*reaction->reactants[reactantj]->quantity;
            
        a_mu[reactionj] = h*reaction->rate;
        reactionj = reactionj+1;
      }
      a_0 = sum(Nreactions,a_mu);

      GC++; //#EP
//      printf("calculate_parameters finished\n");
//      sout();
    }
    Gillespie_2(){Nreactions=0;Nspecies=0;}
    Gillespie_2(vector<Reaction*> _reactions, vector<Species*> _all_species){
      Nreactions=_reactions.size();
      Nspecies = _all_species.size();
      all_species=_all_species;
      reactions = _reactions;

      a_mu = new float[Nreactions];
//      out();
      // set initial values
      calculate_parameters();
//      out();
    }
//    ~Gillespie
    void run_2(float Global_time, int verbose){
      if(verbose>1){
        printf("Time  |");
        for(int j=0;j<Nspecies;j++){
          printf("%6s | ",all_species[j]->name);
        }
        printf("\n");
      }else
      if(verbose<0){
        
        fwrite((void*)&Nspecies,1,sizeof(Nspecies),stdout);
      }
//      printf("run_2\n");

    

//        ##Prepare the output
//        output=[]
//        output_first_line=[]
//        output_single=[]
      float t=0.0;

//        output_first_line.append("Time")
//        for one_species in self.all_species:
//            output_first_line.append(one_species.name)

        
//        if verbose:
//            to_print=""
//            for string in output_first_line:
//                to_print=to_print+string+"\t"
//
//        output_single=[]
//        output_single=[t]
//        
//        for one_species in self.all_species:
//            output_single.append(one_species.quantity)
//
//        
//        if verbose:
//            to_print=""
//            for string in output_single:
//                to_print=to_print+str(string)+"\t"
//
//            #print to_print
//
//        output.append(output_single)

//        ##Start simulation
      float r1=0.0;
      float r2=0.0;
      float tau=0.0;
      
      float sum_of_as;
      int i;


      while (t < Global_time){
//            #print self.a_0
            
//        printf("run_2 inner_cycle\n");
        if (a_0 <= 0)
          break; //## All reactants got exausted
            
        r1=rand() / ((float)RAND_MAX+1.0);
        r2=rand() / ((float)RAND_MAX+1.0);

//        printf("r1=%f r2=%f\n",r1,r2);
//            #print r1,r2
        tau=(1.0/a_0)*log(1.0/r1);
        t=t+tau;
//            # see which reaction will occur, using r2

        sum_of_as=0.0;
            
//            # Number of reaction to happen
        i=-1;

//        printf("run_2 inner_cycle summing a_mus\n");
        while(sum_of_as<r2*a_0){
          i=i+1;
          sum_of_as=sum_of_as+a_mu[i];
        }

//        printf("run_2 inner_cycle i=%i (total reactions: %i), reaction name=%s\n",i,Nreactions,reactions[i]->name);
//            ## ith reaction will occur

//        printf("run_2 inner_cycle zeroing qs\n");
        if(all_species.size()>28){
          all_species[28]->quantity = 0 ;
          all_species[27]->quantity = 0 ;
          all_species[26]->quantity = 0 ;
          all_species[25]->quantity = 0 ;           
        }
  
                  
                   
//            #one_species.quantity=one_species.quantity-1
//            #print  self.reactions[i].r_type  
//        printf("run_2 inner_cycle reactions of r_type==1\n");
//        out();
        if (reactions[i]->r_type == 1)               
          for(int j = 0 ; j < reactions[i]->reactants.size() ; j++)
            reactions[i]->reactants[j]->quantity-=1 ;
//        out();
//             one_species in self.reactions[i].reactants:
//                one_species.quantity=one_species.quantity-1

//        printf("run_2 inner_cycle reactions add products\n");
//        outReaction(reactions[i]);
        for(int j = 0 ; j < reactions[i]->products.size() ; j++){
//          printf("run_2 inner_cycle reactions add products j=%i\n",j);
//          outSpecies(reactions[i]->products[j]);
//          printf("run_2 inner_cycle reactions add products product species name = %s\n",reactions[i]->products[j]->name);
//          printf("run_2 inner_cycle reactions add products product species quantity = %i\n",reactions[i]->products[j]->quantity);
          reactions[i]->products[j]->quantity+=1 ;
        }
//        for one_species in self.reactions[i].products:
//            one_species.quantity=one_species.quantity+1

            
//            ## Output
//            output_single=[t]
//        
//            for one_species in self.all_species:
//                output_single.append(one_species.quantity)

        if((all_species.size()>19) and (all_species[3]->quantity == 0) and (all_species[9]->quantity == 0) and (all_species[14]->quantity == 0 )and (all_species[19]->quantity == 0 )){
          t = Global_time;
              
          if(verbose){
//              to_print=""
//              for string in output_single:
//                  to_print=to_print+str(string)+"\t"
//
//              #print to_print
          }
//          output.append(output_single)

//          ##Recalculate parameters -- Procedure can be optimized

        }

        if(verbose>0){
          
          printf("t=%f",t)    ;
          if(verbose>1){
            sout();      
          }else
            printf("\n");
        }else
        if(verbose<0){
          size_t n = fwrite((void*)&t, 1, sizeof(t), stdout);
          sout_bin();
        }
      
        calculate_parameters();
      }
//      return output
    }
};


//vector<char*> makeNamesVec(...)
unsigned long mix(unsigned long a, unsigned long b, unsigned long c)
{
    a=a-b;  a=a-c;  a=a^(c >> 13);
    b=b-c;  b=b-a;  b=b^(a << 8);
    c=c-a;  c=c-b;  c=c^(b >> 13);
    a=a-b;  a=a-c;  a=a^(c >> 12);
    b=b-c;  b=b-a;  b=b^(a << 16);
    c=c-a;  c=c-b;  c=c^(b >> 5);
    a=a-b;  a=a-c;  a=a^(c >> 3);
    b=b-c;  b=b-a;  b=b^(a << 10);
    c=c-a;  c=c-b;  c=c^(b >> 15);
    return c;
}

int main(int argc,char *argv[] ){

//  srand(0);
//  srand(time( 0 ) );
  unsigned long seed = mix(clock(), time(NULL), getpid());
  srand(seed);
  if(0){
    Species* D = makeSpecies("myname",12);
    Species* E = makeSpecies("AA<",1);
  
    printf("Simple:\n");
    outSpecies(D);
    outSpecies(E);
  
    printf("As array:\n");
    Species* Arr[] = {D,E};
    outManyArr(2,Arr);
    printf("As variable number of arguments:\n");
    outMany(2,D,E);
    
    printf("Vectors simple:\n");
    vector<Species*> sv(2);
    sv[0] = D;
    sv[1] = E;
    outManyVec(sv);
    printf("Vectors OTG:\n");
    outManyVec(makeSpeciesVec(2,D,E));
  
    Reaction* R1 = makeReaction("reaction1",makeSpeciesVec(2,D,E),makeSpeciesVec(1,E),1,4.3245/1000.0);
  
    outReaction(R1);
    
  
    printf("Creating Gillespie2..\n");
    Gillespie_2 G = Gillespie_2(makeReactionsVec(1,R1),makeSpeciesVec(2,D,E));
    printf("Running Gillespie2..\n");
    
    int NRT = 1000;
    
    float mean_dGC = 0.0;
    for(int j=0;j<NRT;j++){
      GC=0;
  //    G = Gillespie_2(makeReactionsVec(1,R1),makeSpeciesVec(2,D,E));
      if(1){
        D = makeSpecies("myname",12);
        E = makeSpecies("AA<",1);
        R1 = makeReaction("reaction1",makeSpeciesVec(2,D,E),makeSpeciesVec(1,E),1,4.3245/1000.0);
        G = Gillespie_2(makeReactionsVec(1,R1),makeSpeciesVec(2,D,E));
      }else{
        G = Gillespie_2();
        G.addSpecies("myname",12);
        G.addSpecies("AA<",1);
//        G.addReaction
      }
      G.run_2(1000,0);
  //    printf("GC=%i\n",GC);
      mean_dGC+=GC*1.0/NRT;
    }
    mean_dGC=mean_dGC;
    printf("mean_dGC=%f\n",mean_dGC);
  //  printf("asd");
  //  cout<<"aa";
  }else{
    float HCV_Pars[] =  { 0.363    , 0.05         , 4          , 1      , 0.0005    , 0.25   };
    int N_HCV_Pars = 6;
    float X0[] = {0,0,0,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int N_X0 = 29;

    float _time_of_death = 400; // if >=0 that it's the maximum limit. if <0 - it's TOD's fixed value

//    printf("argc=%i\n",argc);
    if(argc>=N_HCV_Pars+1){
      for(int j=0;j<N_HCV_Pars;j++){
        HCV_Pars[j] = atof(argv[j+1]);
      }
  
      if(argc>=N_HCV_Pars+N_X0+1){
        for(int j = 0;j<N_X0;j++){
          X0[j] = atoi(argv[j+1+N_HCV_Pars]);
        }
        if(argc>N_HCV_Pars+N_X0+1){
          _time_of_death = atof(argv[N_HCV_Pars+N_X0+1]);
        } 
      
      }
    }
    for(int j=0;j<argc;j++){
//      printf("%i: %s\n",j,argv[j]);    
    }
//    printf("_time_of_death = %f\n\n",_time_of_death);
//    if(_time_of_death>=0)
//      printf("_time_of_death>=0\n");
//    else
//      printf("_time_of_death<0\n");
//    return 0;
    float degrRate = HCV_Pars[0];
    float virFormation = HCV_Pars[1];
    float cellFactor= HCV_Pars[2];
    float virOut = HCV_Pars[3];

    Species* V = makeSpecies("Vis_w",X0[0]);
    Species* p = makeSpecies("Proteins_w", X0[1])  ;  
    Species* pcf = makeSpecies("Cell factor + proteins_w",X0[2]);
    Species* R = makeSpecies("RNA_w",X0[3]);
    Species* polyp = makeSpecies("Polyprotein_w",X0[4]);
    Species* cf = makeSpecies("Cell factor",X0[5]);
        
    Species* Vm1 = makeSpecies("Vis_m1", X0[6]);
    Species* pm1 = makeSpecies("Proteins_m1", X0[7])    ;
    Species* pcfm1 = makeSpecies("Cell factor + proteins_m1",X0[8]);
    Species* Rm1 = makeSpecies("RNA_m1",X0[9]);
    Species* polypm1 = makeSpecies("Polyprotein_m1",X0[10]);

    Species* Vm2 = makeSpecies("Vis_m2", X0[11]);
    Species* pm2 = makeSpecies("Proteins_m2", X0[12])    ;
    Species* pcfm2 = makeSpecies("Cell factor + proteins_m2",X0[13]);
    Species* Rm2 = makeSpecies("RNA_m2",X0[14]);
    Species* polypm2 = makeSpecies("Polyprotein_m2",X0[15]);
    
    Species* Vm3 = makeSpecies("Vis_m3", X0[16]);
    Species* pm3 = makeSpecies("Proteins_m3",X0[17])    ;
    Species* pcfm3 = makeSpecies("Cell factor + proteins_m3",X0[18]);
    Species* Rm3 = makeSpecies("RNA_m3",X0[19]);
    Species* polypm3 = makeSpecies("Polyprotein_m3",X0[20]);
    
    Species* Vir = makeSpecies("Virion_wild",X0[21])   ;
    Species* Virm1 = makeSpecies("Virion_m1",X0[22]) ;
    Species* Virm2 = makeSpecies("Virion_m2",X0[23]) ;
    Species* Virm3 = makeSpecies("Virion_m3",X0[24]) ;
    
    Species* Vir_out = makeSpecies("Virion_wild_out",X0[25])   ;
    Species* Virm1_out = makeSpecies("Virion_m1_out",X0[26]) ;
    Species* Virm2_out = makeSpecies("Virion_m2_out",X0[27]) ;
    Species* Virm3_out = makeSpecies("Virion_m3_out",X0[28]) ;
    
    float kv = 3;
    float mv = 0.0578;
    float kout = 1.177;
//    float mr = 0.5;//  Degradation rate
//    float mr = 0.363;// Degradation rate
//    float mr = 0.2;// Degradation rate
    float mr = degrRate;
    float kt = 0.1;
    float kc = 1;
    float kcm = 0.42;
    float kp = 0.75;
//    float kcf = 4;// # Cell Factor
    float kcf = cellFactor;
    float mp = 1;
    float mpolyp = 0.3 ;
    float kout_v_vm = 0.0002;
    float kout_vm_v = 0.0002;
//    float vf = 0.05;// # Virion Formation
//    float vf = 0.5;// # Virion Formation
    float vf = virFormation;
    float mcf = 1;
    float mvir = 0;
//    float vout = 1;
    float vout = virOut;


//  with open('HCV_cell.py','r') as f:
//    R = f.read()
//  for s in re.findall("([a-zA-Z_0-9]+) = Reaction\(\'{1}[^\[]+\[(.*?)\],\[(.*?)\],(0|1),(.+?)\)",R):
//    print('Reaction* '+s[0]+' = makeReaction("'+s[0]+'",makeSpeciesVec('+str(min(len(s[1]),len(s[1].split(','))))+','+s[1]+'),makeSpeciesVec('+str(min(len(s[2]),len(s[2].split(','))))+','+s[2]+'),'+s[3]+','+s[4]+');')

    Reaction* Polyp_transl_w = makeReaction("Polyp_transl_w",makeSpeciesVec(1,R),makeSpeciesVec(1,polyp),0,kt);
    Reaction* Vesicle_form_w = makeReaction("Vesicle_form_w",makeSpeciesVec(2,pcf, R),makeSpeciesVec(1,V),1,kv);
    Reaction* Vesicle_deg_w = makeReaction("Vesicle_deg_w",makeSpeciesVec(1,V),makeSpeciesVec(0),1,mv);
    Reaction* CF_prod = makeReaction("CF_prod",makeSpeciesVec(0),makeSpeciesVec(1,cf),1,kcf);
    Reaction* CF_deg = makeReaction("CF_deg",makeSpeciesVec(1,cf),makeSpeciesVec(0),1,mcf);
    Reaction* Polyp_proc_w = makeReaction("Polyp_proc_w",makeSpeciesVec(1,polyp),makeSpeciesVec(1,p),1,kc);
    Reaction* Replic_form_w = makeReaction("Replic_form_w",makeSpeciesVec(2,cf,p),makeSpeciesVec(1,pcf),1,kp);
    Reaction* RNA_deg_w = makeReaction("RNA_deg_w",makeSpeciesVec(1,R),makeSpeciesVec(0),1,mr);
    Reaction* P_deg_w = makeReaction("P_deg_w",makeSpeciesVec(1,p),makeSpeciesVec(0),1,mp);
    Reaction* Replic_deg_w = makeReaction("Replic_deg_w",makeSpeciesVec(1,pcf),makeSpeciesVec(0),1,mv);
    Reaction* Polyp_deg_w = makeReaction("Polyp_deg_w",makeSpeciesVec(1,polyp),makeSpeciesVec(0),1,mpolyp);
    Reaction* RNA_prod_w = makeReaction("RNA_prod_w",makeSpeciesVec(1,V),makeSpeciesVec(1,R),0,kout);
    Reaction* RNA_prod_m1_w = makeReaction("RNA_prod_m1_w",makeSpeciesVec(1,V),makeSpeciesVec(1,Rm1),0,kout_v_vm);
    Reaction* Vesicle_form_m1 = makeReaction("Vesicle_form_m1",makeSpeciesVec(2,pcfm1, Rm1),makeSpeciesVec(1,Vm1),1,kv);
    Reaction* Vesicle_deg_m1 = makeReaction("Vesicle_deg_m1",makeSpeciesVec(1,Vm1),makeSpeciesVec(0),1,mv);
    Reaction* RNA_prod_m1 = makeReaction("RNA_prod_m1",makeSpeciesVec(1,Vm1),makeSpeciesVec(1,Rm1),0,kout);
    Reaction* RNA_deg_m1 = makeReaction("RNA_deg_m1",makeSpeciesVec(1,Rm1),makeSpeciesVec(0),1,mr);
    Reaction* Polyp_proc_m1 = makeReaction("Polyp_proc_m1",makeSpeciesVec(1,polypm1),makeSpeciesVec(1,pm1),1,kcm);
    Reaction* Replic_form_m1 = makeReaction("Replic_form_m1",makeSpeciesVec(2,cf,pm1),makeSpeciesVec(1,pcfm1),1,kp);
    Reaction* P_deg_m1 = makeReaction("P_deg_m1",makeSpeciesVec(1,pm1),makeSpeciesVec(0),1,mp);
    Reaction* Replic_deg_m1 = makeReaction("Replic_deg_m1",makeSpeciesVec(1,pcfm1),makeSpeciesVec(0),1,mv);
    Reaction* Polyp_transl_m1 = makeReaction("Polyp_transl_m1",makeSpeciesVec(1,Rm1),makeSpeciesVec(1,polypm1),0,kt);
    Reaction* Polyp_deg_m1 = makeReaction("Polyp_deg_m1",makeSpeciesVec(1,polypm1),makeSpeciesVec(0),1,mpolyp);
    Reaction* RNA_prod_w_m1 = makeReaction("RNA_prod_w_m1",makeSpeciesVec(1,Vm1),makeSpeciesVec(1,R),0,kout_vm_v);
    Reaction* RNA_prod_m2_w = makeReaction("RNA_prod_m2_w",makeSpeciesVec(1,V),makeSpeciesVec(1,Rm2),0,kout_v_vm);
    Reaction* Vesicle_form_m2 = makeReaction("Vesicle_form_m2",makeSpeciesVec(2,pcfm2, Rm2),makeSpeciesVec(1,Vm2),1,kv);
    Reaction* Vesicle_deg_m2 = makeReaction("Vesicle_deg_m2",makeSpeciesVec(1,Vm2),makeSpeciesVec(0),1,mv);
    Reaction* RNA_prod_m2 = makeReaction("RNA_prod_m2",makeSpeciesVec(1,Vm2),makeSpeciesVec(1,Rm2),0,kout);
    Reaction* RNA_deg_m2 = makeReaction("RNA_deg_m2",makeSpeciesVec(1,Rm2),makeSpeciesVec(0),1,mr);
    Reaction* Polyp_proc_m2 = makeReaction("Polyp_proc_m2",makeSpeciesVec(1,polypm2),makeSpeciesVec(1,pm2),1,kcm);
    Reaction* Replic_form_m2 = makeReaction("Replic_form_m2",makeSpeciesVec(2,cf,pm2),makeSpeciesVec(1,pcfm2),1,kp);
    Reaction* P_deg_m2 = makeReaction("P_deg_m2",makeSpeciesVec(1,pm2),makeSpeciesVec(0),1,mp);
    Reaction* Replic_deg_m2 = makeReaction("Replic_deg_m2",makeSpeciesVec(1,pcfm2),makeSpeciesVec(0),1,mv);
    Reaction* Polyp_transl_m2 = makeReaction("Polyp_transl_m2",makeSpeciesVec(1,Rm2),makeSpeciesVec(1,polypm2),0,kt);
    Reaction* Polyp_deg_m2 = makeReaction("Polyp_deg_m2",makeSpeciesVec(1,polypm2),makeSpeciesVec(0),1,mpolyp);
    Reaction* RNA_prod_w_m2 = makeReaction("RNA_prod_w_m2",makeSpeciesVec(1,Vm2),makeSpeciesVec(1,R),0,kout_vm_v);
    Reaction* RNA_prod_m3_w = makeReaction("RNA_prod_m3_w",makeSpeciesVec(1,V),makeSpeciesVec(1,Rm3),0,kout_v_vm);
    Reaction* Vesicle_form_m3 = makeReaction("Vesicle_form_m3",makeSpeciesVec(2,pcfm3, Rm3),makeSpeciesVec(1,Vm3),1,kv);
    Reaction* Vesicle_deg_m3 = makeReaction("Vesicle_deg_m3",makeSpeciesVec(1,Vm3),makeSpeciesVec(0),1,mv);
    Reaction* RNA_prod_m3 = makeReaction("RNA_prod_m3",makeSpeciesVec(1,Vm3),makeSpeciesVec(1,Rm3),0,kout);
    Reaction* RNA_deg_m3 = makeReaction("RNA_deg_m3",makeSpeciesVec(1,Rm3),makeSpeciesVec(0),1,mr);
    Reaction* Polyp_proc_m3 = makeReaction("Polyp_proc_m3",makeSpeciesVec(1,polypm3),makeSpeciesVec(1,pm3),1,kcm);
    Reaction* Replic_form_m3 = makeReaction("Replic_form_m3",makeSpeciesVec(2,cf,pm3),makeSpeciesVec(1,pcfm3),1,kp);
    Reaction* P_deg_m3 = makeReaction("P_deg_m3",makeSpeciesVec(1,pm3),makeSpeciesVec(0),1,mp);
    Reaction* Replic_deg_m3 = makeReaction("Replic_deg_m3",makeSpeciesVec(1,pcfm3),makeSpeciesVec(0),1,mv);
    Reaction* Polyp_transl_m3 = makeReaction("Polyp_transl_m3",makeSpeciesVec(1,Rm3),makeSpeciesVec(1,polypm3),0,kt);
    Reaction* Polyp_deg_m3 = makeReaction("Polyp_deg_m3",makeSpeciesVec(1,polypm3),makeSpeciesVec(0),1,mpolyp);
    Reaction* RNA_prod_w_m3 = makeReaction("RNA_prod_w_m3",makeSpeciesVec(1,Vm3),makeSpeciesVec(1,R),0,kout_vm_v);
    Reaction* Virion_form_w = makeReaction("Virion_form_w",makeSpeciesVec(1,R),makeSpeciesVec(1,Vir),1,vf);
    Reaction* Virion_form_m1 = makeReaction("Virion_form_m1",makeSpeciesVec(1,Rm1),makeSpeciesVec(1,Virm1),1,vf);
    Reaction* Virion_form_m2 = makeReaction("Virion_form_m2",makeSpeciesVec(1,Rm2),makeSpeciesVec(1,Virm2),1,vf);
    Reaction* Virion_form_m3 = makeReaction("Virion_form_m3",makeSpeciesVec(1,Rm3),makeSpeciesVec(1,Virm3),1,vf);
    Reaction* Virion_deg_w = makeReaction("Virion_deg_w",makeSpeciesVec(1,Vir),makeSpeciesVec(0),1,mvir);
    Reaction* Virion_deg_m1 = makeReaction("Virion_deg_m1",makeSpeciesVec(1,Virm1),makeSpeciesVec(0),1,mvir);
    Reaction* Virion_deg_m2 = makeReaction("Virion_deg_m2",makeSpeciesVec(1,Virm2),makeSpeciesVec(0),1,mvir);
    Reaction* Virion_deg_m3 = makeReaction("Virion_deg_m3",makeSpeciesVec(1,Virm3),makeSpeciesVec(0),1,mvir);
    Reaction* Virion_out_w = makeReaction("Virion_out_w",makeSpeciesVec(1,Vir),makeSpeciesVec(1,Vir_out),1,vout);
    Reaction* Virion_out_m1 = makeReaction("Virion_out_m1",makeSpeciesVec(1,Virm1),makeSpeciesVec(1,Virm1_out),1,vout);
    Reaction* Virion_out_m2 = makeReaction("Virion_out_m2",makeSpeciesVec(1,Virm2),makeSpeciesVec(1,Virm2_out),1,vout);
    Reaction* Virion_out_m3 = makeReaction("Virion_out_m3",makeSpeciesVec(1,Virm3),makeSpeciesVec(1,Virm3_out),1,vout);

    
    Gillespie_2 example_2=Gillespie_2(makeReactionsVec(60,Virion_out_w,Virion_out_m1,Virion_out_m2,Virion_out_m3,Virion_form_w, Virion_form_m1, Virion_form_m2, Virion_form_m3,Vesicle_form_w,	Vesicle_form_m1,	Vesicle_form_m2,	Vesicle_form_m3,	Vesicle_deg_w,	Vesicle_deg_m1,	Vesicle_deg_m2,	Vesicle_deg_m3,	RNA_prod_w,	RNA_prod_m1,	RNA_prod_m2,	RNA_prod_m3,	RNA_deg_w,	RNA_deg_m1,	RNA_deg_m2,	RNA_deg_m3,	Polyp_proc_w,	Polyp_proc_m1,	Polyp_proc_m2,	Polyp_proc_m3,	Replic_form_w,	Replic_form_m1,	Replic_form_m2,	Replic_form_m3,	P_deg_w,	P_deg_m1,	P_deg_m2,	P_deg_m3,	Replic_deg_w,	Replic_deg_m1,	Replic_deg_m2,	Replic_deg_m3,	Polyp_transl_w,	Polyp_transl_m1,	Polyp_transl_m2,	Polyp_transl_m3,	Polyp_deg_w,	Polyp_deg_m1,	Polyp_deg_m2,	Polyp_deg_m3,	RNA_prod_w_m1,	RNA_prod_w_m2,	RNA_prod_w_m3,	RNA_prod_m1_w,	RNA_prod_m2_w,	RNA_prod_m3_w,	CF_prod,	CF_deg, Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3),  makeSpeciesVec(29,V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3,Vir,Virm1,Virm2,Virm3,Vir_out,Virm1_out,Virm2_out,Virm3_out));

    
    float time_of_death = rand()/(1.0+(float)RAND_MAX);
    time_of_death = (1/0.005)*log(1/time_of_death);

    if(_time_of_death>=0){
      if (time_of_death > _time_of_death)
          time_of_death = _time_of_death;
    }else
      time_of_death = -_time_of_death;
        
//    printf("_time_of_death=%f\n",_time_of_death);
//    printf("time_of_death=%f\n",time_of_death);
//    return 0;
    example_2.run_2(time_of_death,-1);
  }
  return 0;

  

}



//eof