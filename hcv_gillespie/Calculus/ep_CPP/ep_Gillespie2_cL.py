# This file was automatically generated by SWIG (http://www.swig.org).
# Version 2.0.11
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info
if version_info >= (2,6,0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_ep_Gillespie2_cL', [dirname(__file__)])
        except ImportError:
            import _ep_Gillespie2_cL
            return _ep_Gillespie2_cL
        if fp is not None:
            try:
                _mod = imp.load_module('_ep_Gillespie2_cL', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _ep_Gillespie2_cL = swig_import_helper()
    del swig_import_helper
else:
    import _ep_Gillespie2_cL
del version_info
try:
    _swig_property = property
except NameError:
    pass # Python < 2.2 doesn't have 'property'.
def _swig_setattr_nondynamic(self,class_type,name,value,static=1):
    if (name == "thisown"): return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    if (not static):
        self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)

def _swig_setattr(self,class_type,name,value):
    return _swig_setattr_nondynamic(self,class_type,name,value,0)

def _swig_getattr(self,class_type,name):
    if (name == "thisown"): return self.this.own()
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError(name)

def _swig_repr(self):
    try: strthis = "proxy of " + self.this.__repr__()
    except: strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0


class SwigPyIterator(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, SwigPyIterator, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, SwigPyIterator, name)
    def __init__(self, *args, **kwargs): raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _ep_Gillespie2_cL.delete_SwigPyIterator
    __del__ = lambda self : None;
    def value(self): return _ep_Gillespie2_cL.SwigPyIterator_value(self)
    def incr(self, n=1): return _ep_Gillespie2_cL.SwigPyIterator_incr(self, n)
    def decr(self, n=1): return _ep_Gillespie2_cL.SwigPyIterator_decr(self, n)
    def distance(self, *args): return _ep_Gillespie2_cL.SwigPyIterator_distance(self, *args)
    def equal(self, *args): return _ep_Gillespie2_cL.SwigPyIterator_equal(self, *args)
    def copy(self): return _ep_Gillespie2_cL.SwigPyIterator_copy(self)
    def next(self): return _ep_Gillespie2_cL.SwigPyIterator_next(self)
    def __next__(self): return _ep_Gillespie2_cL.SwigPyIterator___next__(self)
    def previous(self): return _ep_Gillespie2_cL.SwigPyIterator_previous(self)
    def advance(self, *args): return _ep_Gillespie2_cL.SwigPyIterator_advance(self, *args)
    def __eq__(self, *args): return _ep_Gillespie2_cL.SwigPyIterator___eq__(self, *args)
    def __ne__(self, *args): return _ep_Gillespie2_cL.SwigPyIterator___ne__(self, *args)
    def __iadd__(self, *args): return _ep_Gillespie2_cL.SwigPyIterator___iadd__(self, *args)
    def __isub__(self, *args): return _ep_Gillespie2_cL.SwigPyIterator___isub__(self, *args)
    def __add__(self, *args): return _ep_Gillespie2_cL.SwigPyIterator___add__(self, *args)
    def __sub__(self, *args): return _ep_Gillespie2_cL.SwigPyIterator___sub__(self, *args)
    def __iter__(self): return self
SwigPyIterator_swigregister = _ep_Gillespie2_cL.SwigPyIterator_swigregister
SwigPyIterator_swigregister(SwigPyIterator)

class VecFloat(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VecFloat, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VecFloat, name)
    __repr__ = _swig_repr
    def iterator(self): return _ep_Gillespie2_cL.VecFloat_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _ep_Gillespie2_cL.VecFloat___nonzero__(self)
    def __bool__(self): return _ep_Gillespie2_cL.VecFloat___bool__(self)
    def __len__(self): return _ep_Gillespie2_cL.VecFloat___len__(self)
    def pop(self): return _ep_Gillespie2_cL.VecFloat_pop(self)
    def __getslice__(self, *args): return _ep_Gillespie2_cL.VecFloat___getslice__(self, *args)
    def __setslice__(self, *args): return _ep_Gillespie2_cL.VecFloat___setslice__(self, *args)
    def __delslice__(self, *args): return _ep_Gillespie2_cL.VecFloat___delslice__(self, *args)
    def __delitem__(self, *args): return _ep_Gillespie2_cL.VecFloat___delitem__(self, *args)
    def __getitem__(self, *args): return _ep_Gillespie2_cL.VecFloat___getitem__(self, *args)
    def __setitem__(self, *args): return _ep_Gillespie2_cL.VecFloat___setitem__(self, *args)
    def append(self, *args): return _ep_Gillespie2_cL.VecFloat_append(self, *args)
    def empty(self): return _ep_Gillespie2_cL.VecFloat_empty(self)
    def size(self): return _ep_Gillespie2_cL.VecFloat_size(self)
    def clear(self): return _ep_Gillespie2_cL.VecFloat_clear(self)
    def swap(self, *args): return _ep_Gillespie2_cL.VecFloat_swap(self, *args)
    def get_allocator(self): return _ep_Gillespie2_cL.VecFloat_get_allocator(self)
    def begin(self): return _ep_Gillespie2_cL.VecFloat_begin(self)
    def end(self): return _ep_Gillespie2_cL.VecFloat_end(self)
    def rbegin(self): return _ep_Gillespie2_cL.VecFloat_rbegin(self)
    def rend(self): return _ep_Gillespie2_cL.VecFloat_rend(self)
    def pop_back(self): return _ep_Gillespie2_cL.VecFloat_pop_back(self)
    def erase(self, *args): return _ep_Gillespie2_cL.VecFloat_erase(self, *args)
    def __init__(self, *args): 
        this = _ep_Gillespie2_cL.new_VecFloat(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _ep_Gillespie2_cL.VecFloat_push_back(self, *args)
    def front(self): return _ep_Gillespie2_cL.VecFloat_front(self)
    def back(self): return _ep_Gillespie2_cL.VecFloat_back(self)
    def assign(self, *args): return _ep_Gillespie2_cL.VecFloat_assign(self, *args)
    def resize(self, *args): return _ep_Gillespie2_cL.VecFloat_resize(self, *args)
    def insert(self, *args): return _ep_Gillespie2_cL.VecFloat_insert(self, *args)
    def reserve(self, *args): return _ep_Gillespie2_cL.VecFloat_reserve(self, *args)
    def capacity(self): return _ep_Gillespie2_cL.VecFloat_capacity(self)
    __swig_destroy__ = _ep_Gillespie2_cL.delete_VecFloat
    __del__ = lambda self : None;
VecFloat_swigregister = _ep_Gillespie2_cL.VecFloat_swigregister
VecFloat_swigregister(VecFloat)

class VecVecfloat(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VecVecfloat, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VecVecfloat, name)
    __repr__ = _swig_repr
    def iterator(self): return _ep_Gillespie2_cL.VecVecfloat_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _ep_Gillespie2_cL.VecVecfloat___nonzero__(self)
    def __bool__(self): return _ep_Gillespie2_cL.VecVecfloat___bool__(self)
    def __len__(self): return _ep_Gillespie2_cL.VecVecfloat___len__(self)
    def pop(self): return _ep_Gillespie2_cL.VecVecfloat_pop(self)
    def __getslice__(self, *args): return _ep_Gillespie2_cL.VecVecfloat___getslice__(self, *args)
    def __setslice__(self, *args): return _ep_Gillespie2_cL.VecVecfloat___setslice__(self, *args)
    def __delslice__(self, *args): return _ep_Gillespie2_cL.VecVecfloat___delslice__(self, *args)
    def __delitem__(self, *args): return _ep_Gillespie2_cL.VecVecfloat___delitem__(self, *args)
    def __getitem__(self, *args): return _ep_Gillespie2_cL.VecVecfloat___getitem__(self, *args)
    def __setitem__(self, *args): return _ep_Gillespie2_cL.VecVecfloat___setitem__(self, *args)
    def append(self, *args): return _ep_Gillespie2_cL.VecVecfloat_append(self, *args)
    def empty(self): return _ep_Gillespie2_cL.VecVecfloat_empty(self)
    def size(self): return _ep_Gillespie2_cL.VecVecfloat_size(self)
    def clear(self): return _ep_Gillespie2_cL.VecVecfloat_clear(self)
    def swap(self, *args): return _ep_Gillespie2_cL.VecVecfloat_swap(self, *args)
    def get_allocator(self): return _ep_Gillespie2_cL.VecVecfloat_get_allocator(self)
    def begin(self): return _ep_Gillespie2_cL.VecVecfloat_begin(self)
    def end(self): return _ep_Gillespie2_cL.VecVecfloat_end(self)
    def rbegin(self): return _ep_Gillespie2_cL.VecVecfloat_rbegin(self)
    def rend(self): return _ep_Gillespie2_cL.VecVecfloat_rend(self)
    def pop_back(self): return _ep_Gillespie2_cL.VecVecfloat_pop_back(self)
    def erase(self, *args): return _ep_Gillespie2_cL.VecVecfloat_erase(self, *args)
    def __init__(self, *args): 
        this = _ep_Gillespie2_cL.new_VecVecfloat(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _ep_Gillespie2_cL.VecVecfloat_push_back(self, *args)
    def front(self): return _ep_Gillespie2_cL.VecVecfloat_front(self)
    def back(self): return _ep_Gillespie2_cL.VecVecfloat_back(self)
    def assign(self, *args): return _ep_Gillespie2_cL.VecVecfloat_assign(self, *args)
    def resize(self, *args): return _ep_Gillespie2_cL.VecVecfloat_resize(self, *args)
    def insert(self, *args): return _ep_Gillespie2_cL.VecVecfloat_insert(self, *args)
    def reserve(self, *args): return _ep_Gillespie2_cL.VecVecfloat_reserve(self, *args)
    def capacity(self): return _ep_Gillespie2_cL.VecVecfloat_capacity(self)
    __swig_destroy__ = _ep_Gillespie2_cL.delete_VecVecfloat
    __del__ = lambda self : None;
VecVecfloat_swigregister = _ep_Gillespie2_cL.VecVecfloat_swigregister
VecVecfloat_swigregister(VecVecfloat)

class VecInt(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VecInt, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VecInt, name)
    __repr__ = _swig_repr
    def iterator(self): return _ep_Gillespie2_cL.VecInt_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _ep_Gillespie2_cL.VecInt___nonzero__(self)
    def __bool__(self): return _ep_Gillespie2_cL.VecInt___bool__(self)
    def __len__(self): return _ep_Gillespie2_cL.VecInt___len__(self)
    def pop(self): return _ep_Gillespie2_cL.VecInt_pop(self)
    def __getslice__(self, *args): return _ep_Gillespie2_cL.VecInt___getslice__(self, *args)
    def __setslice__(self, *args): return _ep_Gillespie2_cL.VecInt___setslice__(self, *args)
    def __delslice__(self, *args): return _ep_Gillespie2_cL.VecInt___delslice__(self, *args)
    def __delitem__(self, *args): return _ep_Gillespie2_cL.VecInt___delitem__(self, *args)
    def __getitem__(self, *args): return _ep_Gillespie2_cL.VecInt___getitem__(self, *args)
    def __setitem__(self, *args): return _ep_Gillespie2_cL.VecInt___setitem__(self, *args)
    def append(self, *args): return _ep_Gillespie2_cL.VecInt_append(self, *args)
    def empty(self): return _ep_Gillespie2_cL.VecInt_empty(self)
    def size(self): return _ep_Gillespie2_cL.VecInt_size(self)
    def clear(self): return _ep_Gillespie2_cL.VecInt_clear(self)
    def swap(self, *args): return _ep_Gillespie2_cL.VecInt_swap(self, *args)
    def get_allocator(self): return _ep_Gillespie2_cL.VecInt_get_allocator(self)
    def begin(self): return _ep_Gillespie2_cL.VecInt_begin(self)
    def end(self): return _ep_Gillespie2_cL.VecInt_end(self)
    def rbegin(self): return _ep_Gillespie2_cL.VecInt_rbegin(self)
    def rend(self): return _ep_Gillespie2_cL.VecInt_rend(self)
    def pop_back(self): return _ep_Gillespie2_cL.VecInt_pop_back(self)
    def erase(self, *args): return _ep_Gillespie2_cL.VecInt_erase(self, *args)
    def __init__(self, *args): 
        this = _ep_Gillespie2_cL.new_VecInt(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _ep_Gillespie2_cL.VecInt_push_back(self, *args)
    def front(self): return _ep_Gillespie2_cL.VecInt_front(self)
    def back(self): return _ep_Gillespie2_cL.VecInt_back(self)
    def assign(self, *args): return _ep_Gillespie2_cL.VecInt_assign(self, *args)
    def resize(self, *args): return _ep_Gillespie2_cL.VecInt_resize(self, *args)
    def insert(self, *args): return _ep_Gillespie2_cL.VecInt_insert(self, *args)
    def reserve(self, *args): return _ep_Gillespie2_cL.VecInt_reserve(self, *args)
    def capacity(self): return _ep_Gillespie2_cL.VecInt_capacity(self)
    __swig_destroy__ = _ep_Gillespie2_cL.delete_VecInt
    __del__ = lambda self : None;
VecInt_swigregister = _ep_Gillespie2_cL.VecInt_swigregister
VecInt_swigregister(VecInt)

class VecVecint(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VecVecint, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VecVecint, name)
    __repr__ = _swig_repr
    def iterator(self): return _ep_Gillespie2_cL.VecVecint_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _ep_Gillespie2_cL.VecVecint___nonzero__(self)
    def __bool__(self): return _ep_Gillespie2_cL.VecVecint___bool__(self)
    def __len__(self): return _ep_Gillespie2_cL.VecVecint___len__(self)
    def pop(self): return _ep_Gillespie2_cL.VecVecint_pop(self)
    def __getslice__(self, *args): return _ep_Gillespie2_cL.VecVecint___getslice__(self, *args)
    def __setslice__(self, *args): return _ep_Gillespie2_cL.VecVecint___setslice__(self, *args)
    def __delslice__(self, *args): return _ep_Gillespie2_cL.VecVecint___delslice__(self, *args)
    def __delitem__(self, *args): return _ep_Gillespie2_cL.VecVecint___delitem__(self, *args)
    def __getitem__(self, *args): return _ep_Gillespie2_cL.VecVecint___getitem__(self, *args)
    def __setitem__(self, *args): return _ep_Gillespie2_cL.VecVecint___setitem__(self, *args)
    def append(self, *args): return _ep_Gillespie2_cL.VecVecint_append(self, *args)
    def empty(self): return _ep_Gillespie2_cL.VecVecint_empty(self)
    def size(self): return _ep_Gillespie2_cL.VecVecint_size(self)
    def clear(self): return _ep_Gillespie2_cL.VecVecint_clear(self)
    def swap(self, *args): return _ep_Gillespie2_cL.VecVecint_swap(self, *args)
    def get_allocator(self): return _ep_Gillespie2_cL.VecVecint_get_allocator(self)
    def begin(self): return _ep_Gillespie2_cL.VecVecint_begin(self)
    def end(self): return _ep_Gillespie2_cL.VecVecint_end(self)
    def rbegin(self): return _ep_Gillespie2_cL.VecVecint_rbegin(self)
    def rend(self): return _ep_Gillespie2_cL.VecVecint_rend(self)
    def pop_back(self): return _ep_Gillespie2_cL.VecVecint_pop_back(self)
    def erase(self, *args): return _ep_Gillespie2_cL.VecVecint_erase(self, *args)
    def __init__(self, *args): 
        this = _ep_Gillespie2_cL.new_VecVecint(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _ep_Gillespie2_cL.VecVecint_push_back(self, *args)
    def front(self): return _ep_Gillespie2_cL.VecVecint_front(self)
    def back(self): return _ep_Gillespie2_cL.VecVecint_back(self)
    def assign(self, *args): return _ep_Gillespie2_cL.VecVecint_assign(self, *args)
    def resize(self, *args): return _ep_Gillespie2_cL.VecVecint_resize(self, *args)
    def insert(self, *args): return _ep_Gillespie2_cL.VecVecint_insert(self, *args)
    def reserve(self, *args): return _ep_Gillespie2_cL.VecVecint_reserve(self, *args)
    def capacity(self): return _ep_Gillespie2_cL.VecVecint_capacity(self)
    __swig_destroy__ = _ep_Gillespie2_cL.delete_VecVecint
    __del__ = lambda self : None;
VecVecint_swigregister = _ep_Gillespie2_cL.VecVecint_swigregister
VecVecint_swigregister(VecVecint)

class VecBool(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VecBool, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VecBool, name)
    __repr__ = _swig_repr
    def iterator(self): return _ep_Gillespie2_cL.VecBool_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _ep_Gillespie2_cL.VecBool___nonzero__(self)
    def __bool__(self): return _ep_Gillespie2_cL.VecBool___bool__(self)
    def __len__(self): return _ep_Gillespie2_cL.VecBool___len__(self)
    def pop(self): return _ep_Gillespie2_cL.VecBool_pop(self)
    def __getslice__(self, *args): return _ep_Gillespie2_cL.VecBool___getslice__(self, *args)
    def __setslice__(self, *args): return _ep_Gillespie2_cL.VecBool___setslice__(self, *args)
    def __delslice__(self, *args): return _ep_Gillespie2_cL.VecBool___delslice__(self, *args)
    def __delitem__(self, *args): return _ep_Gillespie2_cL.VecBool___delitem__(self, *args)
    def __getitem__(self, *args): return _ep_Gillespie2_cL.VecBool___getitem__(self, *args)
    def __setitem__(self, *args): return _ep_Gillespie2_cL.VecBool___setitem__(self, *args)
    def append(self, *args): return _ep_Gillespie2_cL.VecBool_append(self, *args)
    def empty(self): return _ep_Gillespie2_cL.VecBool_empty(self)
    def size(self): return _ep_Gillespie2_cL.VecBool_size(self)
    def clear(self): return _ep_Gillespie2_cL.VecBool_clear(self)
    def swap(self, *args): return _ep_Gillespie2_cL.VecBool_swap(self, *args)
    def get_allocator(self): return _ep_Gillespie2_cL.VecBool_get_allocator(self)
    def begin(self): return _ep_Gillespie2_cL.VecBool_begin(self)
    def end(self): return _ep_Gillespie2_cL.VecBool_end(self)
    def rbegin(self): return _ep_Gillespie2_cL.VecBool_rbegin(self)
    def rend(self): return _ep_Gillespie2_cL.VecBool_rend(self)
    def pop_back(self): return _ep_Gillespie2_cL.VecBool_pop_back(self)
    def erase(self, *args): return _ep_Gillespie2_cL.VecBool_erase(self, *args)
    def __init__(self, *args): 
        this = _ep_Gillespie2_cL.new_VecBool(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _ep_Gillespie2_cL.VecBool_push_back(self, *args)
    def front(self): return _ep_Gillespie2_cL.VecBool_front(self)
    def back(self): return _ep_Gillespie2_cL.VecBool_back(self)
    def assign(self, *args): return _ep_Gillespie2_cL.VecBool_assign(self, *args)
    def resize(self, *args): return _ep_Gillespie2_cL.VecBool_resize(self, *args)
    def insert(self, *args): return _ep_Gillespie2_cL.VecBool_insert(self, *args)
    def reserve(self, *args): return _ep_Gillespie2_cL.VecBool_reserve(self, *args)
    def capacity(self): return _ep_Gillespie2_cL.VecBool_capacity(self)
    __swig_destroy__ = _ep_Gillespie2_cL.delete_VecBool
    __del__ = lambda self : None;
VecBool_swigregister = _ep_Gillespie2_cL.VecBool_swigregister
VecBool_swigregister(VecBool)


def HCV_cell_curve_c(*args):
  return _ep_Gillespie2_cL.HCV_cell_curve_c(*args)
HCV_cell_curve_c = _ep_Gillespie2_cL.HCV_cell_curve_c

def model_Gillespie_c(*args):
  return _ep_Gillespie2_cL.model_Gillespie_c(*args)
model_Gillespie_c = _ep_Gillespie2_cL.model_Gillespie_c
# This file is compatible with both classic and new-style classes.


