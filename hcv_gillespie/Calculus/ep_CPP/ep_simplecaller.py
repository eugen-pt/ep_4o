# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 17:25:50 2015

@author: root
"""
import subprocess
import struct

import numpy as np

from time import clock

import ep_Gillespie2_c
import ep_Gillespie2_cL

HCV_Pars = [ 0.363    , 0.05         , 4          , 1      , 0.0005    , 0.25      ]
tX0 = [0.0]*29

tX0[3]=1000.0


time_of_death = -400;

mode = 4;
print(mode)

NT=-1;

NRT=1;
t=clock();
for j in range(NRT):
  if(mode==3):
    R = ep_Gillespie2_c.HCV_cell_curve_c(HCV_Pars,tX0,time_of_death)
    NP = int(R[0])
    NT = (len(R)-1)/(NP+1)
    Ps = [list(R[1+j*(NP+1):1+(j+1)*(NP+1)]) for j in range(NT)]
#    Ps = np.array(R[1:]).reshape((NT,NP+1))

  elif(mode==4):
#    R = ep_Gillespie2_cL.HCV_cell_curve_c(HCV_Pars,tX0,time_of_death)
#    NP = int(R[0])
#    NT = (len(R)-1)/(NP+1)
#    Ps = [list(R[1+j*(NP+1):1+(j+1)*(NP+1)]) for j in range(NT)]

    Ps = ep_Gillespie2_cL.HCV_cell_curve_c(HCV_Pars,tX0,time_of_death)
    NP = len(Ps[0])-1
    NT=len(Ps)
  else:
    if(mode==1):
      comm = ["./ep_Gillespie2"]+map(str,HCV_Pars)+map(str,tX0)+[str(time_of_death)]
      s=subprocess.check_output(comm)
      NP = struct.unpack('<i',s[0:4])[0]
    elif(mode==2):
      comm = ["./ep_Gillespie2_C"]+map(str,HCV_Pars)+map(str,tX0)+[str(time_of_death)]
      s=subprocess.check_output(comm)
#      print(' '.join(comm))
      NP = int(struct.unpack('<f',s[0:4])[0])
    NT = (len(s)-4)/(4*(NP+1))
  #  Ts = np.zeros((NT,),'float')
  #  Ps = np.zeros((NT,NP),'int')
    Ps = [ [] for j in range(NT)]
    for j in range(NT):
      Ps[j] = list(struct.unpack('%if'%(NP+1),s[4+j*4*(NP+1):4+(j+1)*4*(NP+1)]))

  print(NT)
#    Ts[j]=x[0]
#    Ps[j,:] = x[1:]
#  while(sp)

print((clock()-t)/NRT)