#ifndef _code
#define _code

#include <vector>

std::vector<float> HCV_cell_curve_c(std::vector<float> HCV_Pars,std::vector<float> X0,float _time_of_death );

#endif