%module ep_Gillespie2_c
%{
#include "ep_Gillespie2_c.h"
%}
%include "std_vector.i"
namespace std {
  /* On a side note, the names VecDouble and VecVecdouble can be changed, but the order of first the inner vector matters !*/
  %template(VecFloat) vector<float>;
}

%include "ep_Gillespie2_c.h"