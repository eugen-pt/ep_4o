#
#
#
#

#

filename=$(basename "$1")
extension="${filename##*.}"
filename="${filename%.*}"

f="$filename.$extension"
filename_wrap="$filename""_wrap"

echo "___ EP ____ making Py stuff"
echo "Working with: $f"
# echo $filename.cpp
# echo $extension

echo "g++ -c -fPIC $f"
g++ -c -fPIC $f

echo "swig -c++ -python $filename.i"
swig -c++ -python $filename.i

echo "g++ -c -fPIC $filename_wrap.cxx  -I/usr/include/python2.7 -I/usr/lib/python2.7"
g++ -c -fPIC $filename_wrap.cxx  -I/usr/include/python2.7 -I/usr/lib/python2.7


echo "g++ -shared -Wl,-soname,_$filename.so -o _$filename.so $filename.o $filename_wrap.o"
g++ -shared -Wl,-soname,_$filename.so -o _$filename.so $filename.o $filename_wrap.o



#