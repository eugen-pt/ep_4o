# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 10:39:26 2015

@author: root
"""

from cpython cimport array as c_array
from array import array

cdef extern from "testArrayStruct.h":
  cdef cppclass tAS:
    tAS(int,int*,int**) except +
    int N
    int* Ns
    int** ixs
    int* get_ixs(int)
    
cdef class PytAS:
    cdef tAS *thisptr      # hold a C++ instance which we're wrapping
    def __cinit__(self, int _N,int* _Ns,int** _ixs):
      self.thisptr = new tAS(_N,_Ns,_ixs)
    def get_ixs(self,n):
      temp = c_array(n)
      return []#self.thisptr.get_ixs(temp)