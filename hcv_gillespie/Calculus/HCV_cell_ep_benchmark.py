# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 12:23:38 2015

@author: ep
"""
import random
import numpy as np

def runTest(HCV_cell_curve,debug=1,NRT=100):    
  if(debug==1):
#    from numpy import mean,std
    random.seed(0)
    tX0 = [0]*29
    tX0[3]=1000
    NRT = NRT
    ts = [0.0]*NRT
    for j in range(NRT):
      a,t = HCV_cell_curve(0,tX0,timer=1)
      print('%i: %f'%(j,t))
      ts[j]=t
    print('mean:')
    print(sum(ts)/NRT)
#    print('std:')
#    print(np.std(ts))
#    print('%f+-%f'%(np.mean(ts),np.std(ts)))
  elif(debug==2):  
    startRNA = 5 
    
    
    tX0 = [0]*29
    tX0[3]=startRNA
    
    NmoreTstart = 0  
    times=[]
    
#    NT= 1000
    NT=NRT
    virions_out = 0
    for j in range(NT):
      a,t = HCV_cell_curve(0,tX0,timer=1)
      maxRNA = max([t[4] for t in a ])
      virions_out = virions_out + sum([t[26] for t in a ])
         
      if(maxRNA>startRNA):
        print('__')
        print('max RNA = %i'%maxRNA)
        print('max t = %f'%max([t[0] for t in a]))
        NmoreTstart=NmoreTstart+1
      times.append(t)
    virions_out = virions_out/NT
    print ('virions_out by one cell:%f'%virions_out)
    print('time=%.3f+-%.3f'%(np.mean(times),np.std(times)))  
    print('total time: %f'%sum(times))
    print('__')  
    print('RNA count > start: %i / %i times, %i%%'%(NmoreTstart,NT,NmoreTstart*100.0/NT))

def runBenchmark(v=0):
  if(v==0):
    from HCV_cell import HCV_cell_curve
    runTest(HCV_cell_curve,1,1)
  elif(v==1):
    from HCV_cell_epL import HCV_cell_curve
    runTest(HCV_cell_curve,1,1)
  elif(v==2):
    from HCV_cell_epL_4cy import HCV_cell_curve
    runTest(HCV_cell_curve,1,1)

#import pyximport; pyximport.install()

for j in [0,1,2]:
  print(j)
  runBenchmark(j)