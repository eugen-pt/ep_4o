## Gillespie's algorithm, implemented in Python by Paras Chopra (www.paraschopra.com)

## Implementation of Gillespie's algorithm

import random, math
#import matplotlib.pyplot as plt
import time
import numpy
import gc
from time import clock

from numpy import shape,sqrt,array

#from HCV_cell import *
#from HCV_cell_inhibitors import HCV_cell_inhibitor_curve

debug=1
debugplot = 1

debugfname = ''

def debugWrite(t,s):
  with open(debugfname,'a') as f:
    f.write('%f\t%s\n'%(t,s))
 
def add_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,inhibitionTime=500,HCV_Pars = [ 0.05         , 4        , 1    , 0.0005    , 0.25    ]):
    Curing_curves = {}
    for curves in Infected_Curves:
        X0_new = Infected_Curves[curves][0][0]
        for i in range(0,9):
            X0_new.append(0)
        del X0_new[0]
        curve_after_inhibition = HCV_cell_inhibitor_curve(Infected_Curves[curves][2],inhibitorType,X0_new,HCV_Pars = HCV_Pars)
        Curing_curves[curves] = curve_after_inhibition,inhibitionTime,Infected_Curves[curves][2]
        
    return Curing_curves
    
def combination(n,r): ## implements nCr
    numerator = 1.0
    denominator = 1.0

    for i in range(1,r):
        numerator = numerator * (n-i)
        denominator = denominator * i

    return numerator/denominator

class Species:

    def __init__(self, name=None, quantity=0):
        self.name=name
        self.quantity=quantity

class Reaction:

    #def __init__(self, reactants=[], products=[], rate=0):
    def __init__(self, name=None, reactants=[], products=[],r_type=0, rate=0,doit=1):
        self.name=name
        self.reactants=reactants
        self.products=products
        self.r_type=r_type
        self.rate=rate
        self.doit=doit
                
        
class Cell:
  def __init__(self,reactions=[],all_species=[],number=-1):
    self.reactions = reactions
    self.all_species = all_species
    self.all_species_d = {}
    for s in self.all_species:
      self.all_species_d[s.name] = s
    for sp in self.all_species:
        sp.name = ('cell%i_'%number) + sp.name
    for r in self.reactions:
        r.name = ('cell%i_'%number) + r.name
    self.number=number  
  def get_quantity(self,species_name):
    return self.all_species_d[species_name].quantity
  def set_quantity(self,species_name,q):
    self.all_species_d[species_name].quantity=q



def initHCV_cell(cellnum,add_species):
    virFormation = HCV_Pars[0]
    cellFactor= HCV_Pars[1]
    virOut = HCV_Pars[2]

    X0 = [0.0]*29            
    
    V = Species("Vis_w",X0[0])
    p = Species("Proteins_w", X0[1])    
    pcf = Species("Cell factor + proteins_w",X0[2])
    R = Species("RNA_w",X0[3])
    polyp = Species("Polyprotein_w",X0[4])
    cf = Species("Cell factor",X0[5])
        
    Vm1 = Species("Vis_m1", X0[6])
    pm1 = Species("Proteins_m1", X0[7])    
    pcfm1 = Species("Cell factor + proteins_m1",X0[8])
    Rm1 = Species("RNA_m1",X0[9])
    polypm1 = Species("Polyprotein_m1",X0[10])

    Vm2 = Species("Vis_m2", X0[11])
    pm2 = Species("Proteins_m2", X0[12])    
    pcfm2 = Species("Cell factor + proteins_m2",X0[13])
    Rm2 = Species("RNA_m2",X0[14])
    polypm2 = Species("Polyprotein_m2",X0[15])
    
    Vm3 = Species("Vis_m3", X0[16])
    pm3 = Species("Proteins_m3",X0[17])    
    pcfm3 = Species("Cell factor + proteins_m3",X0[18])
    Rm3 = Species("RNA_m3",X0[19])
    polypm3 = Species("Polyprotein_m3",X0[20])
    
    Vir = Species("Virion_wild",X0[21])   
    Virm1 = Species("Virion_m1",X0[22]) 
    Virm2 = Species("Virion_m2",X0[23]) 
    Virm3 = Species("Virion_m3",X0[24]) 
    
#                Vir_out = Species("Virion_wild_out",X0[25])   
#                Virm1_out = Species("Virion_m1_out",X0[26]) 
#                Virm2_out = Species("Virion_m2_out",X0[27]) 
#                Virm3_out = Species("Virion_m3_out",X0[28]) 
    
    kv = 3
    mv = 0.0578
    kout = 1.177
    mr = 0.5 # Degradation rate
    #mr = 0.363 # Degradation rate
    #mr = 0.2 # Degradation rate
    mr = 0.363
    kt = 0.1
    kc = 1
    kcm = 0.42
    kp = 0.75
    kcf = 4 # Cell Factor
    kcf = cellFactor
    mp = 1
    mpolyp = 0.3 
    kout_v_vm = 0.0002
    kout_vm_v = 0.0002
    vf = 0.05 # Virion Formation
    vf = 0.5 # Virion Formation
    vf = virFormation
    mcf = 1
    mvir = 0.25
    vout = 1
    vout = virOut
          
    Polyp_transl_w = Reaction('Polyp_transl_w',[R],[polyp],0,kt) 
    Vesicle_form_w = Reaction('Vesicle_form_w',[pcf, R],[V],1,kv)
    Vesicle_deg_w = Reaction('Vesicle_deg_w',[V],[],1,mv) 
    CF_prod = Reaction('CF_prod',[],[cf],1,kcf) 
    CF_deg = Reaction('CF_deg',[cf],[],1,mcf) 
    Polyp_proc_w = Reaction('Polyp_proc_w',[polyp],[p],1,kc) 
    Replic_form_w = Reaction('Replic_form_w',[cf,p],[pcf],1,kp) 
    RNA_deg_w = Reaction('RNA_deg_w',[R],[],1,mr) 
    P_deg_w = Reaction('P_deg_w',[p],[],1,mp) 
    Replic_deg_w = Reaction('Replic_deg_w',[pcf],[],1,mv) 
    Polyp_deg_w = Reaction('Polyp_deg_w',[polyp],[],1,mpolyp) 
    RNA_prod_w = Reaction('RNA_prod_w',[V],[R],0,kout) 
         
    RNA_prod_m1_w = Reaction('RNA_prod_m1_w',[V],[Rm1],0,kout_v_vm) 
    Vesicle_form_m1 = Reaction('Vesicle_form_m1',[pcfm1, Rm1],[Vm1],1,kv)
    Vesicle_deg_m1 = Reaction('Vesicle_deg_m1',[Vm1],[],1,mv) 
    RNA_prod_m1 = Reaction('RNA_prod_m1',[Vm1],[Rm1],0,kout) 
    RNA_deg_m1 = Reaction('RNA_deg_m1',[Rm1],[],1,mr) 
    Polyp_proc_m1 = Reaction('Polyp_proc_m1',[polypm1],[pm1],1,kcm) 
    Replic_form_m1 = Reaction('Replic_form_m1',[cf,pm1],[pcfm1],1,kp) 
    P_deg_m1 = Reaction('P_deg_m1',[pm1],[],1,mp) 
    Replic_deg_m1 = Reaction('Replic_deg_m1',[pcfm1],[],1,mv) 
    Polyp_transl_m1 = Reaction('Polyp_transl_m1',[Rm1],[polypm1],0,kt) 
    Polyp_deg_m1 = Reaction('Polyp_deg_m1',[polypm1],[],1,mpolyp) 
    RNA_prod_w_m1 = Reaction('RNA_prod_w_m1',[Vm1],[R],0,kout_vm_v) 
         
    RNA_prod_m2_w = Reaction('RNA_prod_m2_w',[V],[Rm2],0,kout_v_vm) 
    Vesicle_form_m2 = Reaction('Vesicle_form_m2',[pcfm2, Rm2],[Vm2],1,kv)
    Vesicle_deg_m2 = Reaction('Vesicle_deg_m2',[Vm2],[],1,mv) 
    RNA_prod_m2 = Reaction('RNA_prod_m2',[Vm2],[Rm2],0,kout) 
    RNA_deg_m2 = Reaction('RNA_deg_m2',[Rm2],[],1,mr) 
    Polyp_proc_m2 = Reaction('Polyp_proc_m2',[polypm2],[pm2],1,kcm) 
    Replic_form_m2 = Reaction('Replic_form_m2',[cf,pm2],[pcfm2],1,kp) 
    P_deg_m2 = Reaction('P_deg_m2',[pm2],[],1,mp) 
    Replic_deg_m2 = Reaction('Replic_deg_m2',[pcfm2],[],1,mv) 
    Polyp_transl_m2 = Reaction('Polyp_transl_m2',[Rm2],[polypm2],0,kt) 
    Polyp_deg_m2 = Reaction('Polyp_deg_m2',[polypm2],[],1,mpolyp) 
    RNA_prod_w_m2 = Reaction('RNA_prod_w_m2',[Vm2],[R],0,kout_vm_v) 
         
         
    RNA_prod_m3_w = Reaction('RNA_prod_m3_w',[V],[Rm3],0,kout_v_vm) 
    Vesicle_form_m3 = Reaction('Vesicle_form_m3',[pcfm3, Rm3],[Vm3],1,kv)
    Vesicle_deg_m3 = Reaction('Vesicle_deg_m3',[Vm3],[],1,mv) 
    RNA_prod_m3 = Reaction('RNA_prod_m3',[Vm3],[Rm3],0,kout) 
    RNA_deg_m3 = Reaction('RNA_deg_m3',[Rm3],[],1,mr) 
    Polyp_proc_m3 = Reaction('Polyp_proc_m3',[polypm3],[pm3],1,kcm) 
    Replic_form_m3 = Reaction('Replic_form_m3',[cf,pm3],[pcfm3],1,kp) 
    P_deg_m3 = Reaction('P_deg_m3',[pm3],[],1,mp) 
    Replic_deg_m3 = Reaction('Replic_deg_m3',[pcfm3],[],1,mv) 
    Polyp_transl_m3 = Reaction('Polyp_transl_m3',[Rm3],[polypm3],0,kt) 
    Polyp_deg_m3 = Reaction('Polyp_deg_m3',[polypm3],[],1,mpolyp) 
    RNA_prod_w_m3 = Reaction('RNA_prod_w_m3',[Vm3],[R],0,kout_vm_v) 
         
    Virion_form_w = Reaction('Virion_form_w',[R],[Vir],1,vf) 
    Virion_form_m1 = Reaction('Virion_form_m1',[Rm1],[Virm1],1,vf) 
    Virion_form_m2 = Reaction('Virion_form_m2',[Rm2],[Virm2],1,vf) 
    Virion_form_m3 = Reaction('Virion_form_m3',[Rm3],[Virm3],1,vf) 
         
    Virion_deg_w = Reaction('Virion_deg_w',[Vir],[],1,mvir) 
    Virion_deg_m1 = Reaction('Virion_deg_m1',[Virm1],[],1,mvir) 
    Virion_deg_m2 = Reaction('Virion_deg_m2',[Virm2],[],1,mvir) 
    Virion_deg_m3 = Reaction('Virion_deg_m3',[Virm3],[],1,mvir) 
         
    Virion_out_w = Reaction('Virion_out_w',[Vir],[add_species["Virion wild"]],1,vout) 
    Virion_out_m1 = Reaction('Virion_out_m1',[Virm1],[add_species["Virion mut1"]],1,vout) 
    Virion_out_m2 = Reaction('Virion_out_m2',[Virm2],[add_species["Virion mut2"]],1,vout) 
    Virion_out_m3 = Reaction('Virion_out_m3',[Virm3],[add_species["Virion mut3"]],1,vout) 
    
    cellSpecies = [V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3,Vir,Virm1,Virm2,Virm3] 
    cellReactions = [Virion_out_w,Virion_out_m1,Virion_out_m2,Virion_out_m3,Virion_form_w, Virion_form_m1, Virion_form_m2, Virion_form_m3,Vesicle_form_w,	Vesicle_form_m1,	Vesicle_form_m2,	Vesicle_form_m3,	Vesicle_deg_w,	Vesicle_deg_m1,	Vesicle_deg_m2,	Vesicle_deg_m3,	RNA_prod_w,	RNA_prod_m1,	RNA_prod_m2,	RNA_prod_m3,	RNA_deg_w,	RNA_deg_m1,	RNA_deg_m2,	RNA_deg_m3,	Polyp_proc_w,	Polyp_proc_m1,	Polyp_proc_m2,	Polyp_proc_m3,	Replic_form_w,	Replic_form_m1,	Replic_form_m2,	Replic_form_m3,	P_deg_w,	P_deg_m1,	P_deg_m2,	P_deg_m3,	Replic_deg_w,	Replic_deg_m1,	Replic_deg_m2,	Replic_deg_m3,	Polyp_transl_w,	Polyp_transl_m1,	Polyp_transl_m2,	Polyp_transl_m3,	Polyp_deg_w,	Polyp_deg_m1,	Polyp_deg_m2,	Polyp_deg_m3,	RNA_prod_w_m1,	RNA_prod_w_m2,	RNA_prod_w_m3,	RNA_prod_m1_w,	RNA_prod_m2_w,	RNA_prod_m3_w,	CF_prod,	CF_deg, Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3]
    
    
    hCell = Cell(cellReactions,cellSpecies,cellnum)
    return hCell

    
    
class Gillespie:

    def __init__(self, reactions=[], all_species=[],inhibitionTime=500,Cells=[]):
        self.reactions=reactions
        self.all_species=all_species
        self.Nreactions=len(reactions)
        ## set initial values
        self.calculate_parameters()
        self.inhibitionTime = inhibitionTime
        
        self.Cells = Cells
        
#        self.healthy_Cells = []#[1]*len(Cells)
        
        self.infection_type = [] #-1 = no infection 0 = wild, >0 => number of a mutant
        
        self.all_species_d = {}
        for s in self.all_species:
            self.all_species_d[s.name] = s
#        self.NCells=0
        
#        self.NInfected = 0
        #print self.reactions,'gill react'
    def set_q(self,species_name,q):    
        self.all_species_d[species_name].quantity = q
    def set_get_quantity(self,species_name,q):    
        self.all_species_d[species_name].quantity = q
    def get_q(self,species_name):
        return self.all_species_d[species_name].quantity
    def get_quantity(self,species_name):
        return self.all_species_d[species_name].quantity
    def calculate_parameters(self):
        self.a_mu = [0.0]*self.Nreactions
        reactionj=0
        while(reactionj < self.Nreactions)  :
            reaction = self.reactions[reactionj]
            try:
                h=reaction.reactants[0].quantity
#            print(reaction.reactants)
                for reactant in reaction.reactants[1:]: 
                    h=h*reactant.quantity
            except:
                h=1.0
            self.a_mu[reactionj] = h*reaction.rate
            reactionj = reactionj+1
        self.a_0 = sum(self.a_mu)
    
    def run(self, Global_time=10, inhibitorType=1, verbose=True,HCV_Pars = [0.05         , 4        , 1    , 0.0005    , 0.25    ]):

        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        t=0.0

        output_first_line.append("Time")
        for one_species in self.all_species:
            output_first_line.append(one_species.name)

        
        if verbose:
            to_print=""
            for string in output_first_line:
                to_print=to_print+string+"\t"

            #print to_print

        output.append(output_first_line)

        ## Output at time=0
        output_single=[]
        output_single=[t]
        
        for one_species in self.all_species:
            output_single.append(one_species.quantity)

        
        if verbose:
            to_print=""
            for string in output_single:
                to_print=to_print+str(string)+"\t"

            #print to_print

        output.append(output_single)

        ##Start simulation
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0
        Infected_Curves = {}
        Inf_curves_counter = 0
        changer = 0

        while t < Global_time:
#                print('time=%f/%f'%(t,Global_time))
                
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1/self.a_0)*math.log((1/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]

            ## ith reaction will occur
            # HAPPENS only a) at STARTb) when all the cells are healthy and we have Virions
#            if len(Infected_Curves) == 0:

            weOnlyStarted = (t-tau<0.0000001) #len(Infected_Curves) == 0

            if(weOnlyStarted):
                i = 5
            #print self.reactions[i].name
                
            cur_reaction_name = self.reactions[i].name;    
                
            if(verbose):
                print('time=%f/%f\tnCells=%i\t%s'%(t,Global_time,len(self.Cells),cur_reaction_name))
            debugWrite(t,self.reactions[i].name)
            
            self.all_species[0].quantity = 100 - self.all_species[1].quantity - self.all_species[2].quantity - self.all_species[3].quantity - self.all_species[4].quantity
            
#            if t < self.inhibitionTime: #BEFORE Inhibition       
            if(self.reactions[i].name in ['Cell_infection_w','Cell_infection_m1','Cell_infection_m2','Cell_infection_m3']):
                try:
                   infected_Cell_num=self.infection_type.index(-1) 
                   hCell = hCell[infected_Cell_num]
                   
                except:  
                    hCell=initHCV_cell(len(self.Cells),self.all_species_d)
                    self.Cells.append(hCell)
                    self.all_species.extend(hCell.all_species)
                    self.reactions.extend(hCell.reactions)
                    self.Nreactions = len(self.reactions)
                    infected_Cell_num = len(self.Cells)-1;
                    self.infection_type.append(-2)
                
                
#                while(self.Cells_healthy[infected_Cell_num]==0):
#                    infected_Cell_num=infected_Cell_num+1
#                self.Cells_healthy[infected_Cell_num]=0 #mark the cell infected
                
            
                if self.reactions[i].name =='Cell_infection_w' :
                    self.infection_type[infected_Cell_num] = 0
                    # Choose cell to infect
                        
                    if weOnlyStarted:
#                        X0 = [0, 0, 0,50, 0,0,0, 0,0,    0,    0,    0, 0,    0,     0,     0,    0,0, 0 , 0,    0,     0,    0 , 0,    0, 0,    0 , 0,    0]                    
                            
                        self.Cells[infected_Cell_num].set_quantity('RNA_w',50)
    #                        infected_curve = HCV_cell_curve(0, X0,HCV_Pars = HCV_Pars)
    
                    else:
#                        X0 = [0, 0, 0,5, 0,0,0, 0,0,    0,    0,    0, 0,    0,     0,     0,    0,0, 0 , 0,    0,     0,    0 , 0,    0, 0,    0 , 0,    0]
                        self.Cells[infected_Cell_num].set_quantity('RNA_w',5)
    #                        infected_curve = HCV_cell_curve(0, X0,HCV_Pars = HCV_Pars)
    
    #                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0
    #                    Inf_curves_counter += 1
                
     
                if self.reactions[i].name =='Cell_infection_m1':    
                    self.infection_type[infected_Cell_num] = 1
                    self.Cells[infected_Cell_num].set_quantity('RNA_m1',5)
#                    X0 = [0, 0, 0, 0, 0, 0,0, 0,0, 5,    0,    0, 0,    0,     0,     0,    0,0, 0 , 0,    0,     0, 0 ,0,    0, 0,    0 , 0,    0]
#                    infected_curve = HCV_cell_curve(1, X0,HCV_Pars = HCV_Pars)
#                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1
#                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m2':    
                    self.infection_type[infected_Cell_num] = 2
                    self.Cells[infected_Cell_num].set_quantity('RNA_m2',5)
#                    X0 = [0, 0, 0, 0, 0, 0,0, 0,0,    0,    0,    0, 0,    0, 5,     0,    0,0, 0 , 0,    0,    0, 0 ,0,    0, 0,    0 , 0,    0]
#                    infected_curve = HCV_cell_curve(2,X0,HCV_Pars = HCV_Pars)
#                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2
#                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m3':    
                    self.infection_type[infected_Cell_num] = 3
                    self.Cells[infected_Cell_num].set_quantity('RNA_m3',5)
#                    X0 = [0, 0, 0, 0, 0, 0,0, 0,0,    0,    0,    0, 0,    0,     0,     0,    0,0, 0 ,5,    0, 0, 0 ,0,    0, 0,    0 , 0,    0 ]
#                    infected_curve = HCV_cell_curve(3, X0,HCV_Pars = HCV_Pars)
#                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3
#                    Inf_curves_counter += 1

            if t > self.inhibitionTime: 
                print 'INHIBITION TIME!!!1'
                if changer == 0:
                    changer = 1
                    # TODO!!
#                    Infected_Curves = add_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,inhibitionTime=self.inhibitionTime,HCV_Pars = HCV_Pars)
#                    
#                if self.reactions[i].name =='Cell_infection_w' :
#                    X0 = [0, 0, 0,5, 0,0,0, 0,0,    0,    0,    0, 0,    0,     0,     0,    0,0, 0 , 0,    0,     0,    0 , 0,    0, 0,    0 , 0,    0,     0,    0 , 0,    0, 0,    0 , 0,    0]
#                    infected_curve = HCV_cell_inhibitor_curve(0, inhibitorType, X0,HCV_Pars = HCV_Pars)
#                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0
#                    Inf_curves_counter += 1
##                    print 'Cell_infection_w'
# 
#                if self.reactions[i].name =='Cell_infection_m1':    
#                    X0 = [0, 0, 0, 0, 0, 0,0, 0,0, 5,    0,    0, 0,    0,     0,     0,    0,0, 0 , 0,    0,     0, 0 ,0,    0, 0,    0 , 0,    0,     0,    0 , 0,    0, 0,    0 , 0,    0]
#                    infected_curve = HCV_cell_inhibitor_curve(1, inhibitorType,X0,HCV_Pars = HCV_Pars)
#                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1
#                    Inf_curves_counter += 1
#    
#                if self.reactions[i].name =='Cell_infection_m2':    
#                    X0 = [0, 0, 0, 0, 0, 0,0, 0,0,    0,    0,    0, 0,    0, 5,     0,    0,0, 0 , 0,    0,    0, 0 ,0,    0, 0,    0 , 0,    0,     0,    0 , 0,    0, 0,    0 , 0,    0]
#                    infected_curve = HCV_cell_inhibitor_curve(2, inhibitorType,X0,HCV_Pars = HCV_Pars)
#                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2
#                    Inf_curves_counter += 1
#    
#                if self.reactions[i].name =='Cell_infection_m3':    
#                    X0 = [0, 0, 0, 0, 0, 0,0, 0,0,    0,    0,    0, 0,    0,     0,     0,    0,0, 0 ,5,    0, 0, 0 ,0,    0, 0,    0 , 0,    0 ,     0,    0 , 0,    0, 0,    0 , 0,    0]
#                    infected_curve = HCV_cell_inhibitor_curve(3, inhibitorType,X0,HCV_Pars = HCV_Pars)
#                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3
#                    Inf_curves_counter += 1

#            vir_w = 0 
#            vir_m1 = 0
#            vir_m2 = 0
#            vir_m3 = 0
#            curves_to_del = []
#            curves_to_cure = []
#            for curve in Infected_Curves:
#                time = t - Infected_Curves[curve][1]
#                counter = 0
#                if len(Infected_Curves[curve][0]) == 0:
#                    curves_to_cure.append(curve)                
#                else: 
#                    if (time > Infected_Curves[curve][0][-1][0]):
#                        if (Infected_Curves[curve][0][-1][1] == 0) and (Infected_Curves[curve][0][-1][2] == 0) and(Infected_Curves[curve][0][-1][3] == 0) and (Infected_Curves[curve][0][-1][4] == 0) :
#                            curves_to_cure.append(curve)
#                        else:
#                            curves_to_del.append(curve)
#                    else:
#                        while time > Infected_Curves[curve][0][counter][0]:
#                            counter = counter + 1
#                                     
#                if counter-1 < len(Infected_Curves[curve][0]):
#                    for i3 in range(counter-1,-1,-1): 
#                        vir_w = vir_w + Infected_Curves[curve][0][i3][26]
#                        vir_m1 = vir_m1 + Infected_Curves[curve][0][i3][27]
#                        vir_m2 = vir_m2 + Infected_Curves[curve][0][i3][28]
#                        vir_m3 = vir_m3 + Infected_Curves[curve][0][i3][29]
#                        del Infected_Curves[curve][0][i3]
#                        
#
#                        
##                print    curves_to_del, ' curves_to_del'
##                print    curves_to_cure , 'curves_to_cure'
#            for curves in curves_to_del:
#                
#                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
#                    self.all_species[1].quantity = self.all_species[1].quantity - 1
#                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
#                    self.all_species[2].quantity = self.all_species[2].quantity - 1
#                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
#                    self.all_species[3].quantity = self.all_species[3].quantity - 1
#                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
#                    self.all_species[4].quantity = self.all_species[4].quantity - 1
#                #print 'cur_end'    
#                del Infected_Curves[curves]
#                
#            for curves in curves_to_cure: 
##                    if curve not in Infected_Curves:
##                        print curve ,'cur'
##                        print 'alarm!'
#                self.all_species[0].quantity = self.all_species[0].quantity + 1
#                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
#                    self.all_species[1].quantity = self.all_species[1].quantity - 1
#                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
#                    self.all_species[2].quantity = self.all_species[2].quantity - 1
#                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
#                    self.all_species[3].quantity = self.all_species[3].quantity - 1
#                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
#                    self.all_species[4].quantity = self.all_species[4].quantity - 1
# 
#                #print 'cur_cure'
#                del Infected_Curves[curves]
#                
#        # CH Cinf Cinfm1 Cinfm2 Cinfm3 Vir Virm1 Virm2 Virm3
#        # 0 1     2    3    4     5    6     7     8     
#
#            self.all_species[5].quantity = self.all_species[5].quantity + vir_w
#            self.all_species[6].quantity = self.all_species[6].quantity + vir_m1
#            self.all_species[7].quantity = self.all_species[7].quantity + vir_m2
#            self.all_species[8].quantity = self.all_species[8].quantity + vir_m3
    
                 

            for one_species in self.reactions[i].reactants:
                if self.reactions[i].r_type == 1:               
#                    print('minusing %s (%i->%i)'%(one_species.name,one_species.quantity,one_species.quantity-1))
                    one_species.quantity=one_species.quantity-1

            for one_species in self.reactions[i].products:                
#                if (self.reactions[i].name != 'Vir_out') and (self.reactions[i].name != 'Virm1_out') and (self.reactions[i].name != 'Virm2_out') and (self.reactions[i].name != 'Virm3_out') : 
                one_species.quantity=one_species.quantity+1
            
            
            """ Curing process """
            for cellj in range(len(self.Cells)):
                if self.infection_type[cellj]>=0:
                    if(all([self.Cells[cellj].get_quantity(ss)==0 for ss in ['RNA_w','RNA_m1','RNA_m2','RNA_m3']])):
                        print('cured cell #%i'%cellj) 
                        debugWrite(t,'cured cell')
                        temp_species_name = ["Infected wild cells","Infected mut1 cells","Infected mut2 cells","Infected mut3 cells"][self.infection_type[cellj]]
                        self.infection_type[cellj] = -1
                        self.set_q(temp_species_name,self.get_q(temp_species_name)-1)
                        self.set_q("Healthy cells",self.get_q("Healthy cells")+1)
#                        healthy_Cells.append(cellj)
                        
            
            
            ## Output
            output_single=[t]
        
            for one_species in self.all_species:
                output_single.append(one_species.quantity)
                if one_species.quantity < 0:
                    print('someone < 0 ! (%s)'%one_species.name)
                    Global_time = t                    

             
            if all([self.all_species[ii].quantity==0 for ii in [1,2,3,4,5,6,7,8]]):  
                print('All in [1,2,3,4,5,6,7,8] =0 !')
                Global_time = t
            
            
            if verbose:
                to_print=""
                for string in output_single:
                    to_print=to_print+str(string)+"\t"

                #print to_print

            output.append(output_single)
            ##Recalculate parameters -- Procedure can be optimized

            self.calculate_parameters()
        
        return output,Infected_Curves

def average_curve_by_one_inhibitor(inhibitorType,number_of_curves,parametrs_to_optimize,simTime=1000,timer=0,verbose=0,inhibitionTime=500,res_MinT=0,res_MaxT=-1,HCV_Pars = [0.05         , 4        , 1    , 0.0005    , 0.25    ]):
    o_cellInf = HCV_Pars[3]
    o_virDegr = HCV_Pars[4]
    if(res_MaxT<0):
        res_MaxT = simTime
    t0 = clock()
    
    organizm_curves = []
    
    for ik in range(number_of_curves):  
        
      
        if(verbose==1):
            print('ik=%i/%i'%(ik+1,number_of_curves))


        AllSpecies = []
        AllReactions = []
    
        X1 = [100, 0, 0, 0, 0, 1, 0, 0, 0]
        CH = Species("Healthy cells",X1[0])
        Cinf = Species("Infected wild cells",X1[1])
        Cinfm1 = Species("Infected mut1 cells",X1[2])
        Cinfm2 = Species("Infected mut2 cells",X1[3])
        Cinfm3 = Species("Infected mut3 cells",X1[4])
        Vir_out = Species("Virion wild", X1[5])
        Virm1_out = Species("Virion mut1",X1[6])
        Virm2_out = Species("Virion mut2",X1[7])
        Virm3_out = Species("Virion mut3",X1[8]) 
        
        kinf = 0.0005
        
        kinf = o_cellInf
        mch = 0.0001
        mvir = 0.25
        mvir = o_virDegr        
        
        pch = 1.7
        virout = 1
        
        Cell_infection_w = Reaction('Cell_infection_w', [Vir_out, CH],[Cinf],1, kinf)
        Cell_infection_m1 = Reaction('Cell_infection_m1', [Virm1_out, CH],[Cinfm1],1, kinf)
        Cell_infection_m2 = Reaction('Cell_infection_m2', [Virm2_out, CH],[Cinfm2],1, kinf)
        Cell_infection_m3 = Reaction('Cell_infection_m3', [Virm3_out, CH],[Cinfm3],1, kinf)
        CH_formation = Reaction('CH_formation', [],[CH],1, pch)
#        Vir_outR = Reaction('Vir_out', [],[Vir_out],1, virout)
#        Virm1_outR = Reaction('Virm1_out', [],[Virm1_out],1, virout)
#        Virm2_outR = Reaction('Virm2_out', [],[Virm2_out],1, virout)
#        Virm3_outR = Reaction('Virm3_out', [],[Virm3_out],1, virout)
        CH_degradation = Reaction('CH_degradation', [CH],[],1, mch)
    
        Vir_degradation = Reaction('Vir_degradation', [Vir_out],[],1, mvir)
        Virm1_degradation = Reaction('Virm1_degradation', [Virm1_out],[],1, mvir)
        Virm2_degradation = Reaction('Virm2_degradation', [Virm2_out],[],1, mvir)
        Virm3_degradation = Reaction('Virm3_degradation', [Virm3_out],[],1, mvir)
        
        AllSpecies.extend([CH, Cinf, Cinfm1, Cinfm2, Cinfm3, Vir_out, Virm1_out, Virm2_out, Virm3_out])
        AllReactions.extend([CH_degradation, Virm3_degradation,Virm2_degradation,Virm1_degradation,Vir_degradation,Cell_infection_w,Cell_infection_m1, Cell_infection_m2, Cell_infection_m3,CH_formation])
        

        Cells = []
        if(0): 
            # so that new cells would know where to put their virions
            add_species={}
            for s in [Vir_out,Virm1_out,Virm2_out,Virm3_out]:
                add_species[s.name]=s
            for cellnum in range(CH.quantity):
                hCell=initHCV_cell(cellnum,add_species)
                Cells.append(hCell)
                AllSpecies.extend(hCell.all_species)
                AllReactions.extend(hCell.reactions)


        
        
     
        ex = Gillespie(AllReactions,AllSpecies,inhibitionTime=inhibitionTime,Cells=Cells)
        output, curves = ex.run(simTime,inhibitorType,verbose=verbose,HCV_Pars = HCV_Pars)
        
        for v in output[1:]:
            while(len(v)<len(output[-1])):
                v.append(0)
#        output=array(output)

        
        organizm_curves.append( output[1:] )
    
    if(timer):
        print clock() - t0, "seconds process time" 

    points = []
    values = []
    grid_x = numpy.linspace(res_MinT,res_MaxT,num = res_MaxT-res_MinT)
    grid_allx =     numpy.linspace(0,simTime,num=simTime+1);
    
    summ = numpy.zeros(len(grid_x))
    
    meanData = numpy.zeros((len(grid_x),len(organizm_curves[0][0])))
    allMeanData = numpy.zeros((len(grid_allx),len(organizm_curves[0][0])))
    print(shape(meanData))
#    print(shape(organizm_curves_a))    
    
    for curve in organizm_curves:
        acurve = array(curve)
#    print(shape(acurve))
    if(len(shape(acurve))>1):
        for j in range(1,shape(acurve)[1]):
    #        print(j)
    #        print(shape(acurve[:,0]))
    #        print(shape(acurve[:,j]))
    #        print(shape(meanData[:,j]))
    #        print(shape(grid_x))
    #        print(type(acurve[0][0]))
    #        print(type(grid_x[0]))
            meanData[:,j] = numpy.interp(grid_x,acurve[:,0],acurve[:,j])
            allMeanData[:,j] = numpy.interp(grid_allx,acurve[:,0],acurve[:,j])
    #        Virw = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][6] )
    #        Virm1 = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][7] )
    #        Virm2 = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][8] )
    #        Virm3 = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][9] )
    #        
    #        Inf
    #        for i in range(len(Y)):
    #            summ[i] = summ[i] + Y[i]
#    summ = summ/len(organizm_curves)
#    
#    plt.plot(grid_x,summ)
#    return organizm_curves

    res_parnames = ['InfectedTotal','VirionTotal']
    res_parIxs = [[1,2,3,4],[5,6,7,8]]
    
    res_pars = numpy.zeros((len(grid_x),len(res_parnames)))
    
    for k in range(len(res_parIxs)):
        res_pars[:,k] = numpy.sum(meanData[:,[ij+1 for ij in res_parIxs[k]]],axis=1)
    
    for_Return = {'meanData':allMeanData,'res_pars':res_pars,'output':output}

    gc.collect()
    return for_Return

    
 

HCV_ParNames= ['virFormation','cellFactor','virOut','o_cellInf','o_virDegr']
srcHCV_Pars = [ 0.05         , 4        , 1    , 0.0005    , 0.25    ]
HCV_Pars_min= [ 0.1        , 3        , 0.1    , 0.1     , 0.1    ]
HCV_Pars_max= [ 0.5        , 5        , 1    , 0.5     , 2         ]

#HCV_Pars_min = array(srcHCV_Pars)/3.2
#HCV_Pars_max = array(srcHCV_Pars)*3.2

HCV_Pars_min = array(HCV_Pars_min)
HCV_Pars_max = array(HCV_Pars_max)

def genRandHCV_Pars():
    return array([random.random() for j in range(len(HCV_ParNames))])*(HCV_Pars_max-HCV_Pars_min)+HCV_Pars_min

Target_values = [1000,500]

MaxTime = 100

SimPars = {'Random_seed':4,'number_of_curves':1,'inhibitorType':2,
         'simTime':MaxTime,'inhibitionTime':150,'res_MinT':0,'res_MaxT':MaxTime}

#Random_seed = 1
#number_of_curves = 10
#inhibitorType=1
#simTime=400
#inhibitionTime=3500
#res_MinT=350
#res_MaxT=400
import datetime
import json 

random.seed(SimPars['Random_seed'])

while(1):
  outfname = ''+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+'__outptut_epT_.json'
  
  debugfname = outfname.replace('.json','debugLog.txt')

  HCV_Pars = genRandHCV_Pars()
  returned = average_curve_by_one_inhibitor(SimPars['inhibitorType'],SimPars['number_of_curves'],0,simTime=SimPars['simTime'],verbose=1,timer=1,inhibitionTime=SimPars['inhibitionTime'],res_MinT=SimPars['res_MinT'],res_MaxT=SimPars['res_MaxT'],HCV_Pars = HCV_Pars)

  op = returned['output']
  
  with open(outfname,'w') as f:
    f.write(json.dumps(op))






aaa


op = returned['output']
h=op[0]
op=op[1:]

for v in op:
    while(len(v)<len(op[-1])):
        v.append(0)
op=array(op)

aaa
#random.seed()


def R2_fun(HCV_Pars = [ 0.1, 5 , 0.5, 0.0005 , 0.25]):
#res = average_curve_by_one_inhibitor(1,10,0,simTime=10,verbose=1,timer=1,inhibitionTime=3500,res_MinT=0,res_MaxT=10,HCV_Pars = HCV_Pars)
    returned = average_curve_by_one_inhibitor(SimPars['inhibitorType'],SimPars['number_of_curves'],0,simTime=SimPars['simTime'],verbose=1,timer=1,inhibitionTime=SimPars['inhibitionTime'],res_MinT=SimPars['res_MinT'],res_MaxT=SimPars['res_MaxT'],HCV_Pars = HCV_Pars)
    gc.collect()
    res = returned['res_pars']
    R2 = sum([ sqrt(sum((res[:,j]/Target_values[j]-1)**2)/shape(res)[0])for j in range(len((Target_values))) ])
    for_Return = {'meanData':returned['meanData'],'R2':R2}
    # sum across all res_parametersofsqrt( mean in time ( (value - target_value)/target_value, and all squared ) )
    return for_Return


print(outfname)
with open(outfname,'a') as f:
    f.write('#Target_values=%s\n'%Target_values)
    for k in SimPars:
        f.write('#%s=%s\n'%(k,SimPars[k]))
    
    f.write(','.join(['HCV_Pars']+['%s'%f for f in HCV_ParNames]+['R2',str(987654321)])+'\n')



#aaa
J=0
while(J<1):
    HCV_Pars = genRandHCV_Pars()
    returned = R2_fun(HCV_Pars)
    R2 = returned['R2']
    meanData = returned['meanData']
    print('HCV_Pars:')
    print(HCV_Pars) 
    print('R2=%f'%R2)
    with open(outfname,'a') as f:
        f.write(','.join(['HCV_Pars']+['%f'%f for f in HCV_Pars]+['R2',str(R2)])+'||'+json.dumps([list(a) for a in meanData])+'\n')
    gc.collect()
    J=J+1
 

#
