# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 15:06:35 2015

@author: brain
"""

#import glob
import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import random


rpath = 'output_LOGs/'

def plotLOG(logfname,Nmax=20):
    if(type(logfname)==type(2)):
        #someone geva us a number of logfile!
#        import matplotlib.pyplot as plt
        import glob
        files = glob.glob(rpath+'*debugLOG.txt')
        files.extend(glob.glob(rpath+'*dbgLOG.txt'))
        files.extend(glob.glob(rpath+'*.txt'))
        logfname= files[logfname]
    else:
        if(rpath not in logfname):
            logfname=rpath+logfname
    with open(logfname,'r') as f:
        LS= f.readlines()
    print(logfname+'\t'+str(len(LS)))
    LS = [a[0:-1].split('\t') for a in LS]

    ts = []
    rs = []
    t=0
    s=''
    All_rs=[]
    for a in LS:
        try:
            t=float(a[0])
            s = a[1]
        except:
            pass
        ts.append(t)
        rs.append(s)

    All_rs.extend(np.unique(rs))


#    All_rs = np.unique(All_rs)
    All_rs_Cs = [0]*len(All_rs)

    All_rsD = {All_rs[j]:j for j in range(len(All_rs))}

    for r in rs:
        All_rs_Cs[All_rsD[r]]=All_rs_Cs[All_rsD[r]]+1


    Rix = np.argsort(All_rs_Cs)

    L_rtypes = [All_rsD[a] for a in rs]

    colors = ['b','g','r','c','m','y','k']

    f=plt.figure(1)
    f.clear()
    N2plot= min(len(All_rs),Nmax)
    Js = []
    for i in range(N2plot):
        j=Rix[-1-i]
        Js.append(j)
        tix = [k for k in range(len(L_rtypes)) if L_rtypes[k]==j]
        plt.plot(array(ts)[tix],[N2plot-i-0.25+random.random()*0.5 for iii in tix],'.'+colors[i%len(colors)])
        plt.legend([All_rs[i] for i in Js])
    xl=plt.xlim()
    plt.xlim(xl[0],xl[1]*1.5)
    plt.ylim(0,N2plot+1)

#plotLog()