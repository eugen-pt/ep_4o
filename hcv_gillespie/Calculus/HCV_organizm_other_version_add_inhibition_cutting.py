## Gillespie's algorithm, implemented in Python by Paras Chopra (www.paraschopra.com)

## Implementation of Gillespie's algorithm

import random, math
#import matplotlib.pyplot as plt
import time
import numpy

from numpy import shape,sqrt,array

from HCV_cell import *
from HCV_cell_inhibitors import HCV_cell_inhibitor_curve

debug=1
debugplot = 1

#def combination(n,r): ## implements nCr
#    numerator = 1.0
#    denominator = 1.0
#
#    for i in range(1,r):
#        numerator = numerator * (n-i)
#        denominator = denominator * i
#
#    return numerator/denominator
#
#class Species:
#
#    def __init__(self, name=None, quantity=0):
#        self.name=name
#        self.quantity=quantity
#
#class Reaction:
#
#    #def __init__(self, reactants=[], products=[], rate=0):
#    def __init__(self, name=None, reactants=[], products=[],r_type=0, rate=0):
#        self.name=name
#        self.reactants=reactants
#        self.products=products
#        self.r_type=r_type
#        self.rate=rate
        
              
def add_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,inhibitionTime=500,HCV_Pars = [ 0.363    , 0.05         , 4          , 1      , 0.0005    , 0.25      ]):
    Curing_curves = {}
    for curves in Infected_Curves:
        X0_new = Infected_Curves[curves][0][0]
        for i in range(0,9):
            X0_new.append(0)
        del X0_new[0]
        curve_after_inhibition = HCV_cell_inhibitor_curve(Infected_Curves[curves][2],inhibitorType,X0_new,HCV_Pars = HCV_Pars)
        Curing_curves[curves] = curve_after_inhibition,inhibitionTime,Infected_Curves[curves][2]
        
    return Curing_curves
    
class Gillespie:

    def __init__(self, reactions=[], all_species=[],inhibitionTime=500):
        self.reactions=reactions
        self.all_species=all_species
        self.Nreactions=len(reactions)
        ## set initial values
        self.calculate_parameters()
        self.inhibitionTime = inhibitionTime
        #print self.reactions,'gill react'
        
    def calculate_parameters(self):
        
        self.h_mu = []
        self.a_mu = []
        self.a_0 = 0.0


        for reaction in self.reactions:
            #print reaction
            #print reaction.reactants
            reaction_types={} ## Holds how many reactants are of the same type in 1 reaction
            ## E.g. if 2S + P --> R, then type -= {"S":2,"P":1}
            
            for reactant in reaction.reactants: 
                if not reaction_types.has_key(reactant):
                    reaction_types[reactant]=1
                else:
                    reaction_types[reactant]=reaction_types[reactant]+1
            
            h=1

            for key in reaction_types.keys():
                h=h*key.quantity
            
            #print h,'h'
            
            self.h_mu.append(h)
            #print reaction.rate,'reaction.rate'
            
            self.a_mu.append(h*reaction.rate)            
            self.a_0=self.a_0+h*reaction.rate
            #print self.a_0, 2
            
        #print self.a_0, 3

    def run(self, Global_time=10, inhibitorType=1, verbose=True,HCV_Pars = [ 0.363    , 0.05         , 4          , 1      , 0.0005    , 0.25      ]):

        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        t=0.0

        output_first_line.append("Time")
        for one_species in self.all_species:
            output_first_line.append(one_species.name)

        
        if verbose:
            to_print=""
            for string in output_first_line:
                to_print=to_print+string+"\t"

            #print to_print

        output.append(output_first_line)

        ## Output at time=0
        output_single=[]
        output_single=[t]
        
        for one_species in self.all_species:
            output_single.append(one_species.quantity)

        
        if verbose:
            to_print=""
            for string in output_single:
                to_print=to_print+str(string)+"\t"

            #print to_print

        output.append(output_single)

        ##Start simulation
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0
        Infected_Curves = {}
        Inf_curves_counter = 0
        changer = 0

        while t < Global_time:
            if(verbose):
              print('time=%f/%f'%(t,Global_time))
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1/self.a_0)*math.log((1/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]

            ## ith reaction will occur
            # HAPPENS only a) at START  b) when all the cells are healthy and we have Virions
            if len(Infected_Curves) == 0:
                i = 5
            #print self.reactions[i].name
                
                
            if t < self.inhibitionTime: #BEFORE Inhibition               
                if self.reactions[i].name =='Cell_infection_w' :  
                    if len(Infected_Curves) == 0:
                        with open('/E/EP/ep_work/ICG/O/HCV/Calculus/wild_infected_717.txt','r') as f:    
                            f.readline()
                            infected_curve = []
                            for line in f.readlines():
                                if 'T' not in line:
                                    line = line.replace('\n','')           
                                    words = line[0:-1].split(',')
                                    inf_curve_k = []
                                    for n in range(0,len(words)-1):
                                        inf_curve_k.append(float(words[n]))
                                    infected_curve.append(inf_curve_k)
                    else:
                        X0 = [0, 0, 0,10, 0,  0,  0,   0,  0,    0,    0,    0,   0,    0,     0,     0,    0,  0,   0 , 0,    0,     0,    0 ,   0,    0, 0,    0 ,   0,    0]
                        infected_curve = HCV_cell_curve(0, X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0
                    Inf_curves_counter += 1
                
 
                if self.reactions[i].name =='Cell_infection_m1':      
                    X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,   10,    0,    0,   0,    0,     0,     0,    0,  0,   0 , 0,    0,     0,   0 ,  0,    0, 0,    0 ,   0,    0]
                    infected_curve = HCV_cell_curve(1, X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m2':      
                    X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,    0,    0,    0,   0,    0,   10,     0,    0,  0,   0 , 0,    0,      0,   0 ,  0,    0, 0,    0 ,   0,    0]
                    infected_curve = HCV_cell_curve(2,X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m3':      
                    X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,    0,    0,    0,   0,    0,     0,     0,    0,  0,   0 ,  10,    0,   0,   0 ,  0,    0, 0,    0 ,   0,    0 ]
                    infected_curve = HCV_cell_curve(3, X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3
                    Inf_curves_counter += 1
    
            if t > self.inhibitionTime:  
                if changer == 0:
                    changer = 1
                    Infected_Curves = add_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,inhibitionTime=self.inhibitionTime,HCV_Pars = HCV_Pars)
                    
                if self.reactions[i].name =='Cell_infection_w' :  
                    X0 = [0, 0, 0,10, 0,  0,  0,   0,  0,    0,    0,    0,   0,    0,     0,     0,    0,  0,   0 , 0,    0,     0,    0 ,   0,    0, 0,    0 ,   0,    0,     0,    0 ,   0,    0, 0,    0 ,   0,    0]
                    infected_curve = HCV_cell_inhibitor_curve(0, inhibitorType, X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0
                    Inf_curves_counter += 1
                
 
                if self.reactions[i].name =='Cell_infection_m1':      
                    X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,   10,    0,    0,   0,    0,     0,     0,    0,  0,   0 , 0,    0,     0,   0 ,  0,    0, 0,    0 ,   0,    0,     0,    0 ,   0,    0, 0,    0 ,   0,    0]
                    infected_curve = HCV_cell_inhibitor_curve(1, inhibitorType,X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m2':      
                    X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,    0,    0,    0,   0,    0,   10,     0,    0,  0,   0 , 0,    0,      0,   0 ,  0,    0, 0,    0 ,   0,    0,     0,    0 ,   0,    0, 0,    0 ,   0,    0]
                    infected_curve = HCV_cell_inhibitor_curve(2, inhibitorType,X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m3':      
                    X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,    0,    0,    0,   0,    0,     0,     0,    0,  0,   0 ,  10,    0,   0,   0 ,  0,    0, 0,    0 ,   0,    0 ,     0,    0 ,   0,    0, 0,    0 ,   0,    0]
                    infected_curve = HCV_cell_inhibitor_curve(3, inhibitorType,X0,HCV_Pars = HCV_Pars)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3
                    Inf_curves_counter += 1

            vir_w = 0 
            vir_m1 = 0
            vir_m2 = 0
            vir_m3 = 0
            curves_to_del = []
            curves_to_cure = []
            for curve in Infected_Curves:
                time = t - Infected_Curves[curve][1]
                counter = 0  
                if len(Infected_Curves[curve][0]) == 0:
                    curves_to_cure.append(curve)                
                else: 
                    if (time > Infected_Curves[curve][0][-1][0]):
                        if (Infected_Curves[curve][0][-1][1] == 0) and (Infected_Curves[curve][0][-1][2] == 0) and  (Infected_Curves[curve][0][-1][3] == 0) and (Infected_Curves[curve][0][-1][4] == 0) :
                            curves_to_cure.append(curve)
                        else:
                            curves_to_del.append(curve)
                    else:
                        while time > Infected_Curves[curve][0][counter][0]:
                            counter = counter + 1
                                       
                if counter-1 < len(Infected_Curves[curve][0]):
                    for i3 in range(counter-1,-1,-1): 
                        vir_w = vir_w + Infected_Curves[curve][0][i3][26]
                        vir_m1 = vir_m1 + Infected_Curves[curve][0][i3][27]
                        vir_m2 = vir_m2 + Infected_Curves[curve][0][i3][28]
                        vir_m3 = vir_m3 + Infected_Curves[curve][0][i3][29]
                        del Infected_Curves[curve][0][i3]
                        

                          
#                print      curves_to_del, ' curves_to_del'
#                print      curves_to_cure   , 'curves_to_cure'
            for curves in curves_to_del:
                
                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
                    self.all_species[1].quantity = self.all_species[1].quantity - 1
                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
                    self.all_species[2].quantity = self.all_species[2].quantity - 1
                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
                    self.all_species[3].quantity = self.all_species[3].quantity - 1
                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
                    self.all_species[4].quantity = self.all_species[4].quantity - 1
                #print 'cur_end'    
                del Infected_Curves[curves]
                
            for curves in curves_to_cure:   
#                    if curve not in Infected_Curves:
#                        print curve ,'cur'
#                        print 'alarm!'
                self.all_species[0].quantity = self.all_species[0].quantity + 1
                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
                    self.all_species[1].quantity = self.all_species[1].quantity - 1
                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
                    self.all_species[2].quantity = self.all_species[2].quantity - 1
                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
                    self.all_species[3].quantity = self.all_species[3].quantity - 1
                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
                    self.all_species[4].quantity = self.all_species[4].quantity - 1
 
                #print 'cur_cure'
                del Infected_Curves[curves]
                
        # CH Cinf Cinfm1 Cinfm2 Cinfm3 Vir Virm1 Virm2 Virm3
        # 0   1     2      3      4     5    6     7     8     
     
            self.all_species[5].quantity = self.all_species[5].quantity + vir_w
            self.all_species[6].quantity = self.all_species[6].quantity + vir_m1
            self.all_species[7].quantity = self.all_species[7].quantity + vir_m2
            self.all_species[8].quantity = self.all_species[8].quantity + vir_m3
        
                   

            for one_species in self.reactions[i].reactants:
                #one_species.quantity=one_species.quantity-1
                #print  self.reactions[i].r_type  
 
                if self.reactions[i].r_type == 1:                
                    one_species.quantity=one_species.quantity-1

            for one_species in self.reactions[i].products:                
                if (self.reactions[i].name != 'Vir_out') and (self.reactions[i].name != 'Virm1_out') and (self.reactions[i].name != 'Virm2_out') and (self.reactions[i].name != 'Virm3_out') : 
                    one_species.quantity=one_species.quantity+1
            
            ## Output
            output_single=[t]
        
            for one_species in self.all_species:
                output_single.append(one_species.quantity)

        
            if verbose:
                to_print=""
                for string in output_single:
                    to_print=to_print+str(string)+"\t"

                #print to_print

            output.append(output_single)

            ##Recalculate parameters -- Procedure can be optimized

            self.calculate_parameters()
        
        return output,Infected_Curves

def average_curve_by_one_inhibitor(inhibitorType,number_of_curves,parametrs_to_optimize,simTime=1000,timer=0,verbose=0,inhibitionTime=500,res_MinT=0,res_MaxT=-1,HCV_Pars = [ 0.363    , 0.05         , 4          , 1      , 0.0005    , 0.25      ]):
    o_cellInf =   HCV_Pars[4]
    o_virDegr = HCV_Pars[5]
    if(res_MaxT<0):
      res_MaxT = simTime
    t0 = clock()
    
    organizm_curves = []
    
    for ik in range(number_of_curves):        
        if(verbose==1):
          print('ik=%i/%i'%(ik+1,number_of_curves))
    
        X1 = [1000, 0, 0, 0, 0, 1, 0, 0, 0]
        CH = Species("Healthy cells",X1[0])
        Cinf = Species("Infected wild cells",X1[1])
        Cinfm1 = Species("Infected mut1 cells",X1[2])
        Cinfm2 = Species("Infected mut2 cells",X1[3])
        Cinfm3 = Species("Infected mut3 cells",X1[4])
        Vir = Species("Virion wild", X1[5])
        Virm1 = Species("Virion mut1",X1[6])
        Virm2 = Species("Virion mut2",X1[7])
        Virm3 = Species("Virion mut3",X1[8]) 
        
        kinf = 0.0005
        
        kinf = o_cellInf
        mch = 0.0005
        mvir = 0.25
        mvir = o_virDegr        
        
        pch = 0.7
        virout = 0.5
        
        Cell_infection_w = Reaction('Cell_infection_w', [Vir, CH],[Cinf],1, kinf)
        Cell_infection_m1 = Reaction('Cell_infection_m1', [Virm1, CH],[Cinfm1],1, kinf)
        Cell_infection_m2 = Reaction('Cell_infection_m2', [Virm2, CH],[Cinfm2],1, kinf)
        Cell_infection_m3 = Reaction('Cell_infection_m3', [Virm3, CH],[Cinfm3],1, kinf)
        CH_formation = Reaction('CH_formation', [],[CH],1, pch)
        Vir_out = Reaction('Vir_out', [],[Vir],1, virout)
        Virm1_out = Reaction('Virm1_out', [],[Virm1],1, virout)
        Virm2_out = Reaction('Virm2_out', [],[Virm2],1, virout)
        Virm3_out = Reaction('Virm3_out', [],[Virm3],1, virout)
        CH_degradation = Reaction('CH_degradation', [CH],[],1, mch)
    
        Vir_degradation = Reaction('Vir_degradation', [Vir],[],1, mvir)
        Virm1_degradation = Reaction('Virm1_degradation', [Virm1],[],1, mvir)
        Virm2_degradation = Reaction('Virm2_degradation', [Virm2],[],1, mvir)
        Virm3_degradation = Reaction('Virm3_degradation', [Virm3],[],1, mvir)
       
        ex = Gillespie([CH_degradation, Virm3_degradation,Virm2_degradation,Virm1_degradation,Vir_degradation,Cell_infection_w,Cell_infection_m1, Cell_infection_m2, Cell_infection_m3,CH_formation, Vir_out, Virm1_out,Virm2_out, Virm3_out],
                       [CH, Cinf, Cinfm1, Cinfm2, Cinfm3, Vir, Virm1, Virm2, Virm3],inhibitionTime=inhibitionTime)
        output, curves = ex.run(simTime,inhibitorType,verbose=verbose,HCV_Pars = HCV_Pars)
        
#        Conc = {}
#        for i in range(len(output[0])):
#            Conc[i] = []
#        for j in range(1,len(output)):
#            for i in range(len(output[j])):
#                Conc[i].append(output[j][i])


#        plt.plot( Conc[0], Conc[1], 'r',
#                  Conc[0], Conc[2], 'b')     
#        organizm_curves[ik]  = Conc
        organizm_curves.append( output[1:] )
    
    points = []
    values = []
    grid_x = numpy.linspace(res_MinT,res_MaxT,num = res_MaxT-res_MinT)
    
    summ = numpy.zeros(len(grid_x))
    
    meanData = numpy.zeros((len(grid_x),len(organizm_curves[0][0])))
    print(shape(meanData))
#    print(shape(organizm_curves_a))    
    
    for curve in organizm_curves:
      acurve = array(curve)
#      print(shape(acurve))
      for j in range(1,shape(acurve)[1]):
#        print(j)
#        print(shape(acurve[:,0]))
#        print(shape(acurve[:,j]))
#        print(shape(meanData[:,j]))
#        print(shape(grid_x))
#        print(type(acurve[0][0]))
#        print(type(grid_x[0]))
        meanData[:,j] = numpy.interp(grid_x,acurve[:,0],acurve[:,j])
#        Virw = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][6] )
#        Virm1 = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][7] )
#        Virm2 = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][8] )
#        Virm3 = numpy.interp(grid_x,organizm_curves[curve][0],organizm_curves[curve][9] )
#        
#        Inf
#        for i in range(len(Y)):
#            summ[i] = summ[i] + Y[i]
#    summ = summ/len(organizm_curves)
#    
#    plt.plot(grid_x,summ)
    if(timer):
      print clock() - t0, "seconds process time" 
#    return organizm_curves

    res_parnames = ['HC','InfectedTotal','VirionTotal']
    res_parIxs   = [[0],[1,2,3,4],[5,6,7,8]]
    
    res_pars = numpy.zeros((len(grid_x),len(res_parnames)))
    
    for k in range(len(res_parIxs)):
      res_pars[:,k] = numpy.sum(meanData[:,[ij+1 for ij in res_parIxs[k]]],axis=1)
      

    gc.collect()
    return res_pars

    
    
debug=0
if(debug):
  random.seed(0)
  a = average_curve_by_one_inhibitor(1,10,0,simTime=20,verbose=1,timer=1,inhibitionTime=3500,res_MinT=0,res_MaxT=10)
  
  pass
  if(debugplot):
    pass
 

HCV_ParNames= ['degrRate','virFormation','cellFactor','virOut','o_cellInf','o_virDegr']
srcHCV_Pars = [ 0.363    , 0.05         , 4          , 1      , 0.0005    , 0.25      ]

HCV_Pars_min = array(srcHCV_Pars)/3.2
HCV_Pars_max = array(srcHCV_Pars)*3.2

Target_values = [900,100,10]
random.seed(0)

def genRandHCV_Pars():
  return array([random.random() for j in range(len(HCV_ParNames))])*(HCV_Pars_max-HCV_Pars_min)+HCV_Pars_min

import gc

def R2_fun(HCV_Pars = [ 0.363, 0.05  , 4   , 1  , 0.0005 , 0.25  ]):
#  res = average_curve_by_one_inhibitor(1,10,0,simTime=10,verbose=1,timer=1,inhibitionTime=3500,res_MinT=0,res_MaxT=10,HCV_Pars = HCV_Pars)
  res = average_curve_by_one_inhibitor(1,10,0,simTime=400,verbose=1,timer=1,inhibitionTime=3500,res_MinT=350,res_MaxT=400,HCV_Pars = HCV_Pars)
  # sum across all res_parameters  of  sqrt( mean in time ( (value - target_value)/target_value  , and all squared ) )
  return sum([ sqrt(sum((res[:,j]/Target_values[j]-1)**2)/shape(res)[0])  for j in range(len((Target_values))) ])

import datetime
outfname = '/E/EP/ep_work/ICG/O/HCV/Calculus/'+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+'__outptut__.csv'

with open(outfname,'a') as f:
  f.write('#Target_values=%s\n'%Target_values)
  f.write(','.join(['HCV_Pars']+['%s'%f for f in HCV_ParNames]+['R2',str(987654321)])+'\n')

#aaa

J=1
while(J>0):
  HCV_Pars = genRandHCV_Pars()
  R2 = R2_fun(HCV_Pars)
  print('HCV_Pars:')
  print(HCV_Pars) 
  print('R2=%f'%R2)
  with open(outfname,'a') as f:
    f.write(','.join(['HCV_Pars']+['%f'%f for f in HCV_Pars]+['R2',str(R2)])+'\n')
  J=J+1
 
  
#  