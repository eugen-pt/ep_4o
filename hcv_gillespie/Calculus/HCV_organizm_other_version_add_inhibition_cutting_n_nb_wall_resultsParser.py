# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 22:19:09 2015

@author: ep
"""

import glob
import json 

files = glob.glob('*wall_.csv')

Dyns = []
HCV_Pars = []
R2s = []

for fname in files:
  with open(fname,'r') as f:
    for line in f:
      if(line[0]=='#'):
        continue
      if('||' not in line):
        continue
#      print(line)
      s = line[0:line.index('||')].split(',')
      HCV_Pars.append([float(ts) for ts in s[1:7]])
      R2s.append(float(s[-1]))
#      print(s)
      dyns = json.loads(line[line.index('||')+2:])
      Dyns.append(dyns)
#      print(dyns)
#      raise ValueError
      
aaa

print(outfname)
with open(outfname,'a') as f:
  f.write('#Target_values=%s\n'%Target_values)
  for k in SimPars:
    f.write('#%s=%s\n'%(k,SimPars[k]))
    
  f.write(','.join(['HCV_Pars']+['%s'%f for f in HCV_ParNames]+['R2',str(987654321)])+'\n')

#aaa
J=1
while(J>0):
  HCV_Pars = genRandHCV_Pars()
  returned = R2_fun(HCV_Pars)
  R2 = returned['R2']
  meanData = returned['meanData']
  print('HCV_Pars:')
  print(HCV_Pars) 
  print('R2=%f'%R2)
  with open(outfname,'a') as f:
    f.write(','.join(['HCV_Pars']+['%f'%f for f in HCV_Pars]+['R2',str(R2)])+'||'+json.dumps([list(a) for a in meanData])+'\n')
  gc.collect()
  J=J+1
