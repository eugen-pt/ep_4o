# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 22:19:09 2015

@author: ep
"""

import glob
import json 
import sys
import matplotlib.pyplot as plt
import os
import time
import random
import gc
rpath = 'outputs/'

rimgspath = 'output_Imgs/'
try:
    os.mkdir(rimgspath)
except:
    pass    

rdaylogimgspath = 'output_Imgs_daylog/'
try:
    os.mkdir(rdaylogimgspath)
except:
    pass    

files = glob.glob(rpath+'*outptut_n_nb_wall_.csv')
files = glob.glob(rpath+'*.json')
#files = glob.glob(rpath+'*T300i*.json')
print(files)

Dyns = []
HCV_Pars = []
R2s = []

HCV_ParNames= ['virFormation','cellFactor','virOut','o_cellInf','o_virDegr']

HCV_sParNames = ['vf','cf','vo','o_inf','o_vd'] #short parnames)


SimParsNames = {'Random_seed':'rseed','number_of_curves':'NofCs','inhibitorType':'inhType',
         'simTime':'simT','inhibitionTime':'inhT','res_MinT':'res_MinT','res_MaxT':'res_MaxT','NCells':'NC'}

SimParsssNames = {'inhibitorType':'i','simTime':'sT','inhibitionTime':'iT','res_MinT':'rmT','res_MaxT':'rMT','NCells':'NC'}   


f=plt.figure(1)
percents = {}
for fname in files[2:]:#random.sample(files,len(files)):
#  J=0
  cells_selfcured = 0
  cells_inhcurd = 0
  with open(fname,'r') as f:
      try:
          R = json.load(f)  
      except:
          print('problem with %s'%fname)
          continue
      print(fname)
      dyns = array(R['meanData'])
      HCV_Pars = R['HCV_Pars']
      R2 = R['R2']
      SimPars=R['SimPars']
      
      
#      dyns[dyns<1]=0
#      if(std(dyns[:,0])<10):
#          t = arange(SimPars['simTime']+1)
#      else:    
      t=dyns[:,0]
      for i in range(len(dyns[:,0])):          
          summ_inf_cells = dyns[i,2] + dyns[i,3] + dyns[i,4] + dyns[i,5]
          summ_virions = dyns[i,6] + dyns[i,7] + dyns[i,8] + dyns[i,9]    
          if summ_inf_cells == 0 and summ_virions == 0:
              if t[i] < SimPars['inhibitionTime']:
                  cells_selfcured = cells_selfcured + 1
              else:
                  cells_inhcurd = cells_inhcurd + 1
          else:  
              temp_v = []
              if summ_inf_cells == 0:
                  summ_inf_cells = 1
              if summ_virions == 0:
                  summ_virions = 1                  
              for l in range(4):
                      temp_v.append(100.0*dyns[i,2 + l]/(summ_inf_cells ))
              for l in range(4):
                      temp_v.append((100.0*dyns[i,6 + l]/(summ_virions ) ))
          if t[i]in percents:
              for m in range(len(percents[dyns[i,0]])):
                  percents[t[i]][m] = percents[t[i]][m] + temp_v[m]
          else:
              percents[t[i]] = temp_v
              
  break
#      toptitle = ' '.join(['%s=%i'%(SimParsNames[k],SimPars[k]) for k in SimPars])
#      
##    for line in f:
###      if(J>=1):
###        break
##      if(line[0]=='#'):
##        continue
##      if('||' not in line):
##        continue
###      print(line)
##      s = line[0:line.index('||')].split(',')
##      HCV_Pars=[float(ts) for ts in s[1:6]]
##      R2 = float(s[-1])
##      dyns = json.loads(line[line.index('||')+2:])
#      
#      if(0):
#          imgfname = fname
#    #      imgfname = imgfname.replace('.csv','j%i.png'%(J))
#          imgfname = imgfname.replace('.json','.png')
#          
#          if(0): #if you wanna add parameter values into filename
#            imgfname = imgfname.replace('.png','__'+'_'.join(['%s=%.2f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))])+'.png')
#          imgfname = imgfname.replace(rpath,rimgspath)
#          
#          if(not os.path.isfile(imgfname)):
#              print(fname)
#    #          f=plt.figure(1)
#              plt.plot(dyns)
#        #      legend(['t']+HCV_sParNames)  #dont have the value names (which are in )
#              
#              plt.xlabel(' '.join(['%s=%f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))]))
#              plt.title(toptitle)
#              plt.savefig(imgfname, bbox_inches='tight')
#              plt.cla()
#          
##          plt.close(f)
#          
#      rdaylogimgspath   
#      imgfname = fname
#      imgfname = imgfname.replace('.json','.png')
#      imgfname = imgfname.replace(rpath,rdaylogimgspath)
#      if(not os.path.isfile(imgfname)):
#          print(imgfname)
##          f=plt.figure(1)
#          dyns[dyns<1]=0
#          if(std(dyns[:,0])<10):
#              t = arange(SimPars['simTime']+1)
#          else:    
#              t=dyns[:,0]
#          t=t/24    
#          tmax=0    
#          temp = t*1.0
#            
#          colors = ['g']+['r','b','c','m']*3
#          styles = ['-']*5+['--']*4+['-.']*4
#          
#          for j in range(1,shape(dyns)[1]):
#              temp = log10(dyns[:,j])
#              plt.plot(t,temp,styles[j-1]+colors[j-1])
#              tmax = max(tmax,max(temp))
#          plot(array([SimPars['inhibitionTime']-0.5,SimPars['inhibitionTime']+0.5])/24.0,[1,tmax],'-.k')    
##          plt.plot(log10(dyns))
#    #      legend(['t']+HCV_sParNames)  #dont have the value names (which are in )
#          
#          plt.xlabel(' '.join(['%s=%f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))]))
#          plt.title(toptitle)
#          ty=plt.ylim()
##          plt.ylim([0,ty[1]])
#          plt.savefig(imgfname, bbox_inches='tight')
#          plt.cla()
##          plt.close(f)
##          break
#        
#      gc.collect()    
#      time.sleep(.5)
#      break  
#      J=J+1
#    if(J>=1):
#      break
#      print(dyns)
#      raise ValueError
      
