## Gillespie's algorithm, implemented in Python by Paras Chopra (www.paraschopra.com)

## Implementation of Gillespie's algorithm

import random, math
#import matplotlib.pyplot as plt
import time
import numpy


def combination(n,r): ## implements nCr
    numerator = 1.0
    denominator = 1.0

    for i in range(1,r):
        numerator = numerator * (n-i)
        denominator = denominator * i

    return numerator/denominator

class Species:

    def __init__(self, name=None, quantity=0):
        self.name=name
        self.quantity=quantity

class Reaction:

    #def __init__(self, reactants=[], products=[], rate=0):
    def __init__(self, name=None, reactants=[], products=[],r_type=0, rate=0):
        self.name=name
        self.reactants=reactants
        self.products=products
        self.r_type=r_type
        self.rate=rate

class Gillespie:

    def __init__(self, reactions=[], all_species=[]):
        self.reactions=reactions
        self.all_species=all_species
        ## set initial values
        self.calculate_parameters()
        #print self.reactions,'gill react'
        
    def calculate_parameters(self):
        
        self.h_mu = []
        self.a_mu = []
        self.a_0 = 0.0


        for reaction in self.reactions:
            #print reaction
            #print reaction.reactants
            reaction_types={} ## Holds how many reactants are of the same type in 1 reaction
            ## E.g. if 2S + P --> R, then type -= {"S":2,"P":1}
            
            for reactant in reaction.reactants: 
                if not reaction_types.has_key(reactant):
                    reaction_types[reactant]=1
                else:
                    reaction_types[reactant]=reaction_types[reactant]+1
            
            h=1

            for key in reaction_types.keys():
                h=h*key.quantity
            
            #print h,'h'
            
            self.h_mu.append(h)
            #print reaction.rate,'reaction.rate'
            
            self.a_mu.append(h*reaction.rate)            
            self.a_0=self.a_0+h*reaction.rate
            #print self.a_0, 2
            
        #print self.a_0, 3

    def run(self, Global_time=10, verbose=True):

        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        t=0.0

        output_first_line.append("Time")
        for one_species in self.all_species:
            output_first_line.append(one_species.name)

        
        if verbose:
            to_print=""
            for string in output_first_line:
                to_print=to_print+string+"\t"

            #print to_print

        output.append(output_first_line)

        ## Output at time=0
        output_single=[]
        output_single=[t]
        
        for one_species in self.all_species:
            output_single.append(one_species.quantity)

        
        if verbose:
            to_print=""
            for string in output_single:
                to_print=to_print+str(string)+"\t"

            #print to_print

        output.append(output_single)

        ##Start simulation
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0
        Infected_Curves = {}
        Inf_curves_counter = 0
        while t < Global_time:
            #print self.a_0
            
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1/self.a_0)*math.log((1/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]

            ## ith reaction will occur
#            

            self.all_species[28].quantity = 0 
            self.all_species[27].quantity = 0 
            self.all_species[26].quantity = 0 
            self.all_species[25].quantity = 0             
  
           
            if (0):
                #print self.reactions[i].name
                
                if self.reactions[i].name =='Cell_infection_w':      
                    curve_number = random.choice(curves_w.keys())
                    
                    with open(curves_w[curve_number],'r') as f:    
                        f.readline
                        infected_curve = {}
                        inf_curve_k = 0
                        for line in f.readlines():
                            if'T'not in line:
                                line = line.replace('\n','')           
                                words = line.split(',')
                                infected_curve[inf_curve_k] = []
                                for n in words:
                                    infected_curve[inf_curve_k].append(float(n))
                                inf_curve_k += 1
                                
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0
                    Inf_curves_counter += 1

                if self.reactions[i].name =='Cell_infection_m1':      
                    curve_number = random.choice(curves_m1.keys())
                    
                    with open(curves_m1[curve_number],'r') as f:    
                        f.readline
                        infected_curve = {}
                        inf_curve_k = 0
                        for line in f.readlines():
                            if'T'not in line:
                                line = line.replace('\n','')         
                                words = line.split(',')
                                infected_curve[inf_curve_k] = []
                                for n in words:
                                    infected_curve[inf_curve_k].append(float(n))
                                inf_curve_k += 1
                                
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1
                    Inf_curves_counter += 1

                if self.reactions[i].name =='Cell_infection_m2':      
                    curve_number = random.choice(curves_m2.keys())
                    
                    with open(curves_m2[curve_number],'r') as f:    
                        f.readline
                        infected_curve = {}
                        inf_curve_k = 0
                        for line in f.readlines():
                            if'T'not in line:
                                line = line.replace('\n','')           
                                words = line.split(',')
                                infected_curve[inf_curve_k] = []
                                for n in words:
                                    infected_curve[inf_curve_k].append(float(n))
                                inf_curve_k += 1
                                
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2
                    Inf_curves_counter += 1

                if self.reactions[i].name =='Cell_infection_m3':      
                    curve_number = random.choice(curves_m3.keys())
                    
                    with open(curves_m3[curve_number],'r') as f:    
                        f.readline
                        infected_curve = {}
                        inf_curve_k = 0
                        for line in f.readlines():
                            if'T'not in line:
                                line = line.replace('\n','')           
                                words = line.split(',')
                                infected_curve[inf_curve_k] = []
                                for n in words:
                                    infected_curve[inf_curve_k].append(float(n))
                                inf_curve_k += 1
                                
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3
                    Inf_curves_counter += 1



                # TIME: Infected_Curves[0][0][1][0]
    
            #   time V  p pcf R  polyp cf  Vm1, pm1,pcfm1,Rm1, polypm1 Vm2, pm2, pcfm2, Rm2, polypm2, Vm3,pm3,pcfm3,Rm3,polypm3
            #     0  1  2  3  4    5   6    7   8     9   10    11     12   13    14    15     16     17   18   19  20    21
    
                vir_w = 0 
                vir_m1 = 0
                vir_m2 = 0
                vir_m3 = 0
                curves_to_del = []
                
                for curve in Infected_Curves:
                    time = t - Infected_Curves[curve][1]
                    counter = 0
                    if time > Infected_Curves[curve][0][len(Infected_Curves[curve][0])-1][0]:
                        curves_to_del.append(curve)
                    else:
                        while time > Infected_Curves[curve][0][counter][0]:
                            counter = counter + 1
                            
                        vir_w = vir_w + Infected_Curves[curve][0][counter][5]
                        vir_m1 = vir_m1 + Infected_Curves[curve][0][counter][6]
                        vir_m2 = vir_m2 + Infected_Curves[curve][0][counter][7]
                        vir_m3 = vir_m3 + Infected_Curves[curve][0][counter][8]
                                           

                for curves in curves_to_del:
                    if Infected_Curves[curve][2] == '0':
                        self.all_species[1].quantity = self.all_species[1].quantity - 1
                    if Infected_Curves[curve][2] == '1':
                        self.all_species[2].quantity = self.all_species[2].quantity - 1
                    if Infected_Curves[curve][2] == '2':
                        self.all_species[3].quantity = self.all_species[3].quantity - 1
                    if Infected_Curves[curve][2] == '3':
                        self.all_species[4].quantity = self.all_species[4].quantity - 1
                        
                    del Infected_Curves[curves]
                        
            # CH Cinf Cinfm1 Cinfm2 Cinfm3 Vir Virm1 Virm2 Virm3
            # 0   1     2      3      4     5    6     7     8     
         
                self.all_species[5].quantity = self.all_species[5].quantity + vir_w
                self.all_species[6].quantity = self.all_species[6].quantity + vir_m1
                self.all_species[7].quantity = self.all_species[7].quantity + vir_m2
                self.all_species[8].quantity = self.all_species[8].quantity + vir_m3
            
                   
            for one_species in self.reactions[i].reactants:
                #one_species.quantity=one_species.quantity-1
                #print  self.reactions[i].r_type           
                if self.reactions[i].r_type == 1:                
                    one_species.quantity=one_species.quantity-1

            for one_species in self.reactions[i].products:
                one_species.quantity=one_species.quantity+1

            
            ## Output
            output_single=[t]
        
            for one_species in self.all_species:
                output_single.append(one_species.quantity)

        
            if verbose:
                to_print=""
                for string in output_single:
                    to_print=to_print+str(string)+"\t"

                print to_print

            output.append(output_single)

            ##Recalculate parameters -- Procedure can be optimized

            self.calculate_parameters()
        
        return output
        

def HCV_cell_curve(ind):
    random.seed()

    ## Example Reaction: Isomerization reaction from Gillespie's paper

    ## X ---c---> Z
    if ind == 0:
        
        #     V  p pcf R polyp cf Vm1, pm1,pcfm1,Rm1, polypm1 Vm2, pm2, pcfm2, Rm2, polypm2, Vm3,pm3,pcfm3,Rm3,polypm3 Vir Virm1 Virm2 Virm3 Viro Virm1o Virm2o Virm3o
        #     0  1  2  3  4    5   6    7   8     9     10    11   12    13    14     15     16  17   18   19   20     21   22    23    24   25    26      27    28
        X0 = [0, 0, 0,100, 0,  0,  0,   0,  0,    0,    0,    0,   0,    0,     0,     0,    0,  0,   0 , 0,    0,     0,    0 ,   0,    0, 0,    0 ,   0,    0]

    if ind == 1:
        #     V  p pcf R polyp cf Vm1, pm1,pcfm1,Rm1, polypm1 Vm2, pm2, pcfm2, Rm2, polypm2, Vm3,pm3,pcfm3,Rm3,polypm3 Vir Virm1 Virm2 Virm3 Viro Virm1o Virm2o Virm3o
        #     0  1  2  3  4    5   6    7   8     9     10    11   12    13    14     15     16  17   18   19   20     21   22    23    24   25    26      27    28
        X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,   100,    0,    0,   0,    0,     0,     0,    0,  0,   0 , 0,    0,     0,   0 ,  0,    0, 0,    0 ,   0,    0]

    if ind == 2:
        #     V  p pcf R polyp cf Vm1, pm1,pcfm1,Rm1, polypm1 Vm2, pm2, pcfm2, Rm2, polypm2, Vm3,pm3,pcfm3,Rm3,polypm3 Vir Virm1 Virm2 Virm3 Viro Virm1o Virm2o Virm3o
        #     0  1  2  3  4    5   6    7   8     9     10    11   12    13    14     15     16  17   18   19   20     21   22    23    24   25    26      27    28
        X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,    0,    0,    0,   0,    0,   100,     0,    0,  0,   0 , 0,    0,      0,   0 ,  0,    0, 0,    0 ,   0,    0]

    if ind == 3:
        #     V  p pcf R polyp cf Vm1, pm1,pcfm1,Rm1, polypm1 Vm2, pm2, pcfm2, Rm2, polypm2, Vm3,pm3,pcfm3,Rm3,polypm3 Vir Virm1 Virm2 Virm3 Viro Virm1o Virm2o Virm3o
        #     0  1  2  3  4    5   6    7   8     9     10    11   12    13    14     15     16  17   18   19   20     21   22    23    24   25    26      27    28
        X0 = [0, 0, 0, 0, 0,   0,  0,   0,  0,    0,    0,    0,   0,    0,     0,     0,    0,  0,   0 ,  100,    0,   0,   0 ,  0,    0, 0,    0 ,   0,    0 ]

    V = Species("Vis_w",X0[0])
    p = Species("Proteins_w", X0[1])    
    pcf = Species("Cell factor + proteins_w",X0[2])
    R = Species("RNA_w",X0[3])
    polyp = Species("Polyprotein_w",X0[4])
    cf = Species("Cell factor",X0[5])
        
    Vm1 = Species("Vis_m1", X0[6])
    pm1 = Species("Proteins_m1", X0[7])    
    pcfm1 = Species("Cell factor + proteins_m1",X0[8])
    Rm1 = Species("RNA_m1",X0[9])
    polypm1 = Species("Polyprotein_m1",X0[10])

    Vm2 = Species("Vis_m2", X0[11])
    pm2 = Species("Proteins_m2", X0[12])    
    pcfm2 = Species("Cell factor + proteins_m2",X0[13])
    Rm2 = Species("RNA_m2",X0[14])
    polypm2 = Species("Polyprotein_m2",X0[15])
    
    Vm3 = Species("Vis_m3", X0[16])
    pm3 = Species("Proteins_m3",X0[17])    
    pcfm3 = Species("Cell factor + proteins_m3",X0[18])
    Rm3 = Species("RNA_m3",X0[19])
    polypm3 = Species("Polyprotein_m3",X0[20])
    
    Vir = Species("Virion_wild",X0[21])   
    Virm1 = Species("Virion_m1",X0[22]) 
    Virm2 = Species("Virion_m2",X0[23]) 
    Virm3 = Species("Virion_m3",X0[24]) 
    
    Vir_out = Species("Virion_wild_out",X0[25])   
    Virm1_out = Species("Virion_m1_out",X0[26]) 
    Virm2_out = Species("Virion_m2_out",X0[27]) 
    Virm3_out = Species("Virion_m3_out",X0[28]) 
    
    kv = 3
    mv = 0.0578
    kout = 1.177
    mr = 0.5
    kt = 0.1
    kc = 1
    kcm = 0.42
    kp = 0.75
    kcf = 5
    mp = 1
    mpolyp = 0.3 
    kout_v_vm = 0.0002
    kout_vm_v = 0.0002
    vf = 0.05
    ki = 0.12
    ki_rev = 84
    ki2 = 0
    ki_rev2 = 0 
    mcf = 1
    mvir = 0.0578
    vout = 0.01
          
    Polyp_transl_w = Reaction('Polyp_transl_w',[R],[polyp],0,kt) 
    Vesicle_form_w = Reaction('Vesicle_form_w',[pcf, R],[V],1,kv)
    Vesicle_deg_w = Reaction('Vesicle_deg_w',[V],[],1,mv) 
    CF_prod = Reaction('CF_prod',[],[cf],1,kcf) 
    CF_deg = Reaction('CF_deg',[cf],[],1,mcf) 
    Polyp_proc_w = Reaction('Polyp_proc_w',[polyp],[p],1,kc) 
    Replic_form_w = Reaction('Replic_form_w',[cf,p],[pcf],1,kp) 
    RNA_deg_w = Reaction('RNA_deg_w',[R],[],1,mr) 
    P_deg_w = Reaction('P_deg_w',[p],[],1,mp) 
    Replic_deg_w = Reaction('Replic_deg_w',[pcf],[],1,mv) 
    Polyp_deg_w = Reaction('Polyp_deg_w',[polyp],[],1,mpolyp) 
    RNA_prod_w = Reaction('RNA_prod_w',[V],[R],0,kout) 
         
    RNA_prod_m1_w = Reaction('RNA_prod_m1_w',[V],[Rm1],0,kout_v_vm) 
    Vesicle_form_m1 = Reaction('Vesicle_form_m1',[pcfm1, Rm1],[Vm1],1,kv)
    Vesicle_deg_m1 = Reaction('Vesicle_deg_m1',[Vm1],[],1,mv) 
    RNA_prod_m1 = Reaction('RNA_prod_m1',[Vm1],[Rm1],0,kout) 
    RNA_deg_m1 = Reaction('RNA_deg_m1',[Rm1],[],1,mr) 
    Polyp_proc_m1 = Reaction('Polyp_proc_m1',[polypm1],[pm1],1,kcm) 
    Replic_form_m1 = Reaction('Replic_form_m1',[cf,pm1],[pcfm1],1,kp) 
    P_deg_m1 = Reaction('P_deg_m1',[pm1],[],1,mp) 
    Replic_deg_m1 = Reaction('Replic_deg_m1',[pcfm1],[],1,mv) 
    Polyp_transl_m1 = Reaction('Polyp_transl_m1',[Rm1],[polypm1],0,kt) 
    Polyp_deg_m1 = Reaction('Polyp_deg_m1',[polypm1],[],1,mpolyp) 
    RNA_prod_w_m1 = Reaction('RNA_prod_w_m1',[Vm1],[R],0,kout_vm_v) 
         
    RNA_prod_m2_w = Reaction('RNA_prod_m2_w',[V],[Rm2],0,kout_v_vm) 
    Vesicle_form_m2 = Reaction('Vesicle_form_m2',[pcfm2, Rm2],[Vm2],1,kv)
    Vesicle_deg_m2 = Reaction('Vesicle_deg_m2',[Vm2],[],1,mv) 
    RNA_prod_m2 = Reaction('RNA_prod_m2',[Vm2],[Rm2],0,kout) 
    RNA_deg_m2 = Reaction('RNA_deg_m2',[Rm2],[],1,mr) 
    Polyp_proc_m2 = Reaction('Polyp_proc_m2',[polypm2],[pm2],1,kcm) 
    Replic_form_m2 = Reaction('Replic_form_m2',[cf,pm2],[pcfm2],1,kp) 
    P_deg_m2 = Reaction('P_deg_m2',[pm2],[],1,mp) 
    Replic_deg_m2 = Reaction('Replic_deg_m2',[pcfm2],[],1,mv) 
    Polyp_transl_m2 = Reaction('Polyp_transl_m2',[Rm2],[polypm2],0,kt) 
    Polyp_deg_m2 = Reaction('Polyp_deg_m2',[polypm2],[],1,mpolyp) 
    RNA_prod_w_m2 = Reaction('RNA_prod_w_m2',[Vm2],[R],0,kout_vm_v) 
         
         
    RNA_prod_m3_w = Reaction('RNA_prod_m3_w',[V],[Rm3],0,kout_v_vm) 
    Vesicle_form_m3 = Reaction('Vesicle_form_m3',[pcfm3, Rm3],[Vm3],1,kv)
    Vesicle_deg_m3 = Reaction('Vesicle_deg_m3',[Vm3],[],1,mv) 
    RNA_prod_m3 = Reaction('RNA_prod_m3',[Vm3],[Rm3],0,kout) 
    RNA_deg_m3 = Reaction('RNA_deg_m3',[Rm3],[],1,mr) 
    Polyp_proc_m3 = Reaction('Polyp_proc_m3',[polypm3],[pm3],1,kcm) 
    Replic_form_m3 = Reaction('Replic_form_m3',[cf,pm3],[pcfm3],1,kp) 
    P_deg_m3 = Reaction('P_deg_m3',[pm3],[],1,mp) 
    Replic_deg_m3 = Reaction('Replic_deg_m3',[pcfm3],[],1,mv) 
    Polyp_transl_m3 = Reaction('Polyp_transl_m3',[Rm3],[polypm3],0,kt) 
    Polyp_deg_m3 = Reaction('Polyp_deg_m3',[polypm3],[],1,mpolyp) 
    RNA_prod_w_m3 = Reaction('RNA_prod_w_m3',[Vm3],[R],0,kout_vm_v) 
         
    Virion_form_w = Reaction('Virion_form_w',[R],[Vir],1,vf) 
    Virion_form_m1 = Reaction('Virion_form_m1',[Rm1],[Virm1],1,vf) 
    Virion_form_m2 = Reaction('Virion_form_m2',[Rm2],[Virm2],1,vf) 
    Virion_form_m3 = Reaction('Virion_form_m3',[Rm3],[Virm3],1,vf) 
         
    Virion_deg_w = Reaction('Virion_deg_w',[Vir],[],1,mvir) 
    Virion_deg_m1 = Reaction('Virion_deg_m1',[Virm1],[],1,mvir) 
    Virion_deg_m2 = Reaction('Virion_deg_m2',[Virm2],[],1,mvir) 
    Virion_deg_m3 = Reaction('Virion_deg_m3',[Virm3],[],1,mvir) 
         
    Virion_out_w = Reaction('Virion_out_w',[Vir],[Vir_out],1,vout) 
    Virion_out_m1 = Reaction('Virion_out_m1',[Virm1],[Virm1_out],1,vout) 
    Virion_out_m2 = Reaction('Virion_out_m2',[Virm2],[Virm2_out],1,vout) 
    Virion_out_m3 = Reaction('Virion_out_m3',[Virm3],[Virm3_out],1,vout) 
    
    example=Gillespie([Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3,Virion_out_w,Virion_out_m1,Virion_out_m2,Virion_out_m3,Virion_form_w, Virion_form_m1, Virion_form_m2, Virion_form_m3,Vesicle_form_w,	Vesicle_form_m1,	Vesicle_form_m2,	Vesicle_form_m3,	Vesicle_deg_w,	Vesicle_deg_m1,	Vesicle_deg_m2,	Vesicle_deg_m3,	RNA_prod_w,	RNA_prod_m1,	RNA_prod_m2,	RNA_prod_m3,	RNA_deg_w,	RNA_deg_m1,	RNA_deg_m2,	RNA_deg_m3,	Polyp_proc_w,	Polyp_proc_m1,	Polyp_proc_m2,	Polyp_proc_m3,	Replic_form_w,	Replic_form_m1,	Replic_form_m2,	Replic_form_m3,	P_deg_w,	P_deg_m1,	P_deg_m2,	P_deg_m3,	Replic_deg_w,	Replic_deg_m1,	Replic_deg_m2,	Replic_deg_m3,	Polyp_transl_w,	Polyp_transl_m1,	Polyp_transl_m2,	Polyp_transl_m3,	Polyp_deg_w,	Polyp_deg_m1,	Polyp_deg_m2,	Polyp_deg_m3,	RNA_prod_w_m1,	RNA_prod_w_m2,	RNA_prod_w_m3,	RNA_prod_m1_w,	RNA_prod_m2_w,	RNA_prod_m3_w,	CF_prod,	CF_deg],
                      [V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3,Vir,Virm1,Virm2,Virm3,Vir_out,Virm1_out,Virm2_out,Virm3_out])


    time_of_death = (1/0.02)*math.log((1/random.random()),math.e)

    output = example.run(time_of_death)
    return output    
    
    

t0 = time.clock()



curves_w = {}
curves_m1 = {}
curves_m2 = {}
curves_m3 = {}
k = k1 = k2 = k3 = 0

with open('D:\Work\HCV\Curves\w_list.txt','r') as f:
    for line in f.readlines():
        curves_w[k] = line.replace('\n','')
        k += 1
with open('D:\Work\HCV\Curves\m1_list.txt','r') as f:
    for line in f.readlines():
        curves_m1[k1] = line.replace('\n','')
        k1 += 1
with open('D:\Work\HCV\Curves\m2_list.txt','r') as f:
    for line in f.readlines():
        curves_m2[k2] = line.replace('\n','')
        k2 += 1
with open('D:\Work\HCV\Curves\m3_list.txt','r') as f:
    for line in f.readlines():
        curves_m3[k3] = line.replace('\n','')
        k3 += 1


        
CH = Species("Healthy cells",1000)
Cinf = Species("Infected wild cells",0)
Cinfm1 = Species("Infected mut1 cells",0)
Cinfm2 = Species("Infected mut2 cells",0)
Cinfm3 = Species("Infected mut3 cells",0)
Vir = Species("Virion wild", 1)
Virm1 = Species("Virion mut1",0)
Virm2 = Species("Virion mut2",0)
Virm3 = Species("Virion mut3",0) 

# CH Cinf Cinfm1 Cinfm2 Cinfm3 Vir Virm1 Virm2 Virm3
# 0   1     2      3      4     5    6     7     8

kinf = 0.001
mch = 0.001
mvir = 0.8
pch = 1

Cell_infection_w = Reaction('Cell_infection_w', [Vir, CH],[Cinf],1, kinf)
Cell_infection_m1 = Reaction('Cell_infection_m1', [Virm1, CH],[Cinfm1],1, kinf)
Cell_infection_m2 = Reaction('Cell_infection_m2', [Virm2, CH],[Cinfm2],1, kinf)
Cell_infection_m3 = Reaction('Cell_infection_m3', [Virm3, CH],[Cinfm3],1, kinf)
CH_formation = Reaction('CH_formation', [],[CH],1, pch)

CH_degradation = Reaction('CH_degradation', [CH],[],1, mch)
#Cinf_degradation = Reaction('Cinf_degradation', [Cinf],[],1, mcinf)
#Cinfm1_degradation = Reaction('Cinfm1_degradation', [Cinfm1],[],1, mcinf)
#Cinfm2_degradation = Reaction('Cinfm2_degradation', [Cinfm2],[],1, mcinf)
#Cinfm3_degradation = Reaction('Cinfm3_degradation', [Cinfm3],[],1, mcinf)
Vir_degradation = Reaction('Vir_degradation', [Vir],[],1, mvir)
Virm1_degradation = Reaction('Virm1_degradation', [Virm1],[],1, mvir)
Virm2_degradation = Reaction('Virm2_degradation', [Virm2],[],1, mvir)
Virm3_degradation = Reaction('Virm3_degradation', [Virm3],[],1, mvir)

ex = Gillespie([CH_formation, Virm3_degradation,Virm2_degradation,Virm1_degradation,Vir_degradation,Cell_infection_w,Cell_infection_m1, Cell_infection_m2, Cell_infection_m3,CH_degradation ],[CH, Cinf, Cinfm1, Cinfm2, Cinfm3, Vir, Virm1, Virm2, Virm3])
#output = ex.run(1000)

#Conc = {}
#for i in range(len(out2[0])):
#    Conc[i] = []
#for i in range(1,len(out2)):
#    for j in range(len(out2[i])):
#        Conc[j].append(out2[i][j])
#        
## V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3
#plt.plot(Conc[0], Conc[4],'g',
#         Conc[0], Conc[10],'r',
#         Conc[0], Conc[15],'b',
#         Conc[0], Conc[20],'y')            
  
if (1):
    N_w = sum(1 for l in open('D:\Work\HCV\Curves\w_list.txt','r')) 
    N_m1 = sum(1 for l in open('D:\Work\HCV\Curves\m1_list.txt','r')) 
    N_m2 = sum(1 for l in open('D:\Work\HCV\Curves\m2_list.txt','r')) 
    N_m3 = sum(1 for l in open('D:\Work\HCV\Curves\m2_list.txt','r')) 
    
    random.seed()
    curves_w = {}
    for i in range(N_w + 1 , N_w + 200):
        curve = HCV_cell_curve(0)  
        way ='D:\Work\HCV\Curves\wild_infected_'+ str(i) +'.txt' 
        with open(way,'w') as f:
            for j in curve:
                f.write(str(j[0])+','+str(j[4]) +','+ str(j[10])+','+ str(j[15])+','+ str(j[20])+','+ str(j[26])+','+ str(j[27])+','+ str(j[28])+','+ str(j[29]) +'\n')    
        curves_w[i] = way
        
    random.seed()
    curves_m1 = {}
    for i in range(N_m1 + 1,N_m1 + 50):
        curve = HCV_cell_curve(1)  
        way ='D:\Work\HCV\Curves\m1_infected_'+ str(i)  +'.txt'
        with open(way,'w') as f:
            for j in curve:
                f.write(str(j[0])+','+str(j[4]) +','+ str(j[10])+','+ str(j[15])+','+ str(j[20])+','+ str(j[26])+','+ str(j[27])+','+ str(j[28])+','+ str(j[29]) +'\n')    
        curves_m1[i] = way
        
    random.seed()
    curves_m2 = {}
    for i in range(N_m2 + 1, N_m2 + 50):
        curve = HCV_cell_curve(2)  
        way ='D:\Work\HCV\Curves\m2_infected_'+ str(i)  +'.txt'
        with open(way,'w') as f:
            for j in curve:
                f.write(str(j[0])+','+str(j[4]) +','+ str(j[10])+','+ str(j[15])+','+ str(j[20])+','+ str(j[26])+','+ str(j[27])+','+ str(j[28])+','+ str(j[29]) +'\n')    
        curves_m2[i] = way
        
    random.seed()
    curves_m3 = {}
    for i in range(N_m3 + 1,N_m3 + 50):
        curve = HCV_cell_curve(3)  
        way ='D:\Work\HCV\Curves\m3_infected_'+ str(i)  +'.txt'
        with open(way,'w') as f:
            for j in curve:
                f.write(str(j[0])+','+str(j[4]) +','+ str(j[10])+','+ str(j[15])+','+ str(j[20])+','+ str(j[26])+','+ str(j[27])+','+ str(j[28])+','+ str(j[29]) +'\n')    
        curves_m3[i] = way
        
        
    with open('D:\Work\HCV\Curves\w_list.txt','a') as f:
        for i in curves_w:
           f.write(str(curves_w[i]) +'\n')
    with open('D:\Work\HCV\Curves\m1_list.txt','a') as f:
        for i in curves_m1:
           f.write(str(curves_m1[i]) +'\n')
    with open('D:\Work\HCV\Curves\m2_list.txt','a') as f:
        for i in curves_m2:
           f.write(str(curves_m2[i]) +'\n')
    with open('D:\Work\HCV\Curves\m3_list.txt','a') as f:
        for i in curves_m3:
           f.write(str(curves_m3[i]) +'\n')
       
print time.clock() - t0, "seconds process time" 
