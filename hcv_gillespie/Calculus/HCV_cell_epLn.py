# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 16:55:30 2015

@author: PopikOV
"""

import random
import math
#import matplotlib.pyplot as plt
from time import clock
import numpy as np

from numpy import array

##from numpy import prod
#from operator import mul
#def prod(iterable):
#    return reduce(mul, iterable, 1)


debug=1  # 0-nothing  1- simple test 2-test of max RNA count

def combination(n,r): ## implements nCr
    numerator = 1.0
    denominator = 1.0

    for i in range(1,r):
        numerator = numerator * (n-i)
        denominator = denominator * i

    return numerator/denominator

class Species:

    def __init__(self, name=None, quantity=0):
        self.name=name
        self.quantity=quantity

class Reaction:

    #def __init__(self, reactants=[], products=[], rate=0):
    def __init__(self, name=None, reactants=[], products=[],r_type=0, rate=0):
        self.name=name
        self.reactants=reactants
        self.products=products
        self.r_type=r_type
        self.rate=rate
        
class Gillespie_2:

    def __init__(self, reactions=[], all_species=[]):
        
        self.Nreactions = len(reactions)
        self.Nspecies   = len(all_species)
        
        self.all_species_s = [s.name for s in all_species]
        self.all_species_qs = array([s.quantity for s in all_species],dtype='float')
        all_species_iD = {self.all_species_s[j]:j for j in range(self.Nspecies)}
        
        self.reaction_names=[r.name for r in reactions]
        self.reaction_reactant_ixs=[array([all_species_iD[s.name] for s in r.reactants],dtype='int') for r in reactions]
        self.reaction_product_ixs=[array([all_species_iD[s.name] for s in r.products],dtype='int') for r in reactions]
        self.reaction_r_types = array([r.r_type for r in reactions],dtype='int')
        self.reaction_rates = array([r.rate for r in reactions],dtype='float')
        
        
        self.calculate_parameters()
 
    def calculate_parameters(self):
        self.a_mu = np.zeros((self.Nreactions,),'float');
        reactionj=0
        while(reactionj < self.Nreactions)  :
#            reaction = self.reactions[reactionj]
#            h=1.0
            h = np.prod(self.all_species_qs[self.reaction_reactant_ixs[reactionj]])
#            for q in [self.all_species_qs[i] for i in self.reaction_reactant_ixs[reactionj]]:
#              h=h*q
              
            self.a_mu[reactionj] = h*self.reaction_rates[reactionj]
            reactionj = reactionj+1
        self.a_0 = sum(self.a_mu)
#        raise ValueError()
#        """
#          This code is EXTREMELY deeply HACKED. 
#          
#          - Do not use reactions of two or more As 
#          
#        """
#    def calculate_parameters(self):
#        self.a_mu = []#[0.0]*self.Nreactions
#        self.a_0 = 0.0
#        for reaction in self.reactions:
#            h=1
#            for reactant in reaction.reactants: 
#              h=h*reactant.quantity
#            self.a_mu.append(h*reaction.rate)
#            self.a_0=self.a_0+h*reaction.rate
#        



########################

#        self.h_mu = [0.0]*self.Nreactions
#        for reactionj in range(len(self.reactions)):
#        while(reactionj < self.Nreactions)  :
#            print('**********************************')
#            reaction = self.reactions[reactionj]
#            self.a_mu[reactionj] = h*reaction.rate
#            reactionj = reactionj+1
#            reaction_types={} ## Holds how many reactants are of the same type in 1 reaction
                
            
#              reaction_types[reactant.name]=reactant.quantity
#              try:
#                reaction_types[reactant]=reaction_types[reactant]+1
#              except:
                    
#            for key in reaction_types:
#                if(key.quantity==0):
#                  h=0
##                  break
#                else:
#                h=h*reaction_types[key]#key.quantity
#                print(key+' %i'%reaction_types[key])
#                print(key.name+' %i'%key.quantity)
                
#            self.h_mu[reactionj] = h
          
#        raise ValueError(p'DEBUG stop, ep-initiated')

    def run_2(self, Global_time=10, verbose=True):

        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        t=0.0

        output_first_line.append("Time")
#        for one_species in self.all_species:
#            output_first_line.append(one_species.name)
        for s in self.all_species_s:
            output_single.append(s)

        
        if verbose:
            to_print=""
            for string in output_first_line:
                to_print=to_print+string+"\t"

        output_single=[]
        output_single=[t]
        
#        for one_species in self.all_species:
#            output_single.append(one_species.quantity)
        for q in self.all_species_qs:
            output_single.append(q)

        
        if verbose:
            to_print=""
            for string in output_single:
                to_print=to_print+str(string)+"\t"

            #print to_print

        output.append(output_single)

        ##Start simulation
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0

        while t < Global_time:
            #print self.a_0
            
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1.0/self.a_0)*math.log((1.0/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            
            # Number of reaction to happen
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]

            ## ith reaction will occur
#            

#            self.all_species[28].quantity = 0 
#            self.all_species[27].quantity = 0 
#            self.all_species[26].quantity = 0 
#            self.all_species[25].quantity = 0             
            self.all_species_qs[28] = 0 
            self.all_species_qs[27] = 0 
            self.all_species_qs[26] = 0 
            self.all_species_qs[25] = 0             
  
                  
                   
            #one_species.quantity=one_species.quantity-1
            #print  self.reactions[i].r_type
            
            if self.reaction_r_types[i] == 1:                
                for ix in self.reaction_reactant_ixs[i]:
                  self.all_species_qs[ix]=self.all_species_qs[ix]-1
                    # one_species.quantity=one_species.quantity-1

#            for one_species in self.reactions[i].products:
#                one_species.quantity=one_species.quantity+1
            for ix in self.reaction_product_ixs[i]:
              self.all_species_qs[ix]=self.all_species_qs[ix]+1

            
            ## Output
            output_single=[t]
        
#            for one_species in self.all_species:
#                output_single.append(one_species.quantity)
            for q in self.all_species_qs:
                output_single.append(q)
            
#            if self.all_species[3].quantity == 0 and self.all_species[9].quantity == 0 and self.all_species[14].quantity == 0 and self.all_species[19].quantity == 0  :
#                t = Global_time
            if self.all_species_qs[3] == 0 and self.all_species_qs[9] == 0 and self.all_species_qs[14] == 0 and self.all_species_qs[19] == 0  :
                t = Global_time
                  
            
            if verbose:
                to_print=""
                for string in output_single:
                    to_print=to_print+str(string)+"\t"

                #print to_print

            output.append(output_single)

            ##Recalculate parameters -- Procedure can be optimized

            self.calculate_parameters()
        
        return output
        
def HCV_cell_curve(ind,X0,timer=0,HCV_Pars = [ 0.363    , 0.05         , 4          , 1      , 0.0005    , 0.25      ]):
    degrRate = HCV_Pars[0]
    virFormation = HCV_Pars[1]
    cellFactor= HCV_Pars[2]
    virOut = HCV_Pars[3]
#    if(timer):
    tStart = clock()
#    random.seed()

    ## Example Reaction: Isomerization reaction from Gillespie's paper

    ## X ---c---> Z

    V = Species("Vis_w",X0[0])
    p = Species("Proteins_w", X0[1])    
    pcf = Species("Cell factor + proteins_w",X0[2])
    R = Species("RNA_w",X0[3])
    polyp = Species("Polyprotein_w",X0[4])
    cf = Species("Cell factor",X0[5])
        
    Vm1 = Species("Vis_m1", X0[6])
    pm1 = Species("Proteins_m1", X0[7])    
    pcfm1 = Species("Cell factor + proteins_m1",X0[8])
    Rm1 = Species("RNA_m1",X0[9])
    polypm1 = Species("Polyprotein_m1",X0[10])

    Vm2 = Species("Vis_m2", X0[11])
    pm2 = Species("Proteins_m2", X0[12])    
    pcfm2 = Species("Cell factor + proteins_m2",X0[13])
    Rm2 = Species("RNA_m2",X0[14])
    polypm2 = Species("Polyprotein_m2",X0[15])
    
    Vm3 = Species("Vis_m3", X0[16])
    pm3 = Species("Proteins_m3",X0[17])    
    pcfm3 = Species("Cell factor + proteins_m3",X0[18])
    Rm3 = Species("RNA_m3",X0[19])
    polypm3 = Species("Polyprotein_m3",X0[20])
    
    Vir = Species("Virion_wild",X0[21])   
    Virm1 = Species("Virion_m1",X0[22]) 
    Virm2 = Species("Virion_m2",X0[23]) 
    Virm3 = Species("Virion_m3",X0[24]) 
    
    Vir_out = Species("Virion_wild_out",X0[25])   
    Virm1_out = Species("Virion_m1_out",X0[26]) 
    Virm2_out = Species("Virion_m2_out",X0[27]) 
    Virm3_out = Species("Virion_m3_out",X0[28]) 
    
    kv = 3
    mv = 0.0578
    kout = 1.177
    mr = 0.5 # Degradation rate
    #mr = 0.363 # Degradation rate
    mr = 0.2 # Degradation rate
    mr = degrRate
    kt = 0.1
    kc = 1
    kcm = 0.42
    kp = 0.75
    kcf = 4 # Cell Factor
    kcf = cellFactor
    mp = 1
    mpolyp = 0.3 
    kout_v_vm = 0.0002
    kout_vm_v = 0.0002
    vf = 0.05 # Virion Formation
    vf = 0.5 # Virion Formation
    vf = virFormation
    mcf = 1
    mvir = 0
    vout = 1
    vout = virOut
          
    Polyp_transl_w = Reaction('Polyp_transl_w',[R],[polyp],0,kt) 
    Vesicle_form_w = Reaction('Vesicle_form_w',[pcf, R],[V],1,kv)
    Vesicle_deg_w = Reaction('Vesicle_deg_w',[V],[],1,mv) 
    CF_prod = Reaction('CF_prod',[],[cf],1,kcf) 
    CF_deg = Reaction('CF_deg',[cf],[],1,mcf) 
    Polyp_proc_w = Reaction('Polyp_proc_w',[polyp],[p],1,kc) 
    Replic_form_w = Reaction('Replic_form_w',[cf,p],[pcf],1,kp) 
    RNA_deg_w = Reaction('RNA_deg_w',[R],[],1,mr) 
    P_deg_w = Reaction('P_deg_w',[p],[],1,mp) 
    Replic_deg_w = Reaction('Replic_deg_w',[pcf],[],1,mv) 
    Polyp_deg_w = Reaction('Polyp_deg_w',[polyp],[],1,mpolyp) 
    RNA_prod_w = Reaction('RNA_prod_w',[V],[R],0,kout) 
         
    RNA_prod_m1_w = Reaction('RNA_prod_m1_w',[V],[Rm1],0,kout_v_vm) 
    Vesicle_form_m1 = Reaction('Vesicle_form_m1',[pcfm1, Rm1],[Vm1],1,kv)
    Vesicle_deg_m1 = Reaction('Vesicle_deg_m1',[Vm1],[],1,mv) 
    RNA_prod_m1 = Reaction('RNA_prod_m1',[Vm1],[Rm1],0,kout) 
    RNA_deg_m1 = Reaction('RNA_deg_m1',[Rm1],[],1,mr) 
    Polyp_proc_m1 = Reaction('Polyp_proc_m1',[polypm1],[pm1],1,kcm) 
    Replic_form_m1 = Reaction('Replic_form_m1',[cf,pm1],[pcfm1],1,kp) 
    P_deg_m1 = Reaction('P_deg_m1',[pm1],[],1,mp) 
    Replic_deg_m1 = Reaction('Replic_deg_m1',[pcfm1],[],1,mv) 
    Polyp_transl_m1 = Reaction('Polyp_transl_m1',[Rm1],[polypm1],0,kt) 
    Polyp_deg_m1 = Reaction('Polyp_deg_m1',[polypm1],[],1,mpolyp) 
    RNA_prod_w_m1 = Reaction('RNA_prod_w_m1',[Vm1],[R],0,kout_vm_v) 
         
    RNA_prod_m2_w = Reaction('RNA_prod_m2_w',[V],[Rm2],0,kout_v_vm) 
    Vesicle_form_m2 = Reaction('Vesicle_form_m2',[pcfm2, Rm2],[Vm2],1,kv)
    Vesicle_deg_m2 = Reaction('Vesicle_deg_m2',[Vm2],[],1,mv) 
    RNA_prod_m2 = Reaction('RNA_prod_m2',[Vm2],[Rm2],0,kout) 
    RNA_deg_m2 = Reaction('RNA_deg_m2',[Rm2],[],1,mr) 
    Polyp_proc_m2 = Reaction('Polyp_proc_m2',[polypm2],[pm2],1,kcm) 
    Replic_form_m2 = Reaction('Replic_form_m2',[cf,pm2],[pcfm2],1,kp) 
    P_deg_m2 = Reaction('P_deg_m2',[pm2],[],1,mp) 
    Replic_deg_m2 = Reaction('Replic_deg_m2',[pcfm2],[],1,mv) 
    Polyp_transl_m2 = Reaction('Polyp_transl_m2',[Rm2],[polypm2],0,kt) 
    Polyp_deg_m2 = Reaction('Polyp_deg_m2',[polypm2],[],1,mpolyp) 
    RNA_prod_w_m2 = Reaction('RNA_prod_w_m2',[Vm2],[R],0,kout_vm_v) 
         
         
    RNA_prod_m3_w = Reaction('RNA_prod_m3_w',[V],[Rm3],0,kout_v_vm) 
    Vesicle_form_m3 = Reaction('Vesicle_form_m3',[pcfm3, Rm3],[Vm3],1,kv)
    Vesicle_deg_m3 = Reaction('Vesicle_deg_m3',[Vm3],[],1,mv) 
    RNA_prod_m3 = Reaction('RNA_prod_m3',[Vm3],[Rm3],0,kout) 
    RNA_deg_m3 = Reaction('RNA_deg_m3',[Rm3],[],1,mr) 
    Polyp_proc_m3 = Reaction('Polyp_proc_m3',[polypm3],[pm3],1,kcm) 
    Replic_form_m3 = Reaction('Replic_form_m3',[cf,pm3],[pcfm3],1,kp) 
    P_deg_m3 = Reaction('P_deg_m3',[pm3],[],1,mp) 
    Replic_deg_m3 = Reaction('Replic_deg_m3',[pcfm3],[],1,mv) 
    Polyp_transl_m3 = Reaction('Polyp_transl_m3',[Rm3],[polypm3],0,kt) 
    Polyp_deg_m3 = Reaction('Polyp_deg_m3',[polypm3],[],1,mpolyp) 
    RNA_prod_w_m3 = Reaction('RNA_prod_w_m3',[Vm3],[R],0,kout_vm_v) 
         
    Virion_form_w = Reaction('Virion_form_w',[R],[Vir],1,vf) 
    Virion_form_m1 = Reaction('Virion_form_m1',[Rm1],[Virm1],1,vf) 
    Virion_form_m2 = Reaction('Virion_form_m2',[Rm2],[Virm2],1,vf) 
    Virion_form_m3 = Reaction('Virion_form_m3',[Rm3],[Virm3],1,vf) 
         
    Virion_deg_w = Reaction('Virion_deg_w',[Vir],[],1,mvir) 
    Virion_deg_m1 = Reaction('Virion_deg_m1',[Virm1],[],1,mvir) 
    Virion_deg_m2 = Reaction('Virion_deg_m2',[Virm2],[],1,mvir) 
    Virion_deg_m3 = Reaction('Virion_deg_m3',[Virm3],[],1,mvir) 
         
    Virion_out_w = Reaction('Virion_out_w',[Vir],[Vir_out],1,vout) 
    Virion_out_m1 = Reaction('Virion_out_m1',[Virm1],[Virm1_out],1,vout) 
    Virion_out_m2 = Reaction('Virion_out_m2',[Virm2],[Virm2_out],1,vout) 
    Virion_out_m3 = Reaction('Virion_out_m3',[Virm3],[Virm3_out],1,vout) 
#  Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3,  
    example_2=Gillespie_2([Virion_out_w,Virion_out_m1,Virion_out_m2,Virion_out_m3,Virion_form_w, Virion_form_m1, Virion_form_m2, Virion_form_m3,Vesicle_form_w,	Vesicle_form_m1,	Vesicle_form_m2,	Vesicle_form_m3,	Vesicle_deg_w,	Vesicle_deg_m1,	Vesicle_deg_m2,	Vesicle_deg_m3,	RNA_prod_w,	RNA_prod_m1,	RNA_prod_m2,	RNA_prod_m3,	RNA_deg_w,	RNA_deg_m1,	RNA_deg_m2,	RNA_deg_m3,	Polyp_proc_w,	Polyp_proc_m1,	Polyp_proc_m2,	Polyp_proc_m3,	Replic_form_w,	Replic_form_m1,	Replic_form_m2,	Replic_form_m3,	P_deg_w,	P_deg_m1,	P_deg_m2,	P_deg_m3,	Replic_deg_w,	Replic_deg_m1,	Replic_deg_m2,	Replic_deg_m3,	Polyp_transl_w,	Polyp_transl_m1,	Polyp_transl_m2,	Polyp_transl_m3,	Polyp_deg_w,	Polyp_deg_m1,	Polyp_deg_m2,	Polyp_deg_m3,	RNA_prod_w_m1,	RNA_prod_w_m2,	RNA_prod_w_m3,	RNA_prod_m1_w,	RNA_prod_m2_w,	RNA_prod_m3_w,	CF_prod,	CF_deg, Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3],
                      [V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3,Vir,Virm1,Virm2,Virm3,Vir_out,Virm1_out,Virm2_out,Virm3_out])


    time_of_death = (1/0.005)*math.log((1/random.random()),math.e)
    if time_of_death > 400:
        time_of_death = 400
    
#    time_of_death = 100    
        
    output_2 = example_2.run_2(time_of_death)
  
        
#    print( 'done in %.5fs'%(clock()-tStart))
    if(timer):
      return output_2 ,clock()-tStart
    else:
      return output_2 
    
def runTest(debug,NRT=100):    
  if(debug==1):
#    from numpy import mean,std
    random.seed(0)
    tX0 = [0]*29
    tX0[3]=1000
    NRT = NRT
    ts = [0.0]*NRT
    for j in range(NRT):
      a,t = HCV_cell_curve(0,tX0,timer=1)
      print('%i: %f'%(j,t))
      ts[j]=t
    print('mean:')
    print(sum(ts)/NRT)
    print('std:')
    print(np.std(ts))
    print('%f+-%f'%(np.mean(ts),np.std(ts)))
  elif(debug==2):  
    startRNA = 5 
    
    
    tX0 = [0]*29
    tX0[3]=startRNA
    
    NmoreTstart = 0  
    times=[]
    
#    NT= 1000
    NT=NRT
    virions_out = 0
    for j in range(NT):
      a,t = HCV_cell_curve(0,tX0,timer=1)
      maxRNA = max([t[4] for t in a ])
      virions_out = virions_out + sum([t[26] for t in a ])
         
      if(maxRNA>startRNA):
        print('__')
        print('max RNA = %i'%maxRNA)
        print('max t = %f'%max([t[0] for t in a]))
        NmoreTstart=NmoreTstart+1
      times.append(t)
    virions_out = virions_out/NT
    print ('virions_out by one cell:%f'%virions_out)
    print('time=%.3f+-%.3f'%(np.mean(times),np.std(times)))  
    print('total time: %f'%sum(times))
    print('__')  
    print('RNA count > start: %i / %i times, %i%%'%(NmoreTstart,NT,NmoreTstart*100.0/NT))