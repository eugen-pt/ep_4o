#ifndef _code
#define _code

#include <vector>

std::vector<std::vector<float> > HCV_cell_curve_c(std::vector<float> HCV_Pars,std::vector<float> X0,float _time_of_death );
std::vector<std::vector<float> > model_Gillespie_c(int Nspecies,std::vector<float> all_species_qs,int Nreactions,std::vector<float> reaction_rates,std::vector<int> reaction_r_types,std::vector<int> reaction_Nreactants,std::vector<std::vector<int> > reaction_reactant_ixs,std::vector<int> reaction_Nproducts,std::vector<std::vector<int> > reaction_product_ixs,float time_of_death);

#endif