## Gillespie's algorithm, implemented in Python by Paras Chopra (www.paraschopra.com)

## Implementation of Gillespie's algorithm

import random, math
#import matplotlib.pyplot as plt
import time
import numpy

from numpy import shape,sqrt,array

from HCV_cell_2_SM import *
from HCV_cell_inhibitors_2_SM import HCV_cell_inhibitor_curve

debug=1
debugplot = 1

debugfname = ''

def debugWrite(t,s):
  with open(debugfname,'a') as f:
    f.write('%f\t%s\n'%(t,s))
#  print('%f\t%s\n'%(t,s))
 
def add_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,inhibitionTime=500,maxTOD=1000,HCV_Pars = [ 0.05         , 4        , 1    , 0.0005    , 0.25    ]):
    Curing_curves = {}
    for curves in Infected_Curves:
#        X0_new = []
#        X_names = ["Vis_w",	"Proteins_w",	"Cell factor + proteins_w",	"RNA_w",	"Polyprotein_w",	"Cell factor",	"Vis_m1",	"Proteins_m1",	"Cell factor + proteins_m1",	"RNA_m1",	"Polyprotein_m1",	"Vis_m2",	"Proteins_m2",	"Cell factor + proteins_m2",	"RNA_m2",	"Polyprotein_m2",	"Vis_m3",	"Proteins_m3",	"Cell factor + proteins_m3",	"RNA_m3",	"Polyprotein_m3",	"Virion_wild",	"Virion_m1",	"Virion_m2",	"Virion_m3",	"Virion_wild_out",	"Virion_m1_out",	"Virion_m2_out",	"Virion_m3_out",	"RNA_supa_muta",	"Polyprotein\inh_NC_w",	"Polyprotein\inh_NC_m1",	"Polyprotein\inh_C_m1",	"Polyprotein\inh_NC_m2",	"Polyprotein\inh_C_m2",	"Polyprotein\inh_NC_m3",	"Polyprotein\inh_C_m3",	"Polyprotein\inh_C_w",	"Vis_ms",	"Proteins_ms",	"Cell factor + proteins_ms",	"Polyprotein_ms",	"Virion_ms",	"Virion_ms_out",]
#        for i in X_names:
#            if i in Infected_Curves[curves][3]:
#                X0_new.append(Infected_Curves[curves][3][i])
#            else:
#                X0_new.append(0)
#        del X0_new[0]
        X0_new =  Infected_Curves[curves][3]      
#        print(X0_new)
        out_end,curve_after_inhibition = HCV_cell_inhibitor_curve(Infected_Curves[curves][2],inhibitorType,X0_new,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
        Curing_curves[curves] = curve_after_inhibition,inhibitionTime,Infected_Curves[curves][2],out_end
        
    return Curing_curves

def stop_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,stopInhibitionTime=500,maxTOD=300,HCV_Pars = [ 0.05         , 4        , 1    , 0.0005    , 0.25    ]):
    New_curves = {}
    for curves in Infected_Curves:
        # dictionary with all last values
        X0_new =  Infected_Curves[curves][3]      
#        print(X0_new)
        out_end,curve_after_inhibition = HCV_cell_curve(Infected_Curves[curves][2],X0_new,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
        New_curves[curves] = curve_after_inhibition,stopInhibitionTime,Infected_Curves[curves][2],out_end
        
    return New_curves
    
class Gillespie:

    def __init__(self, reactions=[], all_species=[],inhibitionTime=500,stopInhibitionTime=600,NCells=1000):
        self.reactions=reactions
        self.all_species=all_species
        self.Nreactions=len(reactions)
        ## set initial values
        self.inhibitionTime = inhibitionTime
        self.stopInhibitionTime=stopInhibitionTime

        self.NCells=NCells        
        
        self.species_D = {s.name:s for s in all_species}
        
        
        self.calculate_parameters()
        
        
        #print self.reactions,'gill react'
    def set_q(self,name,q):
        self.species_D[name].quantity=q
    def add_q(self,name,dq):
        self.species_D[name].quantity=self.species_D[name].quantity+dq
        
    def calculate_parameters(self):
        
        self.h_mu = []
        self.a_mu = []
        self.a_0 = 0.0


        self.all_species[0].quantity = self.NCells - self.all_species[1].quantity - self.all_species[2].quantity - self.all_species[3].quantity - self.all_species[4].quantity - self.all_species[5].quantity
            
            
        for reaction in self.reactions:
            reaction_types={} ## Holds how many reactants are of the same type in 1 reaction
            
            for reactant in reaction.reactants: 
                if not reaction_types.has_key(reactant):
                    reaction_types[reactant]=1
                else:
                    reaction_types[reactant]=reaction_types[reactant]+1
            
            h=1

            for key in reaction_types.keys():
                h=h*key.quantity
            
            #print h,'h'
            
            self.h_mu.append(h)
            #print reaction.rate,'reaction.rate'
            
            self.a_mu.append(h*reaction.rate)            
            self.a_0=self.a_0+h*reaction.rate
            #print self.a_0, 2
            
        #print self.a_0, 3

    def run(self, Global_time=10, inhibitorType=1, verbose=True,HCV_Pars = [0.05         , 4        , 1    , 0.0005    , 0.25    ]):

        print([s.quantity for s in self.all_species])
        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        t=0.0

        output_first_line.append("Time")
        for one_species in self.all_species:
            output_first_line.append(one_species.name)
        for s in ['RNA_w','RNA_m1','RNA_m2','RNA_m3','RNA_supa_muta']:
            output_first_line.append(s)
        
        if verbose:
            to_print=""
            for string in output_first_line:
                to_print=to_print+string+"\t"

            #print to_print

        output.append(output_first_line)

        ## Output at time=0
        output_single=[]
        output_single=[t]
        
        for one_species in self.all_species:
            output_single.append(one_species.quantity)
        for j in range(4):
            output_single.append(0)
        
        if verbose:
            to_print=""
            for string in output_single:
                to_print=to_print+str(string)+"\t"

            #print to_print

        output.append(output_single)

        ##Start simulation
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0
        Infected_Curves = {}
        Inf_curves_counter = 0
        changer = 0
        
        print_t = -100
        print_dt = 0.5
        
        firstRun = 1

        while t < Global_time:
#            print('---step')    
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1/self.a_0)*math.log((1/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]

            NStartRNA = 5
            if firstRun:#len(Infected_Curves) == 0:
#                print('BEGINNING'  )
                NStartRNA = 50
                i = 0
                
                firstRun=0
#                i=1
#                self.all_species[i+5].quantity=1
                
            if(verbose):
                if(t-print_t>print_dt):
                    print('time=%.4f/%.4f\t%s'%(t,Global_time,self.reactions[i].name))
                    print('['+','.join(['%6i'%s.quantity for s in self.all_species])+']')
#                    print_t = t
            debugWrite(t,self.reactions[i].name)
            X0 = {}
            
            oInf_curves_counter=Inf_curves_counter
#            if len(Infected_Curves) < 1:                
            if t < self.inhibitionTime: #BEFORE Inhibition             
                TIME_BEFORE_Inhibition = self.inhibitionTime - t+0.05
                if self.reactions[i].name =='Cell_infection_w' :
                    X0['RNA_w']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(0, X0,timer = 0, HCV_Pars = HCV_Pars,maxTOD= TIME_BEFORE_Inhibition)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0, out_end
                    Inf_curves_counter += 1
                
 
                if self.reactions[i].name =='Cell_infection_m1':    
                    X0['RNA_m1']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(1, X0,HCV_Pars = HCV_Pars,maxTOD= TIME_BEFORE_Inhibition)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1,out_end
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m2':    
#                    X0[14] = NStartRNA
                    X0['RNA_m2']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(2,X0,HCV_Pars = HCV_Pars,maxTOD= TIME_BEFORE_Inhibition)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2,out_end
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m3':    
#                    X0[19] = NStartRNA
                    X0['RNA_m3']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(3, X0,HCV_Pars = HCV_Pars,maxTOD= TIME_BEFORE_Inhibition)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3,out_end
                    Inf_curves_counter += 1
                    
                if self.reactions[i].name =='Cell_infection_ms':    
#                    X0[29] = NStartRNA
                    X0['RNA_supa_muta']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(4, X0,HCV_Pars = HCV_Pars,maxTOD= TIME_BEFORE_Inhibition)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 4,out_end
                    Inf_curves_counter += 1

            if (t > self.inhibitionTime)and(t<=self.stopInhibitionTime): 
                X0 = {}#[0]*44
                maxTOD = min(Global_time,self.stopInhibitionTime) - t+0.05
                if(verbose):
                    if(t-print_t>print_dt):
                        print 'INHIBITION TIME!!!1'
                if changer == 0:
                    changer = 1
                    debugWrite(t,'Inhibition!!')
                    Infected_Curves = add_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,inhibitionTime=self.inhibitionTime,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
                    
                if self.reactions[i].name =='Cell_infection_w' :
#                    X0[3] = NStartRNA
                    X0['RNA_w']=NStartRNA
                    out_end, infected_curve = HCV_cell_inhibitor_curve(0, inhibitorType, X0,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0,out_end
                    Inf_curves_counter += 1
#                    print 'Cell_infection_w'
 
                if self.reactions[i].name =='Cell_infection_m1':    
#                    X0[9] = NStartRNA
                    X0['RNA_m1']=NStartRNA
                    out_end, infected_curve = HCV_cell_inhibitor_curve(1, inhibitorType,X0,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1,out_end
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m2':    
#                    X0[14] = NStartRNA
                    X0['RNA_m2']=NStartRNA
                    out_end, infected_curve = HCV_cell_inhibitor_curve(2, inhibitorType,X0,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2,out_end
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m3':    
#                    X0[19] = NStartRNA
                    X0['RNA_m3']=NStartRNA
                    out_end, infected_curve = HCV_cell_inhibitor_curve(3, inhibitorType,X0,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3,out_end
                    Inf_curves_counter += 1
                    
                if self.reactions[i].name =='Cell_infection_ms':    
#                    X0[29] = NStartRNA
                    X0['RNA_supa_muta']=NStartRNA
                    out_end, infected_curve = HCV_cell_inhibitor_curve(4, inhibitorType,X0,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 4,out_end
                    Inf_curves_counter += 1
            if t>self.stopInhibitionTime:            
                X0 = {}#[0]*44
                maxTOD = Global_time-t+0.05
                if(verbose):
                    if(t-print_t>print_dt):
                        print 'stopInhibitionTime.'
                if changer == 1:
                    changer = 2
                    debugWrite(t,'Stop Inhibition!!')
                    Infected_Curves = stop_inhibition_to_cell_recalculation(Infected_Curves,inhibitorType,stopInhibitionTime=self.stopInhibitionTime,HCV_Pars = HCV_Pars,maxTOD=maxTOD)
                    
                if self.reactions[i].name =='Cell_infection_w' :
                    X0['RNA_w']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(0, X0,timer = 0, HCV_Pars = HCV_Pars,maxTOD= maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 0, out_end
                    Inf_curves_counter += 1
                
 
                if self.reactions[i].name =='Cell_infection_m1':    
                    X0['RNA_m1']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(1, X0,HCV_Pars = HCV_Pars,maxTOD= maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 1,out_end
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m2':    
#                    X0[14] = NStartRNA
                    X0['RNA_m2']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(2,X0,HCV_Pars = HCV_Pars,maxTOD= maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 2,out_end
                    Inf_curves_counter += 1
    
                if self.reactions[i].name =='Cell_infection_m3':    
#                    X0[19] = NStartRNA
                    X0['RNA_m3']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(3, X0,HCV_Pars = HCV_Pars,maxTOD= maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 3,out_end
                    Inf_curves_counter += 1
                    
                if self.reactions[i].name =='Cell_infection_ms':    
#                    X0[29] = NStartRNA
                    X0['RNA_supa_muta']=NStartRNA
                    out_end, infected_curve = HCV_cell_curve(4, X0,HCV_Pars = HCV_Pars,maxTOD= maxTOD)
                    Infected_Curves[Inf_curves_counter] = infected_curve, t, 4,out_end
                    Inf_curves_counter += 1
            
            if(Inf_curves_counter>oInf_curves_counter):
                try:
                    pass
#                    print("infected with curve of length %i, max time = %f"%(len(Infected_Curves[oInf_curves_counter][0]),Infected_Curves[oInf_curves_counter][0][-1][0]))
                except:
                    print(oInf_curves_counter)
                    print(len(Infected_Curves))
                    print(Infected_Curves[oInf_curves_counter])
                    print(Infected_Curves[oInf_curves_counter][0])
                    print(Infected_Curves[oInf_curves_counter][0][-1])
                    print(Infected_Curves[oInf_curves_counter][0][-1][0])

            vir_w = 0 
            vir_m1 = 0
            vir_m2 = 0
            vir_m3 = 0
            vir_ms = 0
            curves_to_del = []
            curves_to_cure = []
            
            for curve in Infected_Curves:
                time = t - Infected_Curves[curve][1]
                counter = 0
                if len(Infected_Curves[curve][0]) == 0:
                    curves_to_cure.append(curve)                
                else: 
                    if (time > Infected_Curves[curve][0][-1][0]):
                        if (Infected_Curves[curve][0][-1][1] == 0) and (Infected_Curves[curve][0][-1][2] == 0) and(Infected_Curves[curve][0][-1][3] == 0) and (Infected_Curves[curve][0][-1][4] == 0) and (Infected_Curves[curve][0][-1][5] == 0):
                            curves_to_cure.append(curve)
                        else:
                            curves_to_del.append(curve)
                    else:
                        while time > Infected_Curves[curve][0][counter][0]:
                            counter = counter + 1
                                     
                if counter-1 < len(Infected_Curves[curve][0]):
#                    for i3 in range(counter-1,-1,-1): 
#                        vir_w = vir_w + Infected_Curves[curve][0][0][5]
#                        vir_m1 = vir_m1 + Infected_Curves[curve][0][0][6]
#                        vir_m2 = vir_m2 + Infected_Curves[curve][0][0][7]
#                        vir_m3 = vir_m3 + Infected_Curves[curve][0][0][8]
#                        del Infected_Curves[curve][0][0]
                    for i3 in range(counter-1,-1,-1): 
                        vir_w = vir_w + Infected_Curves[curve][0][i3][6]
                        vir_m1 = vir_m1 + Infected_Curves[curve][0][i3][7]
                        vir_m2 = vir_m2 + Infected_Curves[curve][0][i3][8]
                        vir_m3 = vir_m3 + Infected_Curves[curve][0][i3][9]
                        vir_ms = vir_ms + Infected_Curves[curve][0][i3][10]
                        
                        del Infected_Curves[curve][0][i3]

            for curves in curves_to_del:
                
                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
                    self.all_species[1].quantity = self.all_species[1].quantity - 1
                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
                    self.all_species[2].quantity = self.all_species[2].quantity - 1
                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
                    self.all_species[3].quantity = self.all_species[3].quantity - 1
                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
                    self.all_species[4].quantity = self.all_species[4].quantity - 1
                if Infected_Curves[curves][2] == 4 and self.all_species[5].quantity > 0:
                    self.all_species[5].quantity = self.all_species[5].quantity - 1
                
                debugWrite(t,'cur_end')     
                del Infected_Curves[curves]
                
            for curves in curves_to_cure: 
#                    if curve not in Infected_Curves:
#                        print curve ,'cur'
#                        print 'alarm!'
                self.all_species[0].quantity = self.all_species[0].quantity + 1
                if Infected_Curves[curves][2] == 0 and self.all_species[1].quantity > 0:
                    self.all_species[1].quantity = self.all_species[1].quantity - 1
                if Infected_Curves[curves][2] == 1 and self.all_species[2].quantity > 0:
                    self.all_species[2].quantity = self.all_species[2].quantity - 1
                if Infected_Curves[curves][2] == 2 and self.all_species[3].quantity > 0:
                    self.all_species[3].quantity = self.all_species[3].quantity - 1
                if Infected_Curves[curves][2] == 3 and self.all_species[4].quantity > 0:
                    self.all_species[4].quantity = self.all_species[4].quantity - 1
                if Infected_Curves[curves][2] == 4 and self.all_species[5].quantity > 0:
                    self.all_species[5].quantity = self.all_species[5].quantity - 1 
                debugWrite(t,'cur_cure')   
                del Infected_Curves[curves]
                
        # CH Cinf Cinfm1 Cinfm2 Cinfm3 Vir Virm1 Virm2 Virm3
        # 0 1     2    3    4     5    6     7     8   
            if(vir_ms>0):    
                print(vir_w,vir_m1,vir_m2,vir_m3,vir_ms)
                
#            self.all_species[6].quantity = self.all_species[6].quantity + vir_w
#            self.all_species[7].quantity = self.all_species[7].quantity + vir_m1
#            self.all_species[8].quantity = self.all_species[8].quantity + vir_m2
#            self.all_species[9].quantity = self.all_species[9].quantity + vir_m3
#            self.all_species[10].quantity = self.all_species[10].quantity + vir_ms
            
            self.add_q("Virion wild",vir_w)
            self.add_q("Virion mut1",vir_m1)
            self.add_q("Virion mut2",vir_m2)
            self.add_q("Virion mut3",vir_m3)
            self.add_q("Virion muts",vir_ms)
            
           
            RNAcs = [0]*5
            for curvek in Infected_Curves:
                for j in range(5):
                    RNAcs[j] = RNAcs[j]+ Infected_Curves[curvek][0][0][j+1]
            
            if(vir_w>0):
                debugWrite(t,'vir_w_out')
            if(vir_m1>0):
                debugWrite(t,'vir_m1_out')
            if(vir_m2>0):
                debugWrite(t,'vir_m2_out')
            if(vir_m3>0):
                debugWrite(t,'vir_m3_out')
            if(vir_ms>0):
                debugWrite(t,'vir_ms_out')    
                 

            for one_species in self.reactions[i].reactants:
                #one_species.quantity=one_species.quantity-1
                #printself.reactions[i].r_type
 
                if self.reactions[i].r_type == 1:                
                    one_species.quantity=one_species.quantity-1

            for one_species in self.reactions[i].products:                
                if (self.reactions[i].name != 'Vir_out') and (self.reactions[i].name != 'Virm1_out') and (self.reactions[i].name != 'Virm2_out') and (self.reactions[i].name != 'Virm3_out') and (self.reactions[i].name != 'Virms_out'): 
                    one_species.quantity=one_species.quantity+1
            
            ## Output
            output_single=[t]
        
            for one_species in self.all_species:
                output_single.append(one_species.quantity)
                if one_species.quantity < 0:
                    Global_time = t
                    print('someone <0 (%s)'%one_species.name)
                    debugWrite(t,'someone < 0')
            for j in range(4):
                output_single.append(RNAcs[j])
                
                
            if (self.all_species[1].quantity == 0 and 
            self.all_species[2].quantity == 0 and
            self.all_species[3].quantity == 0 and
            self.all_species[4].quantity == 0 and
            self.all_species[5].quantity == 0 and
            self.all_species[6].quantity == 0 and
            self.all_species[7].quantity == 0 and
            self.all_species[8].quantity == 0 and
            self.all_species[9].quantity == 0 and
            self.all_species[10].quantity == 0):
                Global_time = t
            
#            if verbose:
#                to_print=""
#                for string in output_single:
#                    to_print=to_print+str(string)+"\t"
#                print to_print

            output.append(output_single)
            ##Recalculate parameters -- Procedure can be optimized

            self.calculate_parameters()
            if(verbose):
                if(t-print_t>print_dt):
                    print_t=t
        
        return output,Infected_Curves

def average_curve_by_one_inhibitor(inhibitorType,number_of_curves,parametrs_to_optimize,simTime=1000,timer=0,verbose=0,inhibitionTime=500,stopInhibitionTime=600,res_MinT=0,res_MaxT=-1,HCV_Pars = [0.05         , 4        , 1    , 0.0005    , 0.25    ],NCells=1000):
    o_cellInf = HCV_Pars[3]
    o_virDegr = HCV_Pars[4]
    if(res_MaxT<0):
        res_MaxT = simTime
    t0 = clock()
    
    organizm_curves = []
    
    for ik in range(number_of_curves):        
        if(verbose==1):
            print('ik=%i/%i'%(ik+1,number_of_curves))
    
        X1 = [NCells, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
        CH = Species("Healthy cells",X1[0])
        Cinf = Species("Infected wild cells",X1[1])
        Cinfm1 = Species("Infected mut1 cells",X1[2])
        Cinfm2 = Species("Infected mut2 cells",X1[3])
        Cinfm3 = Species("Infected mut3 cells",X1[4])
        Cinfms = Species("Infected muts cells",X1[5])
        
        Vir = Species("Virion wild", X1[6])
        Virm1 = Species("Virion mut1",X1[7])
        Virm2 = Species("Virion mut2",X1[8])
        Virm3 = Species("Virion mut3",X1[9]) 
        Virms = Species("Virion muts",X1[10]) 
        
        kinf = 0.0005
        
        kinf = o_cellInf
        mch = 0.0001
        mvir = 0.25
        mvir = o_virDegr        
        
        pch = 1.7
        virout = 1
        
        Reactions = []
        
        Reactions.append(Reaction('Cell_infection_w', [Vir, CH],[Cinf],1, kinf))
        Reactions.append(Reaction('Cell_infection_m1', [Virm1, CH],[Cinfm1],1, kinf))
        Reactions.append(Reaction('Cell_infection_m2', [Virm2, CH],[Cinfm2],1, kinf))
        Reactions.append(Reaction('Cell_infection_m3', [Virm3, CH],[Cinfm3],1, kinf))
        Reactions.append(Reaction('Cell_infection_ms', [Virms, CH],[Cinfms],1, kinf))
        
        Reactions.append(Reaction('Vir_out_check', [],[],1, virout*4))

    
        Reactions.append(Reaction('Vir_degradation', [Vir],[],1, mvir))
        Reactions.append(Reaction('Virm1_degradation', [Virm1],[],1, mvir))
        Reactions.append(Reaction('Virm2_degradation', [Virm2],[],1, mvir))
        Reactions.append(Reaction('Virm3_degradation', [Virm3],[],1, mvir))
        Reactions.append(Reaction('Virms_degradation', [Virms],[],1, mvir))

        ex = Gillespie(Reactions,
                     [CH, Cinf, Cinfm1, Cinfm2, Cinfm3, Cinfms, Vir, Virm1, Virm2, Virm3, Virms],inhibitionTime=inhibitionTime,stopInhibitionTime=stopInhibitionTime,NCells=NCells)
        output, curves = ex.run(simTime,inhibitorType,verbose=verbose,HCV_Pars = HCV_Pars)
        
#        Conc = {}
#        for i in range(len(output[0])):
#            Conc[i] = []
#        for j in range(1,len(output)):
#            for i in range(len(output[j])):
#                Conc[i].append(output[j][i])


#        plt.plot( Conc[0], Conc[1], 'r',
#                Conc[0], Conc[2], 'b')     
#        organizm_curves[ik]= Conc
        organizm_curves.append( output[1:] )
    
    points = []
    values = []
    grid_x = numpy.linspace(res_MinT,res_MaxT,num = res_MaxT-res_MinT)
    grid_allx =     numpy.linspace(0,simTime,num=simTime+1);
    
    summ = numpy.zeros(len(grid_x))
    
    meanData = numpy.zeros((len(grid_x),len(organizm_curves[0][0])))
    allMeanData = numpy.zeros((len(grid_allx),len(organizm_curves[0][0])))
    print(shape(meanData))
    
    for curve in organizm_curves:
        acurve = numpy.array(curve)
    for j in range(0,shape(acurve)[1]):
        meanData[:,j] = numpy.interp(grid_x,acurve[:,0],acurve[:,j])
        allMeanData[:,j] = numpy.interp(grid_allx,acurve[:,0],acurve[:,j])
    if(timer):
        print clock() - t0, "seconds process time" 

#    res_parnames = ['InfectedTotal','VirionTotal']
#    res_parIxs = [[1,2,3,4],[5,6,7,8]]
#    
#    res_pars = numpy.zeros((len(grid_x),len(res_parnames)))
#    
#    for k in range(len(res_parIxs)):
#        res_pars[:,k] = numpy.sum(meanData[:,[ij+1 for ij in res_parIxs[k]]],axis=1)
    
    for_Return = {'meanData':allMeanData}

    gc.collect()
    return for_Return

    
    
debug=0
if(debug):
    random.seed(0)
    a = average_curve_by_one_inhibitor(1,10,0,simTime=10,verbose=1,timer=1,inhibitionTime=5,res_MinT=0,res_MaxT=10)
    
    pass
    if(debugplot):
        pass
 

HCV_ParNames= ['virFormation','cellFactor','virOut','o_cellInf','o_virDegr']
srcHCV_Pars = [ 0.05         , 4         , 1       , 0.0005    , 0.25    ]
HCV_Pars_min= [ 0.26          ,2.785       , 0.3    , 0.5      , 0.35    ]
HCV_Pars_max= [ 0.2628         ,2.785       , 0.3    , 0.5         ,0.35         ]

HCV_sParNames = ['vf','cf','vo','oinf','ovd'] #short parnames)

HCV_Pars_min = array(HCV_Pars_min)
HCV_Pars_max = array(HCV_Pars_max)


#Target_values = [1000,500]

#SimPars = {'Random_seed':4,'number_of_curves':1,'inhibitorType':1,
#         'simTime':1000,'inhibitionTime':500,'res_MinT':900,'res_MaxT':1000,'NCells':1000}
#

#Ttime=20


random.seed()
def genRandHCV_Pars():
    return array([random.random() for j in range(len(HCV_ParNames))])*(HCV_Pars_max-HCV_Pars_min)+HCV_Pars_min

import gc

def R2_fun(SimPars,HCV_Pars = [ 0.1, 5 , 0.5, 0.0005 , 0.25]):
    returned = average_curve_by_one_inhibitor(SimPars['inhibitorType'],1,0,simTime=SimPars['simTime'],verbose=1,timer=1,inhibitionTime=SimPars['inhibitionTime'],stopInhibitionTime=SimPars['stopInhibitionTime'],res_MinT=SimPars['res_MinT'],res_MaxT=SimPars['res_MaxT'],HCV_Pars = HCV_Pars,NCells = SimPars['NCells'])
    gc.collect()
#    res = returned['res_pars']
    R2 = 0#sum([ sqrt(sum((res[:,j]/Target_values[j]-1)**2)/shape(res)[0])for j in range(len((Target_values))) ])
    for_Return = {'meanData':returned['meanData'],'R2':R2}
    # sum across all res_parametersofsqrt( mean in time ( (value - target_value)/target_value, and all squared ) )
    return for_Return

import datetime
import json 
J=0

rpath = 'outputs/'
rpath_LOGs = 'output_LOGs/'
import os
try:
    os.mkdir(rpath)
except:
    pass    
try:
    os.mkdir(rpath_LOGs)
except:
    pass    


while(J<100):
    
    SimPars = {'inhibitorType':random.randint(1,4),'simTime':1500,'inhibitionTime':500,'stopInhibitionTime':850,'res_MinT':0,'res_MaxT':2,'NCells':500}
    SimParsssNames = {'inhibitorType':'i','simTime':'sT','inhibitionTime':'iT','stopInhibitionTime':'sIT','res_MinT':'rmT','res_MaxT':'rMT','NCells':'NC'}   
    HCV_Pars = genRandHCV_Pars()
    for i in range(1):
        outfname = ''.join(['%s%i'%(SimParsssNames[s],SimPars[s]) for s in SimPars])+'-'+'_'.join(['%s=%.2f'%(HCV_sParNames[j],HCV_Pars[j])for j in range(len(HCV_Pars))])+'_pf_'+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+'r%02i'%random.randint(0,99)+'_SM.json'
        debugfname = rpath_LOGs+outfname.replace('.json','_dbgLOG.txt')
        outfname = rpath+outfname
        print(outfname)
        returned = R2_fun(SimPars,HCV_Pars)
        R2 = returned['R2']
        meanData = returned['meanData']
        print('HCV_Pars:')
        print(HCV_Pars) 
        print('R2=%f'%R2)
        
        with open(outfname,'w') as f:
            json.dump({'SimPars':SimPars,'HCV_Pars':HCV_Pars.tolist(),'meanData':meanData.tolist(),'R2':R2,'HCV_ParNames':HCV_ParNames},f)
        gc.collect()
    J=J+1
    
#    break

#if(debugplot):
#    from ep_LOGparser3 import *
#    import matplotlib as plt
#    plotLOG(debugfname)
#    f=figure(2);
#    f.clear()
#    plot(meanData)



""" old stuff """
#    with open(outfname,'a') as f:
#        f.write('#Target_values=%s\n'%Target_values)
#        for k in SimPars:
#            f.write('#%s=%s\n'%(k,SimPars[k]))
#    
#    
#        f.write(','.join(['HCV_Pars']+['%s'%f for f in HCV_ParNames]+['R2',str(987654321)])+'\n')
#    with open(outfname,'a') as f:
#        f.write(','.join(['HCV_Pars']+['%f'%f for f in HCV_Pars]+['R2',str(R2)])+'||'+json.dumps([list(a) for a in meanData])+'\n')
