# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 16:54:57 2015

@author: PopikOV
"""

import random, math
#import matplotlib.pyplot as plt
import time
import numpy

from numpy import array
from HCV_cell_2_SM import *

debug=0
debugplot=1
def combination(n,r): ## implements nCr
    numerator = 1.0
    denominator = 1.0

    for i in range(1,r):
        numerator = numerator * (n-i)
        denominator = denominator * i

    return numerator/denominator

def HCV_cell_inhibitor_curve(ind,inhibitorType, tX0,timer=0,HCV_Pars = [0.05         , 4        , 1    , 0.0005    , 0.25    ],maxTOD=-1):
#    degrRate = HCV_Pars[0]
    virFormation = HCV_Pars[0]
    cellFactor= HCV_Pars[1]
    virOut = HCV_Pars[2]
    if(timer):
        tStart = time.clock()
        
    X0 = [0.0]*44    
    if(type(tX0)==type([])):
        X0=tX0
#    random.seed()

    ## Example Reaction: Isomerization reaction from Gillespie's paper

    ## X ---c---> Z
    V = Species("Vis_w",X0[0])
    p = Species("Proteins_w", X0[1])    
    pcf = Species("Cell factor + proteins_w",X0[2])
    R = Species("RNA_w",X0[3])
    polyp = Species("Polyprotein_w",X0[4])
    cf = Species("Cell factor",X0[5])
        
    Vm1 = Species("Vis_m1", X0[6])
    pm1 = Species("Proteins_m1", X0[7])    
    pcfm1 = Species("Cell factor + proteins_m1",X0[8])
    Rm1 = Species("RNA_m1",X0[9])
    polypm1 = Species("Polyprotein_m1",X0[10])    

    Vm2 = Species("Vis_m2", X0[11])
    pm2 = Species("Proteins_m2", X0[12])    
    pcfm2 = Species("Cell factor + proteins_m2",X0[13])
    Rm2 = Species("RNA_m2",X0[14])
    polypm2 = Species("Polyprotein_m2",X0[15])    
    
    Vm3 = Species("Vis_m3", X0[16])
    pm3 = Species("Proteins_m3", X0[17])    
    pcfm3 = Species("Cell factor + proteins_m3",X0[18])
    Rm3 = Species("RNA_m3",X0[19])
    polypm3 = Species("Polyprotein_m3",X0[20])    

    Vir = Species("Virion_wild",X0[21]) 
    Virm1 = Species("Virion_m1",X0[22]) 
    Virm2 = Species("Virion_m2",X0[23]) 
    Virm3 = Species("Virion_m3",X0[24]) 
    
    Vir_out = Species("Virion_wild_out",X0[25]) 
    Virm1_out = Species("Virion_m1_out",X0[26]) 
    Virm2_out = Species("Virion_m2_out",X0[27]) 
    Virm3_out = Species("Virion_m3_out",X0[28]) 
    
    RNA_supa_muta = Species("RNA_supa_muta",X0[29])
    
    
    polypi2 = Species("Polyprotein\inh_NC_w",X0[30])
    polypmi1 = Species("Polyprotein\inh_NC_m1",X0[31])
    polypmi21 = Species("Polyprotein\inh_C_m1",X0[32])
    polypmi2 = Species("Polyprotein\inh_NC_m2",X0[33])
    polypmi22 = Species("Polyprotein\inh_C_m2",X0[34])
    polypmi3 = Species("Polyprotein\inh_NC_m3",X0[35])
    polypmi23 = Species("Polyprotein\inh_C_m3",X0[36])
    polypi = Species("Polyprotein\inh_C_w",X0[37])
    
    Vms = Species("Vis_ms", X0[38])
    pms = Species("Proteins_ms", X0[39])    
    pcfms = Species("Cell factor + proteins_ms",X0[40])
    polypms = Species("Polyprotein_ms",X0[41])
    Virms = Species("Virion_ms",X0[42])
    Virms_out = Species("Virion_ms_out",X0[43])  
    
    kout = 1.177
    kout_v_vm = 0.0002
    kout_vm_v = 0.0002
    kp = 0.75
    kt = 0.1
    kv = 3
    kcm = 0.42
    kproc = 1
    kprocm1 = 0.9
    kprocm2 = 0.7
    kprocm3 = 0.5
    mp = 1
    mpolyp = 0.3
    mr = 0.363 # DegradationRate
    mv = 0.058
    kcf = 2    #Cell Factor
    kcf = cellFactor
    mcf = 1
    ingNS3concentration = 1000*30
    ki_obr = 0
    ki = 0
    kim1 = 0
    kim2 = 0
    kim3 = 0
    ki2 = 0
    ki_obr2 = 0
    # BILN-2061
    
    if inhibitorType==1:
        ki_obr = 84
        ki=0.12*ingNS3concentration
        ki2=0
        ki_obr2=0
        kim1=ki/30
        kim2=ki/200
        kim3=ki/600
        kprocm1=0.9
        kprocm2=0.7
        kprocm3=0.5
    elif inhibitorType==2:
        ki_obr = (585-160)*3600
        ki=0.12*ingNS3concentration
        ki2=1.57*36
        ki_obr2=8.2*0.036
        kim1=ki/10
        kim2=ki/40
        kim3=ki/100
        kprocm1=0.9
        kprocm2=0.8
        kprocm3=0.5	
    elif inhibitorType==3:
        ki=0.12*ingNS3concentration
        ki_obr=(495-83)*3600
        ki2=(1.3+0.34)*36
        ki_obr2=(4.9-1.9)*0.036
        kim1=ki
        kim2=ki
        kim3=ki
    elif inhibitorType==4:
        ki=3*3.6/3000*ingNS3concentration
        ki_obr=0.3*3600
        ki2=6*36
        ki_obr2=3.8*0.036
        kim1=ki
        kim2=ki
        kim3=ki
        
    mcf = 1
    vf = 0.1
    vf = virFormation
    mvir = 0.5
    virout = 0.1
    
    virout = virOut
    
    ##Wild type processes:
    Vesicle_form_w = Reaction('Vesicle_form_w',[pcf, R],[V],1,kv)
    Vesicle_deg_w = Reaction('Vesicle_deg_w',[V],[],1,mv) 
    RNA_prod_w = Reaction('RNA_prod_w',[V],[R],0,kout)
    RNA_deg_w = Reaction('RNA_deg_w',[R],[],1,mr)
    Polyp_proc_w = Reaction('Polyp_proc_w',[polyp],[p],1,kproc)
    Replic_form_w = Reaction('Replic_form_w',[cf,p],[pcf],1,kp)
    P_deg_w = Reaction('P_deg_w',[p],[],1,mp) 
    Replic_deg_w = Reaction('Replic_deg_w',[pcf],[],1,mv)     
    Polyp_transl_w = Reaction('Polyp_transl_w',[R],[polyp],0,kt) 
    Polyp_deg_w = Reaction('Polyp_deg_w',[polyp],[],1,mpolyp)
    NC_polypinh_form_w = Reaction('NonCoval_polyp_inh_w',[polyp],[polypi],1,ki) 
    NC_polypinh_diss_w = Reaction('NonCoval_polypinh_diss_w',[polypi],[polyp],1,ki_obr) 
    NC_polypinh_deg_w = Reaction('NonCoval_polypinh_deg_w',[polypi],[],1,mpolyp) 
    C_polypinh_form_w = Reaction('Coval_polyp_inh_w',[polypi],[polypi2],1,ki2) 
    C_polypinh_diss_w = Reaction('Coval_polypinh_diss_w',[polypi2],[polypi],1,ki_obr2) 
    C_polypinh_deg_w = Reaction('Coval_polypinh_deg_w',[polypi2],[],1,mpolyp)     # Cell factor:
    CF_prod = Reaction('CF_prod',[],[cf],1,kcf) 
    CF_deg = Reaction('CF_deg',[cf],[],1,mcf)         
    RNA_prod_m1_w = Reaction('RNA_prod_m1_w',[V],[Rm1],0,kout_v_vm) 
    Vesicle_form_m1 = Reaction('Vesicle_form_m1',[pcfm1, Rm1],[Vm1],1,kv)
    Vesicle_deg_m1 = Reaction('Vesicle_deg_m1',[Vm1],[],1,mv) 
    RNA_prod_m1 = Reaction('RNA_prod_m1',[Vm1],[Rm1],0,kout) 
    RNA_deg_m1 = Reaction('RNA_deg_m1',[Rm1],[],1,mr) 
    Polyp_proc_m1 = Reaction('Polyp_proc_m1',[polypm1],[pm1],1,kprocm1) 
    Replic_form_m1 = Reaction('Replic_form_m1',[cf,pm1],[pcfm1],1,kp) 
    P_deg_m1 = Reaction('P_deg_m1',[pm1],[],1,mp) 
    Replic_deg_m1 = Reaction('Replic_deg_m1',[pcfm1],[],1,mv) 
    Polyp_transl_m1 = Reaction('Polyp_transl_m1',[Rm1],[polypm1],0,kt) 
    Polyp_deg_m1 = Reaction('Polyp_deg_m1',[polypm1],[],1,mpolyp) 
    RNA_prod_w_m1 = Reaction('RNA_prod_w_m1',[Vm1],[R],0,kout_vm_v/2)
    NC_polypinh_form_m1 = Reaction('NonCoval_polyp_inh_m1',[polyp],[polypmi1],1,kim1) 
    NC_polypinh_diss_m1 = Reaction('NonCoval_polypinh_diss_m1',[polypmi1],[polypm1],1,ki_obr) 
    NC_polypinh_deg_m1 = Reaction('NonCoval_polypinh_deg_m1',[polypmi1],[],1,mpolyp) 
    C_polypinh_form_m1 = Reaction('Coval_polyp_inh_m1',[polypmi1],[polypmi21],1,ki2) 
    C_polypinh_diss_m1 = Reaction('Coval_polypinh_diss_m1',[polypmi21],[polypmi1],1,ki_obr2) 
    C_polypinh_deg_m1 = Reaction('Coval_polypinh_deg_m1',[polypmi21],[],1,mpolyp)     
    RNA_prod_m2_w = Reaction('RNA_prod_m2_w',[V],[Rm2],0,kout_v_vm) 
    Vesicle_form_m2 = Reaction('Vesicle_form_m2',[pcfm2, Rm2],[Vm2],1,kv)
    Vesicle_deg_m2 = Reaction('Vesicle_deg_m2',[Vm2],[],1,mv) 
    RNA_prod_m2 = Reaction('RNA_prod_m2',[Vm2],[Rm2],0,kout) 
    RNA_deg_m2 = Reaction('RNA_deg_m2',[Rm2],[],1,mr) 
    Polyp_proc_m2 = Reaction('Polyp_proc_m2',[polypm2],[pm2],1,kprocm2) 
    Replic_form_m2 = Reaction('Replic_form_m2',[cf,pm2],[pcfm2],1,kp) 
    P_deg_m2 = Reaction('P_deg_m2',[pm2],[],1,mp) 
    Replic_deg_m2 = Reaction('Replic_deg_m2',[pcfm2],[],1,mv) 
    Polyp_transl_m2 = Reaction('Polyp_transl_m2',[Rm2],[polypm2],0,kt) 
    Polyp_deg_m2 = Reaction('Polyp_deg_m2',[polypm2],[],1,mpolyp) 
    RNA_prod_w_m2 = Reaction('RNA_prod_w_m2',[Vm2],[R],0,kout_vm_v/2)
    NC_polypinh_form_m2 = Reaction('NonCoval_polyp_inh_m2',[polyp],[polypmi2],1,kim2) 
    NC_polypinh_diss_m2 = Reaction('NonCoval_polypinh_diss_m2',[polypmi2],[polypm2],1,ki_obr) 
    NC_polypinh_deg_m2 = Reaction('NonCoval_polypinh_deg_m2',[polypmi2],[],1,mpolyp) 
    C_polypinh_form_m2 = Reaction('Coval_polyp_inh_m2',[polypmi2],[polypmi22],1,ki2) 
    C_polypinh_diss_m2 = Reaction('Coval_polypinh_diss_m2',[polypmi22],[polypmi2],1,ki_obr2) 
    C_polypinh_deg_m2 = Reaction('Coval_polypinh_deg_m2',[polypmi22],[],1,mpolyp) 
    RNA_prod_m3_w = Reaction('RNA_prod_m3_w',[V],[Rm3],0,kout_v_vm) 
    Vesicle_form_m3 = Reaction('Vesicle_form_m3',[pcfm3, Rm3],[Vm3],1,kv)
    Vesicle_deg_m3 = Reaction('Vesicle_deg_m3',[Vm3],[],1,mv) 
    RNA_prod_m3 = Reaction('RNA_prod_m3',[Vm3],[Rm3],0,kout) 
    RNA_deg_m3 = Reaction('RNA_deg_m3',[Rm3],[],1,mr) 
    Polyp_proc_m3 = Reaction('Polyp_proc_m3',[polypm3],[pm3],1,kprocm3) 
    Replic_form_m3 = Reaction('Replic_form_m3',[cf,pm3],[pcfm3],1,kp) 
    P_deg_m3 = Reaction('P_deg_m3',[pm3],[],1,mp) 
    Replic_deg_m3 = Reaction('Replic_deg_m3',[pcfm3],[],1,mv) 
    Polyp_transl_m3 = Reaction('Polyp_transl_m3',[Rm3],[polypm3],0,kt) 
    Polyp_deg_m3 = Reaction('Polyp_deg_m3',[polypm3],[],1,mpolyp) 
    RNA_prod_w_m3 = Reaction('RNA_prod_w_m3',[Vm3],[R],0,kout_vm_v/2)
    NC_polypinh_form_m3 = Reaction('NonCoval_polyp_inh_m3',[polyp],[polypmi3],1,kim3) 
    NC_polypinh_diss_m3 = Reaction('NonCoval_polypinh_diss_m3',[polypmi3],[polypm3],1,ki_obr) 
    NC_polypinh_deg_m3 = Reaction('NonCoval_polypinh_deg_m3',[polypmi3],[],1,mpolyp) 
    C_polypinh_form_m3 = Reaction('Coval_polyp_inh_m3',[polypmi3],[polypmi23],1,ki2) 
    C_polypinh_diss_m3 = Reaction('Coval_polypinh_diss_m3',[polypmi23],[polypmi3],1,ki_obr2) 
    C_polypinh_deg_m3 = Reaction('Coval_polypinh_deg_m3',[polypmi23],[],1,mpolyp)
    
    Virion_form_w = Reaction('Virion_form_w',[R],[Vir],1,vf) 
    Virion_form_m1 = Reaction('Virion_form_m1',[Rm1],[Virm1],1,vf) 
    Virion_form_m2 = Reaction('Virion_form_m2',[Rm2],[Virm2],1,vf) 
    Virion_form_m3 = Reaction('Virion_form_m3',[Rm3],[Virm3],1,vf) 
    
#    Virion_form_w = Reaction('Virion_form_w',[R,p],[Vir,p],1,vf) 
#    Virion_form_m1 = Reaction('Virion_form_m1',[Rm1,pm1],[Virm1,pm1],1,vf) 
#    Virion_form_m2 = Reaction('Virion_form_m2',[Rm2,pm2],[Virm2,pm2],1,vf) 
#    Virion_form_m3 = Reaction('Virion_form_m3',[Rm3,pm3],[Virm3,pm3],1,vf) 

    Virion_deg_w = Reaction('Virion_deg_w',[Vir],[],1,mvir) 
    Virion_deg_m1 = Reaction('Virion_deg_m1',[Virm1],[],1,mvir) 
    Virion_deg_m2 = Reaction('Virion_deg_m2',[Virm2],[],1,mvir) 
    Virion_deg_m3 = Reaction('Virion_deg_m3',[Virm3],[],1,mvir)        
    Virion_out_w = Reaction('Virion_out_w',[Vir],[Vir_out],1,virout) 
    Virion_out_m1 = Reaction('Virion_out_m1',[Virm1],[Virm1_out],1,virout) 
    Virion_out_m2 = Reaction('Virion_out_m2',[Virm2],[Virm2_out],1,virout) 
    Virion_out_m3 = Reaction('Virion_out_m3',[Virm3],[Virm3_out],1,virout) 
    
    RNA_prod_sm_m1 = Reaction('RNA_prod_sm_m1',[Vm1],[RNA_supa_muta],0,kout_vm_v/2)
    RNA_prod_sm_m2 = Reaction('RNA_prod_sm_m2',[Vm2],[RNA_supa_muta],0,kout_vm_v/2)
    RNA_prod_sm_m3 = Reaction('RNA_prod_sm_m3',[Vm3],[RNA_supa_muta],0,kout_vm_v/2)    
    Vesicle_form_ms = Reaction('Vesicle_form_ms',[pcfms, RNA_supa_muta],[Vms],1,kv)
    
    Vesicle_deg_ms = Reaction('Vesicle_deg_ms',[Vms],[],1,mv) 
    RNA_prod_ms = Reaction('RNA_prod_ms',[Vms],[RNA_supa_muta],0,kout) 
    RNA_deg_ms = Reaction('RNA_deg_ms',[RNA_supa_muta],[],1,mr) 
    Polyp_proc_ms = Reaction('Polyp_proc_ms',[polypms],[pms],1,kcm) 
    Replic_form_ms = Reaction('Replic_form_ms',[cf,pms],[pcfms],1,kp) 
    P_deg_ms = Reaction('P_deg_ms',[pms],[],1,mp) 
    Replic_deg_ms = Reaction('Replic_deg_ms',[pcfms],[],1,mv) 
    Polyp_transl_ms = Reaction('Polyp_transl_ms',[RNA_supa_muta],[polypms],0,kt) 
    Polyp_deg_ms = Reaction('Polyp_deg_ms',[polypms],[],1,mpolyp) 
    Virion_form_ms = Reaction('Virion_form_ms',[RNA_supa_muta,pms],[Virms,pms],1,vf) 
    Virion_out_ms = Reaction('Virion_out_ms',[Virms],[Virms_out],1,virout)
    Virion_deg_ms = Reaction('Virion_deg_ms',[Virms],[],1,mvir) 
    
#    example=Gillespie([Vesicle_form_w,Vesicle_deg_w,RNA_prod_w,RNA_deg_w,Polyp_proc_w,Replic_form_w,P_deg_w,Replic_deg_w,Polyp_transl_w,Polyp_deg_w,NC_polypinh_form_w,NC_polypinh_diss_w,NC_polypinh_deg_w,C_polypinh_form_w,C_polypinh_diss_w,C_polypinh_deg_w,CF_prod,CF_deg,RNA_prod_m1_w,Vesicle_form_m1,Vesicle_deg_m1,RNA_prod_m1,RNA_deg_m1,Polyp_proc_m1,Replic_form_m1,P_deg_m1,Replic_deg_m1,Polyp_transl_m1,Polyp_deg_m1,RNA_prod_w_m1,NC_polypinh_form_m1,NC_polypinh_diss_m1,NC_polypinh_deg_m1,C_polypinh_form_m1,C_polypinh_diss_m1,C_polypinh_deg_m1,RNA_prod_m2_w,Vesicle_form_m2,Vesicle_deg_m2,RNA_prod_m2,RNA_deg_m2,Polyp_proc_m2,Replic_form_m2,P_deg_m2,Replic_deg_m2,Polyp_transl_m2,Polyp_deg_m2,RNA_prod_w_m2,NC_polypinh_form_m2,NC_polypinh_diss_m2,NC_polypinh_deg_m2,C_polypinh_form_m2,C_polypinh_diss_m2,C_polypinh_deg_m2,RNA_prod_m3_w,Vesicle_form_m3,Vesicle_deg_m3,RNA_prod_m3,RNA_deg_m3,Polyp_proc_m3,Replic_form_m3,P_deg_m3,Replic_deg_m3,Polyp_transl_m3,Polyp_deg_m3,RNA_prod_w_m3,NC_polypinh_form_m3,NC_polypinh_diss_m3,NC_polypinh_deg_m3,C_polypinh_form_m3,C_polypinh_diss_m3,C_polypinh_deg_m3],
#                    [V,	p,	pcf,	R,	polyp,	polypi,	polypi2,	cf,	Vm1,	pm1,	pcfm1,	Rm1,	polypm1,	polypmi1,	polypmi21,	Vm2,	pm2,	pcfm2,	Rm2,	polypm2,	polypmi2,	polypmi22,	Vm3,	pm3,	pcfm3,	Rm3,	polypm3,	polypmi3,	polypmi23])


    AllSpecies = [V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3,Vir,Virm1,Virm2,Virm3,Vir_out,Virm1_out,Virm2_out,Virm3_out,RNA_supa_muta, polypi, polypi2, polypmi1, polypmi21, polypmi2, polypmi22, polypmi3, polypmi23,Vms,pms,pcfms,polypms,Virms,Virms_out]
    if(type(tX0)==type({})):
#        print('------ got dictionary')
        
        for s in AllSpecies:
            if s.name in tX0:
                s.quantity = tX0[s.name]
    
    example=Gillespie_2([Vesicle_form_w,Vesicle_deg_w,RNA_prod_w,RNA_deg_w,Polyp_proc_w,Replic_form_w,P_deg_w,Replic_deg_w,Polyp_transl_w,Polyp_deg_w,NC_polypinh_form_w,NC_polypinh_diss_w,NC_polypinh_deg_w,C_polypinh_form_w,C_polypinh_diss_w,C_polypinh_deg_w,CF_prod,CF_deg,RNA_prod_m1_w,Vesicle_form_m1,Vesicle_deg_m1,RNA_prod_m1,RNA_deg_m1,Polyp_proc_m1,Replic_form_m1,P_deg_m1,Replic_deg_m1,Polyp_transl_m1,Polyp_deg_m1,RNA_prod_w_m1,NC_polypinh_form_m1,NC_polypinh_diss_m1,NC_polypinh_deg_m1,C_polypinh_form_m1,C_polypinh_diss_m1,C_polypinh_deg_m1,RNA_prod_m2_w,Vesicle_form_m2,Vesicle_deg_m2,RNA_prod_m2,RNA_deg_m2,Polyp_proc_m2,Replic_form_m2,P_deg_m2,Replic_deg_m2,Polyp_transl_m2,Polyp_deg_m2,RNA_prod_w_m2,NC_polypinh_form_m2,NC_polypinh_diss_m2,NC_polypinh_deg_m2,C_polypinh_form_m2,C_polypinh_diss_m2,C_polypinh_deg_m2,RNA_prod_m3_w,Vesicle_form_m3,Vesicle_deg_m3,RNA_prod_m3,RNA_deg_m3,Polyp_proc_m3,Replic_form_m3,P_deg_m3,Replic_deg_m3,Polyp_transl_m3,Polyp_deg_m3,RNA_prod_w_m3,NC_polypinh_form_m3,NC_polypinh_diss_m3,NC_polypinh_deg_m3,C_polypinh_form_m3,C_polypinh_diss_m3,C_polypinh_deg_m3,Virion_form_w,Virion_form_m1,Virion_form_m2,Virion_form_m3,Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3,Virion_out_w,Virion_out_m1,Virion_out_m2,Virion_out_m3, RNA_prod_sm_m1,RNA_prod_sm_m2,RNA_prod_sm_m3,Vesicle_form_ms, Vesicle_deg_ms, RNA_prod_ms,  RNA_deg_ms, Polyp_proc_ms, Replic_form_ms, P_deg_ms, Replic_deg_ms, Polyp_transl_ms, Polyp_deg_ms, Virion_form_ms,  Virion_out_ms, Virion_deg_ms],
                    AllSpecies)

#    time_of_death = (1/0.003)*math.log((1/random.random()),math.e)
#    if time_of_death > 400:
#        time_of_death = 400

    time_of_death = 500*math.log((1/random.random()),math.e)
    if(maxTOD>0):
        time_of_death = min(time_of_death,maxTOD)
    #time_of_death = 400

    
    output,output_end = example.run_2(time_of_death,time_of_death)
    if(timer):
        return output_end,output,clock()-tStart
        
    return output_end,output


debug = 0
if(debug):
    startRNA = 20
    
    
#    tX0 = [0]*44
#    tX0[9]=startRNA
    tX0={'RNA_m1':startRNA}
    #tX0 = [35, 2, 0, 70, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    NmoreTstart = 0
    times=[]
    
    NT= 1
    virions_out = 0
    for j in range(NT):
        
        out_end,aa,t = HCV_cell_inhibitor_curve(1,1, tX0,timer=1,HCV_Pars = [0.2628         , 2.785        , 0.3    , 0.5    , 0.35    ])
        if len(a) > 0:
            maxRNA = max([ti[1] for ti in a ]) +  max([ti[2] for ti in a ]) +  max([ti[3] for ti in a ]) +  max([ti[4] for ti in a ])  +  max([ti[5] for ti in a ]) 
            virions_out = virions_out + sum([ti[6] for ti in a ]) +  sum([ti[7] for ti in a ]) +  sum([ti[8] for ti in a ]) +  sum([ti[9] for ti in a ])+  sum([ti[10] for ti in a ])
#            print    'virions_out', sum([ti[5] for ti in a ]) +  sum([ti[6] for ti in a ]) +  sum([ti[7] for ti in a ]) +  sum([ti[8] for ti in a ])
            if max([ti[10] for ti in a ]) > 0:
                print 'SUPERMUTA!!!'
            #plot(a)
            if(maxRNA>startRNA):
#            if(1):
                print('__')
                print('max RNA = %i'%maxRNA)
                print('max t = %f'%max([ti[0] for ti in a]))
                NmoreTstart=NmoreTstart+1
            times.append(t)
            
    virions_out_av = float(virions_out/NT)
    print ('virions_out total:%f'%virions_out)
    print ('virions_out by one cell:%f'%virions_out_av)
    print('time=%.3f+-%.3f'%(numpy.mean(times),numpy.std(times)))
    print('total time: %f'%sum(times))
    print('__')
    print('RNA count > start: %i / %i times, %i%%'%(NmoreTstart,NT,NmoreTstart*100.0/NT))



