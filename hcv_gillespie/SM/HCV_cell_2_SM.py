# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 16:55:30 2015

@author: PopikOV
"""

import random
import math
#import matplotlib.pyplot as plt
from time import clock
import numpy

import bisect


##from numpy import prod
#from operator import mul
#def prod(iterable):
#    return reduce(mul, iterable, 1)


debug = 0

def combination(n,r): ## implements nCr
    numerator = 1.0
    denominator = 1.0

    for i in range(1,r):
        numerator = numerator * (n-i)
        denominator = denominator * i

    return numerator/denominator

class Species:

    def __init__(self, name=None, quantity=0):
        self.name=name
        self.quantity=quantity
        
        self.reactions_imin = []
    def set_q(self,q):
        self.quantity = q
#        for r in self.reactions_imin:
#            r.calc()
        

class Reaction:


    def __init__(self, name=None, reactants=[], products=[],r_type=0, rate=0):
        self.name=name
        self.reactants=reactants
        for s in self.reactants:
            s.reactions_imin.append(self)
        self.products=products
        self.r_type=r_type
        self.rate=rate
        
        self.a_mu=0
        
        self.need2recalc = 1

        
        
        
class Gillespie_2:

    def __init__(self, reactions=[], all_species=[]):
        self.reactions=reactions
        self.Nreactions=len(reactions)
        self.all_species=all_species
        self.species_D = {s.name:s for s in all_species}
        self.calculate_parameters()
        
    def set_q(self,name,q):
        self.species_D[name].set_q(q)
        

    def calculate_parameters(self):
        self.a_mu = [0.0]*self.Nreactions
        reactionj=0
        
        while(reactionj < self.Nreactions)  :
            reaction = self.reactions[reactionj]
            try:
                h=reaction.reactants[0].quantity
                for reactant in reaction.reactants[1:]: 
                    h=h*reactant.quantity
            except:
                h=1.0
                
            self.a_mu[reactionj] = h*reaction.rate

            reactionj = reactionj+1
        self.a_0 = sum(self.a_mu)
        

    def run_2(self, Global_time=10, time_before_inhibition = 0, verbose=True):

        ##Prepare the output
        output=[]
        output_first_line=[]
        output_single=[]
        output_end = {}
        t=0.0
      
        output_first_line.append("Time")
        for one_species in self.all_species:
            if (one_species.name == 'RNA_w') or (one_species.name == 'RNA_m1') or (one_species.name == 'RNA_m2') or (one_species.name == 'RNA_m3') or (one_species.name == 'Virion_wild_out') or (one_species.name == 'Virion_m1_out') or (one_species.name == 'Virion_m2_out') or (one_species.name == 'Virion_m3_out') or (one_species.name == 'RNA_supa_muta') or (one_species.name == 'Virion_ms_out'): 
                output_first_line.append(one_species.name)

        
        output_single=[t]
        
        for one_species in self.all_species:
            if (one_species.name == 'RNA_w') or (one_species.name == 'RNA_m1') or (one_species.name == 'RNA_m2') or (one_species.name == 'RNA_m3') or (one_species.name == 'Virion_wild_out') or (one_species.name == 'Virion_m1_out') or (one_species.name == 'Virion_m2_out') or (one_species.name == 'Virion_m3_out') or (one_species.name == 'RNA_supa_muta') or (one_species.name == 'Virion_ms_out'): 
                output_single.append(one_species.quantity)

        
        r1=0.0
        r2=0.0
        tau=0.0
        t=0.0
        time_step = 0
        
        output_single=[t]
    
        for sname in ['RNA_w','RNA_m1','RNA_m2','RNA_m3','RNA_supa_muta','Virion_wild_out','Virion_m1_out','Virion_m2_out','Virion_m3_out','Virion_ms_out']:
            output_single.append(self.species_D[sname].quantity)
        output.append(output_single)
        while t < Global_time:
            
            if self.a_0 <= 0:
                break ## All reactants got exausted
            
            r1=random.random()
            r2=random.random()
            #print r1,r2
            tau=(1.0/self.a_0)*math.log((1.0/r1),math.e)
            t=t+tau
            # see which reaction will occur, using r2

            sum_of_as=0.0
            
            # Number of reaction to happen
            i=-1

            while sum_of_as<r2*self.a_0:
                i=i+1
                sum_of_as=sum_of_as+self.a_mu[i]
            
            if self.reactions[i].r_type == 1:                
                for one_species in self.reactions[i].reactants:
                    one_species.set_q(one_species.quantity-1)

            for one_species in self.reactions[i].products:
                one_species.set_q(one_species.quantity+1)

            
            virion_out = 0
            for s in ['Virion_wild_out','Virion_m1_out','Virion_m2_out','Virion_m3_out','Virion_ms_out']:
                if(self.species_D[s].quantity!=0):
                    virion_out = 1
                    break
                
            
            if all([self.species_D[sname].quantity==0 for sname in ['RNA_w','RNA_m1','RNA_m2','RNA_m3','RNA_supa_muta']]):
                t = Global_time
                virion_out=1
                
            if self.reactions[i].name =='Virion_ms_out' and  self.species_D['RNA_supa_muta'].quantity > 0:  
                print "SUPAMUTA VIR OUT!!"
                print "WOF!   WOF!  WOF! WOF! WOF!"
            
            
            if t >= time_before_inhibition:
                virion_out=1
                
            if virion_out == 1:
                output_single=[t]
            
                for sname in ['RNA_w','RNA_m1','RNA_m2','RNA_m3','RNA_supa_muta','Virion_wild_out','Virion_m1_out','Virion_m2_out','Virion_m3_out','Virion_ms_out']:
                    output_single.append(self.species_D[sname].quantity)
                output.append(output_single)
                
            if t >= time_before_inhibition:
                break    

            for sname in ['Virion_wild_out','Virion_m1_out','Virion_m2_out','Virion_m3_out','Virion_ms_out']:
                self.species_D[sname].quantity=0
#                
            self.calculate_parameters()
            
        for one_species in self.all_species:
            output_end[one_species.name]=one_species.quantity
        return output,output_end
        
def HCV_cell_curve(ind,tX0,timer=0,HCV_Pars = [ 0.05         , 4          , 1      , 0.0005    , 0.25      ], maxTOD = 200):

#    degrRate = HCV_Pars[0]
    virFormation = HCV_Pars[0]
    cellFactor= HCV_Pars[1]
    virOut = HCV_Pars[2]
#    if(timer):
    tStart = clock()
#    random.seed()

    ## Example Reaction: Isomerization reaction from Gillespie's paper

    ## X ---c---> Z
    X0=[0.0]*36
    if(type(tX0)==type([])):
        X0=tX0

    V = Species("Vis_w",X0[0])
    p = Species("Proteins_w", X0[1])    
    pcf = Species("Cell factor + proteins_w",X0[2])
    R = Species("RNA_w",X0[3])
    polyp = Species("Polyprotein_w",X0[4])
    cf = Species("Cell factor",X0[5])
        
    Vm1 = Species("Vis_m1", X0[6])
    pm1 = Species("Proteins_m1", X0[7])    
    pcfm1 = Species("Cell factor + proteins_m1",X0[8])
    Rm1 = Species("RNA_m1",X0[9])
    polypm1 = Species("Polyprotein_m1",X0[10])

    Vm2 = Species("Vis_m2", X0[11])
    pm2 = Species("Proteins_m2", X0[12])    
    pcfm2 = Species("Cell factor + proteins_m2",X0[13])
    Rm2 = Species("RNA_m2",X0[14])
    polypm2 = Species("Polyprotein_m2",X0[15])
    
    Vm3 = Species("Vis_m3", X0[16])
    pm3 = Species("Proteins_m3",X0[17])    
    pcfm3 = Species("Cell factor + proteins_m3",X0[18])
    Rm3 = Species("RNA_m3",X0[19])
    polypm3 = Species("Polyprotein_m3",X0[20])
    
    Vir = Species("Virion_wild",X0[21])   
    Virm1 = Species("Virion_m1",X0[22]) 
    Virm2 = Species("Virion_m2",X0[23]) 
    Virm3 = Species("Virion_m3",X0[24]) 
    
    Vir_out = Species("Virion_wild_out",X0[25])   
    Virm1_out = Species("Virion_m1_out",X0[26]) 
    Virm2_out = Species("Virion_m2_out",X0[27]) 
    Virm3_out = Species("Virion_m3_out",X0[28]) 
    
    RNA_supa_muta = Species("RNA_supa_muta",X0[29])
    Vms = Species("Vis_ms", X0[30])
    pms = Species("Proteins_ms", X0[31])    
    pcfms = Species("Cell factor + proteins_ms",X0[32])
    polypms = Species("Polyprotein_ms",X0[33])
    Virms = Species("Virion_ms",X0[34])
    Virms_out = Species("Virion_ms_out",X0[35])  

    kv = 3
    mv = 0.0578
    kout = 1.177
    mr = 0.5 # Degradation rate
    #mr = 0.363 # Degradation rate
    #mr = 0.2 # Degradation rate
    mr = 0.363
    kt = 0.1
    kc = 1
    kcm = 0.42
    kp = 0.75
    kcf = 4 # Cell Factor
    kcf = cellFactor
    mp = 1
    mpolyp = 0.3 
    kout_v_vm = 0.0002#*1000
    kout_vm_v = 0.0002#*1000
    vf = 0.05 # Virion Formation
    vf = 0.5 # Virion Formation
    vf = virFormation
    mcf = 1
    mvir = 0.5
    vout = 1
    vout = virOut
          
    Polyp_transl_w = Reaction('Polyp_transl_w',[R],[polyp],0,kt) 
    Vesicle_form_w = Reaction('Vesicle_form_w',[pcf, R],[V],1,kv)
    Vesicle_deg_w = Reaction('Vesicle_deg_w',[V],[],1,mv) 
    CF_prod = Reaction('CF_prod',[],[cf],1,kcf) 
    CF_deg = Reaction('CF_deg',[cf],[],1,mcf) 
    Polyp_proc_w = Reaction('Polyp_proc_w',[polyp],[p],1,kc) 
    Replic_form_w = Reaction('Replic_form_w',[cf,p],[pcf],1,kp) 
    RNA_deg_w = Reaction('RNA_deg_w',[R],[],1,mr) 
    P_deg_w = Reaction('P_deg_w',[p],[],1,mp) 
    Replic_deg_w = Reaction('Replic_deg_w',[pcf],[],1,mv) 
    Polyp_deg_w = Reaction('Polyp_deg_w',[polyp],[],1,mpolyp) 
    RNA_prod_w = Reaction('RNA_prod_w',[V],[R],0,kout) 
         
    RNA_prod_m1_w = Reaction('RNA_prod_m1_w',[V],[Rm1],0,kout_v_vm) 
    Vesicle_form_m1 = Reaction('Vesicle_form_m1',[pcfm1, Rm1],[Vm1],1,kv)
    Vesicle_deg_m1 = Reaction('Vesicle_deg_m1',[Vm1],[],1,mv) 
    RNA_prod_m1 = Reaction('RNA_prod_m1',[Vm1],[Rm1],0,kout) 
    RNA_deg_m1 = Reaction('RNA_deg_m1',[Rm1],[],1,mr) 
    Polyp_proc_m1 = Reaction('Polyp_proc_m1',[polypm1],[pm1],1,kcm) 
    Replic_form_m1 = Reaction('Replic_form_m1',[cf,pm1],[pcfm1],1,kp) 
    P_deg_m1 = Reaction('P_deg_m1',[pm1],[],1,mp) 
    Replic_deg_m1 = Reaction('Replic_deg_m1',[pcfm1],[],1,mv) 
    Polyp_transl_m1 = Reaction('Polyp_transl_m1',[Rm1],[polypm1],0,kt) 
    Polyp_deg_m1 = Reaction('Polyp_deg_m1',[polypm1],[],1,mpolyp) 
    RNA_prod_w_m1 = Reaction('RNA_prod_w_m1',[Vm1],[R],0,kout_vm_v) 
         
    RNA_prod_m2_w = Reaction('RNA_prod_m2_w',[V],[Rm2],0,kout_v_vm) 
    Vesicle_form_m2 = Reaction('Vesicle_form_m2',[pcfm2, Rm2],[Vm2],1,kv)
    Vesicle_deg_m2 = Reaction('Vesicle_deg_m2',[Vm2],[],1,mv) 
    RNA_prod_m2 = Reaction('RNA_prod_m2',[Vm2],[Rm2],0,kout) 
    RNA_deg_m2 = Reaction('RNA_deg_m2',[Rm2],[],1,mr) 
    Polyp_proc_m2 = Reaction('Polyp_proc_m2',[polypm2],[pm2],1,kcm) 
    Replic_form_m2 = Reaction('Replic_form_m2',[cf,pm2],[pcfm2],1,kp) 
    P_deg_m2 = Reaction('P_deg_m2',[pm2],[],1,mp) 
    Replic_deg_m2 = Reaction('Replic_deg_m2',[pcfm2],[],1,mv) 
    Polyp_transl_m2 = Reaction('Polyp_transl_m2',[Rm2],[polypm2],0,kt) 
    Polyp_deg_m2 = Reaction('Polyp_deg_m2',[polypm2],[],1,mpolyp) 
    RNA_prod_w_m2 = Reaction('RNA_prod_w_m2',[Vm2],[R],0,kout_vm_v) 
         
         
    RNA_prod_m3_w = Reaction('RNA_prod_m3_w',[V],[Rm3],0,kout_v_vm) 
    Vesicle_form_m3 = Reaction('Vesicle_form_m3',[pcfm3, Rm3],[Vm3],1,kv)
    Vesicle_deg_m3 = Reaction('Vesicle_deg_m3',[Vm3],[],1,mv) 
    RNA_prod_m3 = Reaction('RNA_prod_m3',[Vm3],[Rm3],0,kout) 
    RNA_deg_m3 = Reaction('RNA_deg_m3',[Rm3],[],1,mr) 
    Polyp_proc_m3 = Reaction('Polyp_proc_m3',[polypm3],[pm3],1,kcm) 
    Replic_form_m3 = Reaction('Replic_form_m3',[cf,pm3],[pcfm3],1,kp) 
    P_deg_m3 = Reaction('P_deg_m3',[pm3],[],1,mp) 
    Replic_deg_m3 = Reaction('Replic_deg_m3',[pcfm3],[],1,mv) 
    Polyp_transl_m3 = Reaction('Polyp_transl_m3',[Rm3],[polypm3],0,kt) 
    Polyp_deg_m3 = Reaction('Polyp_deg_m3',[polypm3],[],1,mpolyp) 
    RNA_prod_w_m3 = Reaction('RNA_prod_w_m3',[Vm3],[R],0,kout_vm_v) 
         
    Virion_form_w = Reaction('Virion_form_w',[R,p],[Vir,p],1,vf) 
    Virion_form_m1 = Reaction('Virion_form_m1',[Rm1,pm1],[Virm1,pm1],1,vf) 
    Virion_form_m2 = Reaction('Virion_form_m2',[Rm2,pm2],[Virm2,pm2],1,vf) 
    Virion_form_m3 = Reaction('Virion_form_m3',[Rm3,pm3],[Virm3,pm3],1,vf) 
         
    Virion_deg_w = Reaction('Virion_deg_w',[Vir],[],1,mvir) 
    Virion_deg_m1 = Reaction('Virion_deg_m1',[Virm1],[],1,mvir) 
    Virion_deg_m2 = Reaction('Virion_deg_m2',[Virm2],[],1,mvir) 
    Virion_deg_m3 = Reaction('Virion_deg_m3',[Virm3],[],1,mvir) 
         
    Virion_out_w = Reaction('Virion_out_w',[Vir],[Vir_out],1,vout) 
    Virion_out_m1 = Reaction('Virion_out_m1',[Virm1],[Virm1_out],1,vout) 
    Virion_out_m2 = Reaction('Virion_out_m2',[Virm2],[Virm2_out],1,vout) 
    Virion_out_m3 = Reaction('Virion_out_m3',[Virm3],[Virm3_out],1,vout) 

    RNA_prod_sm_m1 = Reaction('RNA_prod_sm_m1',[Vm1],[RNA_supa_muta],0,kout_vm_v/2)
    RNA_prod_sm_m2 = Reaction('RNA_prod_sm_m2',[Vm2],[RNA_supa_muta],0,kout_vm_v/2)
    RNA_prod_sm_m3 = Reaction('RNA_prod_sm_m3',[Vm3],[RNA_supa_muta],0,kout_vm_v/2)    
    
    Vesicle_form_ms = Reaction('Vesicle_form_ms',[pcfms, RNA_supa_muta],[Vms],1,kv)
    Vesicle_deg_ms = Reaction('Vesicle_deg_ms',[Vms],[],1,mv) 
    RNA_prod_ms = Reaction('RNA_prod_ms',[Vms],[RNA_supa_muta],0,kout) 
    RNA_deg_ms = Reaction('RNA_deg_ms',[RNA_supa_muta],[],1,mr) 
    Polyp_proc_ms = Reaction('Polyp_proc_ms',[polypms],[pms],1,kcm) 
    Replic_form_ms = Reaction('Replic_form_ms',[cf,pms],[pcfms],1,kp) 
    P_deg_ms = Reaction('P_deg_ms',[pms],[],1,mp) 
    Replic_deg_ms = Reaction('Replic_deg_ms',[pcfms],[],1,mv) 
    Polyp_transl_ms = Reaction('Polyp_transl_ms',[RNA_supa_muta],[polypms],0,kt) 
    Polyp_deg_ms = Reaction('Polyp_deg_ms',[polypms],[],1,mpolyp) 
    Virion_form_ms = Reaction('Virion_form_ms',[RNA_supa_muta,pms],[Virms,pms],1,vf) 
    Virion_out_ms = Reaction('Virion_out_ms',[Virms],[Virms_out],1,vout)
    Virion_deg_ms = Reaction('Virion_deg_ms',[Virms],[],1,mvir) 



#  Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3,  
    example_2=Gillespie_2([Virion_out_w,Virion_out_m1,Virion_out_m2,Virion_out_m3,Virion_form_w, Virion_form_m1, Virion_form_m2, Virion_form_m3,Vesicle_form_w,	Vesicle_form_m1,	Vesicle_form_m2,	Vesicle_form_m3,	Vesicle_deg_w,	Vesicle_deg_m1,	Vesicle_deg_m2,	Vesicle_deg_m3,	RNA_prod_w,	RNA_prod_m1,	RNA_prod_m2,	RNA_prod_m3,	RNA_deg_w,	RNA_deg_m1,	RNA_deg_m2,	RNA_deg_m3,	Polyp_proc_w,	Polyp_proc_m1,	Polyp_proc_m2,	Polyp_proc_m3,	Replic_form_w,	Replic_form_m1,	Replic_form_m2,	Replic_form_m3,	P_deg_w,	P_deg_m1,	P_deg_m2,	P_deg_m3,	Replic_deg_w,	Replic_deg_m1,	Replic_deg_m2,	Replic_deg_m3,	Polyp_transl_w,	Polyp_transl_m1,	Polyp_transl_m2,	Polyp_transl_m3,	Polyp_deg_w,	Polyp_deg_m1,	Polyp_deg_m2,	Polyp_deg_m3,	RNA_prod_w_m1,	RNA_prod_w_m2,	RNA_prod_w_m3,	RNA_prod_m1_w,	RNA_prod_m2_w,	RNA_prod_m3_w,	CF_prod,	CF_deg, Virion_deg_w,Virion_deg_m1,Virion_deg_m2,Virion_deg_m3,RNA_prod_sm_m1,RNA_prod_sm_m2,RNA_prod_sm_m3,Vesicle_form_ms, Vesicle_deg_ms, RNA_prod_ms,  RNA_deg_ms, Polyp_proc_ms, Replic_form_ms, P_deg_ms, Replic_deg_ms, Polyp_transl_ms, Polyp_deg_ms, Virion_form_ms,  Virion_out_ms, Virion_deg_ms],
                      [V,p,pcf,R,polyp,cf,Vm1,pm1,pcfm1,Rm1,polypm1,Vm2,pm2,pcfm2,Rm2,polypm2,Vm3,pm3,pcfm3,Rm3,polypm3,Vir,Virm1,Virm2,Virm3,Vir_out,Virm1_out,Virm2_out,Virm3_out,RNA_supa_muta,Vms,pms,pcfms,polypms,Virms,Virms_out])


    if(type(tX0)==type({})):
#        print('------ got dictionary')
        
        for s in example_2.all_species:
            if s.name in tX0:
                s.quantity = tX0[s.name]


    time_of_death = (1/0.003)*math.log((1/random.random()),math.e)
#    if time_of_death > time_before_inhibition:
#        time_of_death = time_before_inhibition
#    if time_of_death > 400:
#        time_of_death = 400
    
#    time_of_death = 1000    
        
    output_2, out_end= example_2.run_2(time_of_death,maxTOD)
  
        
#    print( 'done in %.5fs'%(clock()-tStart))
    if(timer):
      return out_end, output_2 ,clock()-tStart
    else:
      return  out_end, output_2
    
#    print()
debug=0

if(debug):
    startRNA = 50
    
    
    tX0 = {}#[0]*36
    tX0['RNA_w'] = startRNA
#    tX0[3]=startRNA
    #tX0 = [35, 2, 0, 70, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    NmoreTstart = 0
    times=[]
    
    NT= 100
    virions_out = 0
    for j in range(NT):
        
        t,a,time = HCV_cell_curve(1, tX0,timer=1,HCV_Pars = [0.2628         , 2.785        , 0.3    , 0.5    , 0.35    ])
        if len(a) > 0:
            maxRNA = max([ti[1] for ti in a ]) +  max([ti[2] for ti in a ]) +  max([ti[3] for ti in a ]) +  max([ti[4] for ti in a ])  +  max([ti[5] for ti in a ]) 
            virions_out = virions_out + sum([ti[6] for ti in a ]) +  sum([ti[7] for ti in a ]) +  sum([ti[8] for ti in a ]) +  sum([ti[9] for ti in a ])+  sum([ti[10] for ti in a ])
            #print    'virions_out', sum([ti[6] for ti in a ]) +  sum([ti[7] for ti in a ]) +  sum([ti[8] for ti in a ]) +  sum([ti[9] for ti in a ])+  sum([ti[10] for ti in a ])
            if ti[5]>0:
                print 'SUPERMUTA!!!'
            #plot(a)
            if(maxRNA>startRNA):
#            if(1):
                print('__')
                print('max RNA = %i'%maxRNA)
                print('max t = %f'%max([ti[0] for ti in a]))
                NmoreTstart=NmoreTstart+1
            times.append(t)
            
    virions_out_av = float(virions_out/NT)
    print ('virions_out total:%f'%virions_out)
    print ('virions_out by one cell:%f'%virions_out_av)
    print('time=%.3f+-%.3f'%(numpy.mean(times),numpy.std(times)))
    print('total time: %f'%sum(times))
    print('__')
    print('RNA count > start: %i / %i times, %i%%'%(NmoreTstart,NT,NmoreTstart*100.0/NT))