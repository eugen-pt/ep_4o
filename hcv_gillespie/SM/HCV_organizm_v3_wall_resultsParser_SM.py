# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 22:19:09 2015

@author: ep
"""

import glob
import json 

import matplotlib.pyplot as plt
import os
import time
import random
import gc
rpath = 'outputs/'

rimgspath = 'output_Imgs/'
try:
    os.mkdir(rimgspath)
except:
    pass    

rdaylogimgspath = 'output_Imgs_daylog/'
try:
    os.mkdir(rdaylogimgspath)
except:
    pass    

#files = glob.glob(rpath+'*outptut_n_nb_wall_.csv')
files = glob.glob(rpath+'*_SM.json')
#files = glob.glob(rpath+'*T300i*.json')
print(files)

Dyns = []
HCV_Pars = []
R2s = []

HCV_ParNames= ['virFormation','cellFactor','virOut','o_cellInf','o_virDegr']

HCV_sParNames = ['vf','cf','vo','o_inf','o_vd'] #short parnames)


SimParsNames = {'Random_seed':'rseed','number_of_curves':'NofCs','inhibitorType':'inhType',
         'simTime':'simT','inhibitionTime':'inhT','stopInhibitionTime':'sInhT','res_MinT':'res_MinT','res_MaxT':'res_MaxT','NCells':'NC'}

SimParsssNames = {'inhibitorType':'i','simTime':'sT','inhibitionTime':'iT','stopInhibitionTime':'sIT','res_MinT':'rmT','res_MaxT':'rMT','NCells':'NC'}   


f=plt.figure(1)
for fname in files:#random.sample(files,len(files)):
#  J=0
  with open(fname,'r') as f:
      try:
          R = json.load(f)  
      except:
          print('problem with %s'%fname)
          continue
      
      dyns = array(R['meanData'])
      HCV_Pars = R['HCV_Pars']
      R2 = R['R2']
      SimPars=R['SimPars']
      
      toptitle = ' '.join(['%s=%i'%(SimParsNames[k],SimPars[k]) for k in SimPars])
      
#    for line in f:
##      if(J>=1):
##        break
#      if(line[0]=='#'):
#        continue
#      if('||' not in line):
#        continue
##      print(line)
#      s = line[0:line.index('||')].split(',')
#      HCV_Pars=[float(ts) for ts in s[1:6]]
#      R2 = float(s[-1])
#      dyns = json.loads(line[line.index('||')+2:])
      
      if(std(dyns[:,0])<10):
          t = arange(SimPars['simTime']+1)
      else:    
          t=dyns[:,0]
          
      colors = ['g']+['r','b','c','m','k']*3
      styles = ['-']*6+['--']*5+['o']*5
      
      if(1):
          imgfname = fname
    #      imgfname = imgfname.replace('.csv','j%i.png'%(J))
          imgfname = imgfname.replace('.json','.png')
          
          if(0): #if you wanna add parameter values into filename
            imgfname = imgfname.replace('.png','__'+'_'.join(['%s=%.2f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))])+'.png')
          imgfname = imgfname.replace(rpath,rimgspath)
          
          if(not os.path.isfile(imgfname)):
              print(fname)
    #          f=plt.figure(1)
              tmax=0
              for j in range(1,shape(dyns)[1]):
                plt.plot(t,dyns[:,j],styles[j-1]+colors[j-1])
                tmax = max(tmax,max(dyns[:,j]))
              plot(array([SimPars['inhibitionTime']-0.1,SimPars['inhibitionTime']+0.1]),[0,tmax],'-.k')   
              if('stopInhibitionTime' in SimPars):
                  plot(array([SimPars['stopInhibitionTime']-0.1,SimPars['stopInhibitionTime']+0.1]),[0,tmax],'-.k')   
        #      legend(['t']+HCV_sParNames)  #dont have the value names (which are in )
              
              plt.xlabel(' '.join(['%s=%f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))]))
              plt.title(toptitle)
              plt.legend(['H','w','m1','m2','m3','SM'])
              plt.savefig(imgfname, bbox_inches='tight')
              plt.cla()
          
#          plt.close(f)
          
      rdaylogimgspath   
      imgfname = fname
      imgfname = imgfname.replace('.json','.png')
      imgfname = imgfname.replace(rpath,rdaylogimgspath)
      if(not os.path.isfile(imgfname)):
          print(imgfname)
#          f=plt.figure(1)
          dyns[dyns<1]=0
          t=t/24.0 
          tmax=0    
          temp = t*1.0
            
          
          for j in range(1,shape(dyns)[1]):
              temp = log10(dyns[:,j])
              plt.plot(t,temp,styles[j-1]+colors[j-1])
              tmax = max(tmax,max(temp))
          plot(array([SimPars['inhibitionTime']-0.1,SimPars['inhibitionTime']+0.1])/24.0,[0,tmax],'-.k')    
          if('stopInhibitionTime' in SimPars):
              plot(array([SimPars['stopInhibitionTime']-0.1,SimPars['stopInhibitionTime']+0.1])/24.0,[0,tmax],'-.k')    
#          plt.plot(log10(dyns))
    #      legend(['t']+HCV_sParNames)  #dont have the value names (which are in )
          
          plt.xlabel(' '.join(['%s=%f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))]))
          plt.title(toptitle)
          plt.legend(['H','w','m1','m2','m3','SM'])
          ty=plt.ylim()
#          plt.ylim([0,ty[1]])
          plt.savefig(imgfname, bbox_inches='tight')
          plt.cla()
#          plt.close(f)
#          break
        
      gc.collect()    
#      time.sleep(.5)
#      break  
#      J=J+1
#    if(J>=1):
#      break
#      print(dyns)
#      raise ValueError
      
