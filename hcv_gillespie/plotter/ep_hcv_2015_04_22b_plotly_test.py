# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 11:37:25 2015

@author: ep
"""
import glob
import json
import random
#x = np.arange(0.0, 2, 0.01)
#y1 = np.sin(2*np.pi*x)
#y2 = 1.2*np.sin(4*np.pi*x)
#
#fig, (ax1, ax2, ax3) = plt.subplots(3,1, sharex=True)
#
#ax1.fill_between(x, 0, y1)
#ax1.set_ylabel('between y1 and 0')
#
#ax2.fill_between(x, y1, 1)
#ax2.set_ylabel('between y1 and 1')
#
#ax3.fill_between(x, y1, y2)
#ax3.set_ylabel('between y1 and y2')
#ax3.set_xlabel('x')


rpath = '../Calculus/outputs/'

rimgspath = 'output_Imgs/'
try:
    os.mkdir(rimgspath)
except:
    pass    

rdaylogimgspath = 'output_Imgs_daylog/'
try:
    os.mkdir(rdaylogimgspath)
except:
    pass    

files = glob.glob(rpath+'*outptut_n_nb_wall_.csv')
files = glob.glob(rpath+'*.json')
#files = glob.glob(rpath+'*T300i*.json')
print(files)

Dyns = []
HCV_Pars = []
R2s = []

HCV_ParNames= ['virFormation','cellFactor','virOut','o_cellInf','o_virDegr']

HCV_sParNames = ['vf','cf','vo','o_inf','o_vd'] #short parnames)


SimParsNames = {'Random_seed':'rseed','number_of_curves':'NofCs','inhibitorType':'inhType',
         'simTime':'simT','inhibitionTime':'inhT','res_MinT':'res_MinT','res_MaxT':'res_MaxT','NCells':'NC'}

SimParsssNames = {'inhibitorType':'i','simTime':'sT','inhibitionTime':'iT','res_MinT':'rmT','res_MaxT':'rMT','NCells':'NC'}   


f=plt.figure(1)
for fname in [files[4]]:#random.sample(files,1):#
#  J=0
  with open(fname,'r') as f:
      try:
          R = json.load(f)  
      except:
          print('problem with %s'%fname)
          continue
      
      dyns = array(R['meanData'])
      HCV_Pars = R['HCV_Pars']
      R2 = R['R2']
      SimPars=R['SimPars']
      
      if(std(dyns[:,0])<10):
          t = arange(SimPars['simTime']+1)
      else:    
          t=dyns[:,0]
      
      toptitle = ' '.join(['%s=%i'%(SimParsNames[k],SimPars[k]) for k in SimPars])
      
#    for line in f:
##      if(J>=1):
##        break
#      if(line[0]=='#'):
#        continue
#      if('||' not in line):
#        continue
##      print(line)
#      s = line[0:line.index('||')].split(',')
#      HCV_Pars=[float(ts) for ts in s[1:6]]
#      R2 = float(s[-1])
#      dyns = json.loads(line[line.index('||')+2:])
      colors = ['g']+['r','b','c','m']*3
      styles = ['-']*5+['--']*4+['-.']*4
      
      if(1):
          imgfname = fname
    #      imgfname = imgfname.replace('.csv','j%i.png'%(J))
          imgfname = imgfname.replace('.json','.png')
          
          if(0): #if you wanna add parameter values into filename
            imgfname = imgfname.replace('.png','__'+'_'.join(['%s=%.2f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))])+'.png')
          imgfname = imgfname.replace(rpath,rimgspath)
          
          if(not os.path.isfile(imgfname)):
              print(fname)
    #          f=plt.figure(1)
              
              if(shape(dyns)[1] == 10):
                  nplots = 2
              elif(shape(dyns)[1] == 14):
                  nplots=3
              else:
                  raise ValueError()
              """ Cells """
              ax1 = subplot(nplots,1,1)
              plt.title('Cells')
              ax1.fill_between(t, 0, dyns[:,1],facecolor=colors[0])
              plot([0,0],[0,0],colors[0])
              tsum = dyns[:,1]
              for j in range(2,6):
                  ax1.fill_between(t,tsum,tsum+dyns[:,j],facecolor = colors[j-1])
                  tsum=tsum+dyns[:,j]
                  plot([0,0],[0,0],colors[j-1])
              tylim=plt.ylim()    
              ylim(0,tylim[1])
              legend(['H','W','M1','M2','M3'])

              ax1 = subplot(nplots,1,2)
              plt.title('Virions')
              ax1.fill_between(t, 0, dyns[:,6],facecolor=colors[5])
              tsum = dyns[:,6]
              plot([0,0],[0,0],colors[5])
              for j in range(7,10):
                  ax1.fill_between(t,tsum,tsum+dyns[:,j],facecolor = colors[j-1])
                  tsum=tsum+dyns[:,j]
                  plot([0,0],[0,0],colors[j-1])
              tylim=plt.ylim()    
              ylim(0,tylim[1])
              legend(['W','M1','M2','M3'])
              
              if(shape(dyns)[1] == 14):
                  ax1 = subplot(nplots,1,3)
                  plt.title('RNAs')
                  ax1.fill_between(t, 0, dyns[:,10],facecolor=colors[10-1])
                  tsum = dyns[:,10]
                  plot([0,0],[0,0],colors[9])
                  for j in range(11,14):
                      ax1.fill_between(t,tsum,tsum+dyns[:,j],facecolor = colors[j-1])
                      tsum=tsum+dyns[:,j]
                      plot([0,0],[0,0],colors[j-1])
                  tylim=plt.ylim()    
                  ylim(0,tylim[1])
                  legend(['W','M1','M2','M3'])
              
#              plt.plot(dyns)
        #      legend(['t']+HCV_sParNames)  #dont have the value names (which are in )
              
              plt.xlabel(' '.join(['%s=%f'%(HCV_sParNames[j],HCV_Pars[j]) for j in range(len(HCV_Pars))])+'\n'+toptitle)
#              plt.title(toptitle)
#              plt.savefig(imgfname, bbox_inches='tight')
#              plt.cla()
          
#          plt.close(f)
          
 