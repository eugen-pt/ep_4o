# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 11:29:20 2018

@author: petrovskiy
"""


import xlrd
import os

routpath = 'out'

os.mkdir(routpath)

#rb = xlrd.open_workbook('src/abstracts_list_MAIN.xlsx')
rb = xlrd.open_workbook('src/abstracts_list_MAIN (checked).xlsx')
sheet = rb.sheet_by_index(1)


from PIL import Image, ImageDraw, ImageFont
page_size = (103*2,133)

template_n = 1

imgpath_fmt =  'src\PDFtoJPG.me-%i.jpg'
imgpath_fmt =  'src\dpi1200\PDFtoJPG.me-%i.jpg'
imgpath_fmt =  'src\dpi600\PDFtoJPG.me-%i.jpg'


# create Image object with the input image
Images = [Image.open(imgpath_fmt % template_n) for template_n in range(1,4)]

# maximum width the text can be (otherwise - either use smaller font of divide into several lines)
MAX_W = Images[0].size[0]*0.9/2

K = Images[0].size[0]/(2*4867)


## Fonts that are present in the pdf according to Adobe
##  (downloaded from the internet somewhere..)
#font_path = 'src\Helios.ttf'
font_path = 'src\Myriad Pro Regular.ttf'

# %%

SAVE_ALL = 0
ALL_Imgs = []

def processOrg(org):
    
    import re

    org = org.strip() # supposably does nothing. but what if?
    org = org.replace('\n',' ') #who would do that anyway?
    org = org.replace('I. M. ','I.M.') # so I and M do not count as words
    org = org.replace('of the Siberian Branch of the RAS','SB RAS') #yeah, that
    
    RLines = []
    ras_stuff = re.findall('( *(, )?(of )?(the )?([A-Z]+ )?(RAS))$',org)
    if(len(ras_stuff)>0):
        RLines = [ras_stuff[0][-2]+ras_stuff[0][-1]]
        org=org.replace(ras_stuff[0][0],',')
        
    # Lets clean `ofthe` for these organizations
    Ss = """
    Russian Academy of Sciences
    National Academy of Sciences of Belarus
    Azerbaijan National Academy of Sciences
    Nanyang Technological University
    Academy of Sciences of the Republic of Tajikistan
    The University of Texas
    Academy of Sciences of Uzbekistan
    NAS of Belarus
    University Autonoma Barcelona
    St. Petersburg State University
    """
    for S in Ss.split('\n')[1:-1]:
        ras_stuff = re.findall('((, )?(of )?(the )?('+S.strip()+'))$',org)
        if(len(ras_stuff)>0):
            RLines = [ras_stuff[0][-1]]
            org=org.replace(ras_stuff[0][0],',')
    
    # [St. Petersburg State University] without 'of the' `bug`
    if(org==','):
        return RLines
    
    words = org.split(' ')
    # lets count big meaningful words
    N_words = len([w for w in words if w not in {'of','the','in','and'}])
    if(N_words>4):
        n = int(len(words)/(3-len(RLines)))+1
        if(len(RLines)>0) or (N_words<8):
            RLines = [' '.join(words[:n]),' '.join(words[n:])] + RLines
        else:
            RLines = [' '.join(words[:n]),' '.join(words[n:2*n]),' '.join(words[2*n:])] + RLines
    else:
        RLines = [' '.join(words)]+RLines
    return RLines

def doStuff(data, out=None):
#    Name = '%s %s' % (data[15], data[16])
    
    # Dr Name MidName LastName
#    Name = ' '.join([s for s in [data[3],data[5],data[6],data[4]] if len(s)>0])
    # Dr Name LastName
#    Name = ' '.join([s for s in [data[3],data[5],data[4]] if len(s)>0])
    # Name LastName
    Name = ' '.join([s.capitalize() for s in [data[5],data[4]] if len(s)>0])
    """
     capitalize is mostly for Laurent GENTZBITTEL
           although if you think aabout it the name sounds good the way it is,
    
       Laurent
           ...
      _____ ______ _   _ _______ __________ _____ _______ _______ ______ _        _   _   _   _   _   _
     / ____|  ____| \ | |__   __|___  /  _ \_   _|__   __|__   __|  ____| |      | | | | | | | | | | | |
    | |  __| |__  |  \| |  | |     / /| |_) || |    | |     | |  | |__  | |      | | | | | | | | | | | |
    | | |_ |  __| | . ` |  | |    / / |  _ < | |    | |     | |  |  __| | |      | | | | | | | | | | | |
    | |__| | |____| |\  |  | |   / /__| |_) || |_   | |     | |  | |____| |____  |_| |_| |_| |_| |_| |_|
     \_____|______|_| \_|  |_|  /_____|____/_____|  |_|     |_|  |______|______| (_) (_) (_) (_) (_) (_)
     
     
    """

    Email = data[14]
    Organization = data[19]
    Location = ('%s, %s' % (data[17], data[18]))\
                if (data[18] != data[17]) else data[18]
    
    Status = data[12]
    
    template_n = (3) if (Status=='OC') else (2 if (Status=='PC') else 1)
    # %%
    # https://haptik.ai/tech/putting-text-on-image-using-python/
    
#    image = Image.open(imgpath_fmt % template_n)
    image = Images[template_n-1].copy()
    
    draw = ImageDraw.Draw(image)
    
    
    def centerText(draw, s, pos, color, font_path, font_size):
        font = ImageFont.truetype(font_path, size=int(font_size*K))
        
        # https://stackoverflow.com/questions/1970807/center-middle-align-text-with-pil
        w, h = draw.textsize(s, font=font)
        
        if(w > MAX_W):
            nfont_size = font_size * MAX_W / w
            print('%s %i -> %i' % (s, font_size, nfont_size))
            font = ImageFont.truetype(font_path, size=int(nfont_size*K))
            w, h = draw.textsize(s, font=font)
        
        draw.text((pos[0]-w/2, pos[1]), s, fill=color, font=font)
    
    color = 'rgb(0, 0, 0)' # black color
    
    if(0):
        centerText(draw, Name, ( image.size[0]/4, 2450*K ), color, font_path, 600)
    else:
        Name = [' '.join([js.capitalize() for js in s.strip().split(' ')]) for s in [data[5],data[4]] if len(s)>0]
        for Nj in range(len(Name)):
            centerText(draw, Name[Nj], ( image.size[0]/4, 2300*K + 800*Nj*K ), color, font_path, 800)
        
    
#    centerText(draw, Email, ( image.size[0]/2, 3050*K ), color, font_path, 200)
    
    
#    centerText(draw, Organization, ( image.size[0]/2, 3700*K ), color, font_path, 240)
    
    if(0):
        # City on far separate line
        RLines = processOrg(Organization)
        for Lj in range(len(RLines)):
            centerText(draw, RLines[Lj], ( image.size[0]/4, (3675-150*(len(RLines)-1)+300*Lj)*K ), color, font_path, 240)
           
        centerText(draw, Location, ( image.size[0]/4, 4700*K ), color, font_path, 240)
    else:
        RLines = processOrg(Organization)+[Location]
        fontsize = 240
        sep = 300
        if(len(RLines)>4):
            sep = 300*4/len(RLines)
            fontsize = sep*240/300
            print(data)
        for Lj in range(len(RLines)):
            centerText(draw, RLines[Lj],(
                                          image.size[0]/4, 
                                          K*(4800 - sep*(len(RLines)-1)+sep*Lj)#(3675-150*(len(RLines)-1)+300*Lj)*K 
                                      ),
                                        color, font_path, fontsize)
        
    
    #(x, y) = (150, 150)
    #name = 'Vinay'
    #color = 'rgb(255, 255, 255)' # white color
    #draw.text((x, y), name, fill=color, font=font)
    # 
    # save the edited image
    if(out is None):
        pass
    else:
        if(type(out)==type('')):
            outpath = out
        else:
            if(type(data[0])==type('')):
                data[0] = 0
            outpath = os.path.join(routpath,'%03i__%i_%s.jpg' % (data[-1],data[0],Name))
        image.save(outpath, dpi=image.info['dpi'])
        
    if(SAVE_ALL):
        ALL_Imgs.append(image)


if(0):
    # Test Organization -> lines separation
    orgs = sheet.col_values(19,1)
    
    orgs = sorted(list(set(orgs)), key=len)
    
    def centerify(text, width=-1):
      lines = text.split('\n')
      width = max(map(len, lines)) if width == -1 else width
      return '\n'.join(line.center(width) for line in lines)
    
    for org in orgs:
        print('-----------------------------------')
        LS = processOrg(org)
        print(centerify('\n'.join(LS)))
        
    aaa

if(0):
    # Save as PDFs, not too many with 1go (50 for 600dpi, 15 for 1200dpi, but still joinable)
    import gc
    
    SAVE_ALL = 1
    N = 15 # how many with one go
    j = 1
    j1 = 0;    
    jout = 0;
    ALL_Imgs = []

    while(j<sheet.nrows):
        print(j)
        doStuff(sheet.row_values(j)+[j], None)
        
        j = j+1
        j1 = j1+1
        if(j1>=N)or(j==sheet.nrows):
            jout = jout + 1
            ALL_Imgs[0].save("out_%i_%i.pdf" % (j-j1,j-1), save_all=True, append_images=ALL_Imgs[1:])
            ALL_Imgs = []
            j1 = 0
            gc.collect()
            
#            break
    aaa

if(0):
    # test single image
    data = sheet.row_values(188)
    data = sheet.row_values(863)
    doStuff(data,'temp.jpg')

    aaa
    
for j in range(596,597):
#for j in range(1,25):
#for j in range(1,sheet.nrows):
    print(j)
    doStuff(sheet.row_values(j)+[j],1)
""" """ 
#