# -*- coding: utf-8 -*-
"""
Created on Wed Sep  5 16:16:10 2018

@author: ep
"""

# % 
import xlrd
import os

#routpath = 'out'

#os.mkdir(routpath)

#rb = xlrd.open_workbook('src/abstracts_list_MAIN.xlsx')
rb = xlrd.open_workbook('src/Регистрационная ведомостьБГРС2018_присут.xlsx')
sheet = rb.sheet_by_index(0)

header = sheet.row_values(0)

R = {header[j]:sheet.col_values(j,1) for j in range(len(header))}


l=R['country_80']

l=[s.strip() if s!='Amsterdam' else 'Netherlands' for s in l]

print('\n'.join(['%s\t%i' % (s[0] if s[0]!='' else '<empty>',s[1]) for s in sorted([[x,l.count(x)] for x in set(l)],key=lambda a:-a[1])]))
# %

print('\n\n\nCountries:')
print('\n'.join(['%s' % (s[0] if s[0]!='' else '<empty>') for s in sorted([[x,l.count(x)] for x in set(l)],key=lambda a:-a[1])]))

# %%

cities = R['city_31']
cities = [cities[j].strip().capitalize() for j in range(len(cities)) if R['country_80'][j].strip() in ['Russia','Россия'] ]

print('\n\n\nCities:')
print('\n'.join(sorted(list(set(cities)))))
# %
""" """
#
