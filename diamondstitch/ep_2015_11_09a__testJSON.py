# -*- coding: utf-8 -*-
"""
Created on Mon Nov 09 11:17:37 2015

@author: ep
"""

SS = """
-	dmc	3064	1119	 
+	dmc	758	977	 
*	dmc	3779	905	 
=	dmc	3859	805	 
/	dmc	819	777	 
\	dmc	3865	754	 
|	dmc	3772	613	 
#	dmc	225	609	 
@	dmc	407	563	 
$	dmc	543	379	 
^	dmc	779	359	 
}	dmc	3371	345	 
◄	dmc	938	300	 
►	dmc	839	300	 
?	dmc	3790	266	 
≡	dmc	3042	242	 
•	dmc	632	215	 
♦	dmc	898	197	 
◊	dmc	3862	160	 
★	dmc	223	99	 
"""

inch_mm = 25.4


AA = array([s.split('\t') for s in SS.split('\n')[1:-1]])
import json

with open('src\export3.php') as f:
    R = f.read()

D = json.loads(R)

data = array(D['data'])#[0:30,0:45]
colors = D['colors']

fontK = 0.35
fontsize = 5
maxH = 10

W = maxH*1.0*shape(data)[1]/shape(data)[0]

cimg = np.zeros((shape(data)[0],shape(data)[1],3))
for j in range(shape(data)[0]):
    for i in range(shape(data)[1]):
        trgb = [ord(c) for c in colors[data[j,i]]['rgb'][1:].decode('hex')]
        for k in range(3):
            cimg[j,i,k] = trgb[k]*1.0/255.0
fig=plt.figure(facecolor='white',figsize=(W,maxH))     
ax = fig.add_axes([0,0,1,1])       
plt.imshow(cimg,interpolation = 'nearest')
plt.axis('off')
codes = [chr(j) for j in range(ord('A'),ord('A')+100)]
#codes = ['A','B','C','D','E','F','G','H','J','K','L','M','N','O','P','Q','R','S','T']
codes = ['X','O','I','#','<','>','?','-','+','=','&','~','A','B','C','D','E','F','G','H','J','K','L','M','N','P','S','T','U','W','Y','Z','2','3','4','5','6','7','8']
for j in range(shape(data)[0]):
    for i in range(shape(data)[1]):
        plt.text(i-fontK,j+fontK,codes[data[j,i]][0],fontsize=fontsize,family='monospace',color='white' if sum(cimg[j,i,:])<1.5 else 'black' )#,bbox=dict(facecolor=colors[data[j,i]]['rgb'], alpha=1))
        #plt.text(i,j,codes[data[j,i]][0],family='monospace',bbox=dict(facecolor=colors[data[j,i]]['rgb'], alpha=1))
#xlim([0,shape(data)[1]])        
#ylim([0,shape(data)[0]])
#plt.gca().invert_xaxis()        
#plt.gca().invert_yaxis() 

#aaa
print('saving..')
temprand = np.random.randint(1000)

print('temprand=%i'%temprand)
#rdpi = (hdpi*maxH*1.0/shape(data)[0])*( inch_mm/2.5)
#hdpi = rdpi * (shape(data)[0]*1.0/maxH) / (inch_mm/2.5)

#for dpi in [80,120,160,240,400,1200]:
#for dpi in [240,400,600,800]:
#    print('td%i'%dpi)
#    plt.savefig('temp_r%04i_td%i_dpi=%i.png'%(temprand,dpi,(dpi*maxH*1.0/shape(data)[0])*(1.0/2.5)*(1/inch_mm)),dpi=dpi)        


for rdpi in [90,150,300,400,600,800]:
    dpi = int(round(rdpi * (shape(data)[0]*1.0/maxH) / (inch_mm/2.5)))
    print('rdpi = % 3i\tdpi = %i'%(rdpi,dpi))
    plt.savefig('temp_r%04i_td%i_dpi=%i.png'%(temprand,dpi,rdpi),dpi=dpi)        
""" """
#