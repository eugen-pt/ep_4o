import re
import xlrd


# %%

from common import date_match, str2mark, str2mark_int, shorten_subj


def datestr_is_1sem(datestr):
    month = int(datestr.split("-")[1])
    return (month>=11) or (month <=3)
# %%

book = xlrd.open_workbook("src/Список сотрудников на 21.05.2024.xls", encoding_override="cp1251")
print("The number of worksheets is {0}".format(book.nsheets))
print("Worksheet name(s): {0}".format(book.sheet_names()))
sh = book.sheet_by_index(0)

# %%

print("{0} {1} {2}".format(sh.name, sh.nrows, sh.ncols))
print("Cell D30 is {0}".format(sh.cell_value(rowx=29, colx=3)))

cur_dep = None
cur_cell_date_type = {}

rx=9
In_Header = {cj: sh.cell_value(rx,cj) for cj in range(sh.ncols) if sh.cell_value(rx,cj)!=''}
OUT_Structs = []

for rx in range(10, sh.nrows):
    R1 = sh.cell_value(rx,0)

    print(f"{rx} |{sh.cell_value(rx,0)}| |{R1}| |{R1=='№'}|")

    if R1=='№':
        print('!!!')

    try:
        int(R1)
    except:
        cur_dep = R1
        continue

    R = {}
    for cj in In_Header:
        v = sh.cell_value(rx, cj)
        R[In_Header[cj]] = v
    R['Подразделение'] = cur_dep
    OUT_Structs.append(R)

# %% Process all
for R in OUT_Structs:
    R["subjKey"] = R["ФИО"]
all_subjKey = sorted(set([R["subjKey"] for R in OUT_Structs]))
all_subjKey_id = {all_subjKey[j]:j for j in range(len(all_subjKey))}

# %%

for R in OUT_Structs:
    R["ФИО"] = R["ФИО"].strip()
    t = R["ФИО"]
    if '/' in t:
        t = t.split('/')[0]
    t = t.split(" ")
    R["ФИО_short"] = " ".join([t[0]] + [s[0]+"." for s in t[1:] if len(s)>0])


fio_short_2_fio = {}
for R in OUT_Structs:
    if R["ФИО_short"] not in fio_short_2_fio:
        fio_short_2_fio[R["ФИО_short"]] = {}
    fio_short_2_fio[R["ФИО_short"]][R["ФИО"]] = 1

for k in fio_short_2_fio:
    fio_short_2_fio[k] = list(fio_short_2_fio[k].keys())
# %%

OUT_Header = [
    "RowId", "ФИО", "Подразделение", "Должность", "Вид занятости"
]


rowid=0
for R in OUT_Structs:
    R["ФИО"] = R["ФИО_short"]
    if len(fio_short_2_fio[R["ФИО_short"]])>1:
        R["ФИО"] = R["ФИО"] + " (" + str(all_subjKey_id[R["subjKey"]]) + ')'


    rowid = rowid + 1
    R["RowId"] = rowid


# %%

OUT = [{OUT_Header[j]:OUT_Header[j] for j in range(len(OUT_Header))}] + OUT_Structs

# %%

import xlsxwriter

N_total = len(OUT_Structs)
missing_keys_n = {k:0 for k in OUT_Header}

wb = xlsxwriter.Workbook("spisok_sotr.xlsx")
ws = wb.add_worksheet("spisok_sotr")
for rj in range(len(OUT)):
    for cj in range(len(OUT_Header)):
        if OUT_Header[cj] in OUT[rj]:
            ws.write(rj,cj,OUT[rj][OUT_Header[cj]])
        else:
            missing_keys_n[OUT_Header[cj]] = missing_keys_n[OUT_Header[cj]] + 1
            #print(f"Missing key {OUT_Header[cj]}")
wb.close()
print("Saved")

for k in missing_keys_n:
    if missing_keys_n[k] > 0:
        print(f"Key {k} missing in {missing_keys_n[k]}/{N_total}")

# %%