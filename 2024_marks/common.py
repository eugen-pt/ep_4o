
import re

# %%

def date_match(v):
    r = re.match(r'([0-9]{2})\.([0-9]{2})\.([0-9]{2})', v)
    if r:
        r = r.groups()
        return '20'+r[2]+'-'+r[1]+'-'+r[0]
    return r

def str2mark(s):
    if type(s) is not str:
        return s
    part = 0
    if '-' in s:
        part = -0.33
    if '+' in s:
        part = 0.33
    if 'Деление на 0' in s:
        return None
    if 'н' in s:
        return 2
    if 'д' == s:
    	return None
    if 'у' == s:
    	return 2
    if len(s)==0:
        return None

    return int(s.replace('-','').replace('+','')) + part

def str2mark_int(s):
    v = str2mark(s)
    if v is None:
        return None
    return round(v)

def shorten_subj(s):
    for js in ['I','II','III','IV','V']:
        for pfx in [' - ',' ']:
            ts = pfx+js
            if (s[-len(ts):] == ts):
                return s[:-len(ts)]
    return s

# %%