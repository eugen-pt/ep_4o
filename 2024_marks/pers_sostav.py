import re
import xlrd


# %%

from common import date_match, str2mark, str2mark_int, shorten_subj


def datestr_is_1sem(datestr):
    month = int(datestr.split("-")[1])
    return (month>=11) or (month <=3)
# %%

book = xlrd.open_workbook("src/Персональный состав педагогических работников образовательной программы.xls", encoding_override="cp1251")
print("The number of worksheets is {0}".format(book.nsheets))
print("Worksheet name(s): {0}".format(book.sheet_names()))
sh = book.sheet_by_index(0)

# %%

print("{0} {1} {2}".format(sh.name, sh.nrows, sh.ncols))
print("Cell D30 is {0}".format(sh.cell_value(rowx=29, colx=3)))

cur_subj = None
cur_cell_date_type = {}

In_Header = ["ID", "Кафедра", "Наименования_исх", "ФИО", "Должность_ст_зв_исх", "Занятость", "Осн_образование_исх", "Доп_образование_исх", "Общий стаж", "Педагогический стаж"]
OUT_Structs = []

for rx in range(1,sh.nrows):
    R = {}
    for cj in range(len(In_Header)):
        v = sh.cell_value(rx, cj)
        R[In_Header[cj]] = v
    OUT_Structs.append(R)

# %% Process all
for R in OUT_Structs:
    R["subjKey"] = R["ФИО"]+R["Кафедра"]
all_subjKey = sorted(set([R["subjKey"] for R in OUT_Structs]))
all_subjKey_id = {all_subjKey[j]:j for j in range(len(all_subjKey))}


# %%

OUT_Header = In_Header + [
    "Предмет", "Семинары", "Лекции", "Лабораторные работы", "Спецкурс", 
    "Должность", "Ученая степень полная", "Ученая степень", "Ученое звание", 
    "Образование - уровень", "Образование - специальность", "Последнее доп. образование"]


subjs = {"нглийский язык": "Английский язык", "стория": "История", "Обществознание": "Обществознание", "нформатика":"Информатика", "биология": "Биология", "имия":"Химия", "изика":"Физика", "атематика":"Математика", 'Основы безопасности жизнедеятельности':'ОБЖ', 'Русский язык':'Русский язык','итература':'Литература' , "Уроки физической культуры":"Физ. культура"}

steps = ["Кандидат","Доктор","Отсутствует"]

rowid=0
for R in OUT_Structs:
    for subj_key in subjs:
        if subj_key in R["Наименования_исх"]:
            R["Предмет"] = subjs[subj_key]
            break
    
    print(R)

    R["Лекции"] = 'Да' if any(a in R["Наименования_исх"] for a in ['лекции','екционные']) else 'Нет'
    R["Лабораторные работы"] = 'Да' if any(a in R["Наименования_исх"] for a in ['абораторные']) else 'Нет'
    R["Спецкурс"] = 'Да' if any(a in R["Наименования_исх"] for a in ['пецкур','с/курс']) else 'Нет'
    R["Семинары"] = 'Да' if (any(a in R["Наименования_исх"] for a in ['еминар']) or all(R[k]=='Нет' for k in ["Лекции", "Лабораторные работы", "Спецкурс"])) else 'Нет'


    R["Последнее доп. образование"] = "неизв."
    dates = re.findall(r'[0-9]{2}\.[0-9]{2}\.[0-9]{4}',R['Доп_образование_исх'])
    if len(dates)>0:
        dates = ['-'.join(d.split('.')[::-1]) for d in dates]
        R["Последнее доп. образование"] = '.'.join(max(dates).split('-')[::-1])

    t = R['Должность_ст_зв_исх'].split('\n')
    if len(t)==1:
        t = R['Должность_ст_зв_исх'].split(', ')
    R["Должность"] = t[0].replace('Страший','Старший').replace('Доценкт','Доцент').replace(' реподаватель',' преподаватель').strip()
    if len(t)==2:
        if t[1]=='Кандидат физико-математических наук Ученое звание отсутствует':
            t[1] = 'Кандидат физико-математических наук'
            t[2] = t.append('Ученое звание отсутствует')
        elif len(t[1].split(', '))==2:
            t.append('')
            (t[1],t[2]) = t[1].split(', ')
        else:
            raise ValueError()
    R["Ученая степень полная"] = t[1]
    R["Ученое звание"] = t[2]


    if R["Ученая степень полная"]=="Ученая степень отсутствуе":
        R["Ученая степень полная"]="Ученая степень отсутствует"
    for s in steps:
        if s.lower() in R["Ученая степень полная"].lower():
            R["Ученая степень"] = s
            break

    t = R["Осн_образование_исх"].split(' - ')[1].split(', ')

    R["Образование - уровень"] = ', '.join(t[:-1])
    R["Образование - специальность"] = t[-1]


    if '/' in R["ФИО"]:
        R["ФИО"] = R["ФИО"].split('/')[0]
    t = R["ФИО"].split(" ")
    R["ФИО"] = " ".join([t[0]] + [s[0]+"." for s in t[1:] if len(s)>0])

    rowid = rowid + 1
    R["RowId"] = rowid


# %%

for R in OUT_Structs:
    if "Предмет" not in R and (R["Семинары"]=="Да"):
        print(R)

for R in OUT_Structs:
    if "Последнее доп. образование" not in R:
        print(R["Доп_образование_исх"])

for R in OUT_Structs:
    if "Ученая степень" not in R:
        print(R["Ученая степень полная"])

# %%

OUT = [{OUT_Header[j]:OUT_Header[j] for j in range(len(OUT_Header))}] + OUT_Structs

# %%

import xlsxwriter

N_total = len(OUT_Structs)
missing_keys_n = {k:0 for k in OUT_Header}

wb = xlsxwriter.Workbook("pers_sostav.xlsx")
ws = wb.add_worksheet("pers_sostav")
for rj in range(len(OUT)):
    for cj in range(len(OUT_Header)):
        if OUT_Header[cj] in OUT[rj]:
            ws.write(rj,cj,OUT[rj][OUT_Header[cj]])
        else:
            missing_keys_n[OUT_Header[cj]] = missing_keys_n[OUT_Header[cj]] + 1
            #print(f"Missing key {OUT_Header[cj]}")
wb.close()
print("Saved")

for k in missing_keys_n:
    if missing_keys_n[k] > 0:
        print(f"Key {k} missing in {missing_keys_n[k]}/{N_total}")

# %%