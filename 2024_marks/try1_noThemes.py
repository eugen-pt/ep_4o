import re
import xlrd


# %%

from common import date_match, str2mark, str2mark_int, shorten_subj


def datestr_is_1sem(datestr):
    month = int(datestr.split('-')[1])
    return (month>=11) or (month <=3)
# %%

#book = xlrd.open_workbook("src/Для аналитики 8.xls", encoding_override="cp1251")
book = xlrd.open_workbook(r"C:\Users\ep\Downloads\Для аналитики 9.xls", encoding_override="cp1251")

print("The number of worksheets is {0}".format(book.nsheets))
print("Worksheet name(s): {0}".format(book.sheet_names()))
sh = book.sheet_by_index(0)

# %%

print("{0} {1} {2}".format(sh.name, sh.nrows, sh.ncols))
print("Cell D30 is {0}".format(sh.cell_value(rowx=29, colx=3)))

cur_subj = None
cur_cell_date_type = {}

OUT = [ ['Name', 'Group', 'subgroup', 'Subject', 'Professor', 'Type', 'Date', 'Theme', 'Mark', 'Mark_base', 'Mark_string']]
OUT_Header = ['Ученик', 'Класс', 'Подгруппа', 'Класс-сорт', 'Группа-сорт', 'Подгруппа-сорт', 'Предмет', 'Предмет_исх', 'Преподаватель', 'Тип оценки', 'Дата', 'Оценка', 'Оценка(без+-)', 'Оценка(с+-)','Число пересдач','Оценка_до_пересдач(атт)']
OUT_Structs = []

MODE = None

for rx in range(sh.nrows+1):
    if(rx<sh.nrows):
        R1 = sh.cell_value(rx, 0)
        R2 = sh.cell_value(rx, 1)
        R3 = sh.cell_value(rx, 2)
        R4 = sh.cell_value(rx, 3)
    if(R1=='Дисциплина'):
        cur_subj = sh.cell_value(rx,1)
        cur_subj_sh = shorten_subj(cur_subj)
        print(f"Subject -> {cur_subj}")
        continue

    if(R2=='Физическое лицо'):
        cur_type = '?'
        cur_cell_date_type = {}
        row = sh.row(rx)
        cur_cj_header = {cj: row[cj].value for cj in range(len(row)) if len(row[cj].value)>0}

        MODE = 'marks_table'
        print('Starting marks_table for %s' % cur_subj)
        print('header:')
        print(cur_cj_header)
        cur_MARK_ROWS = []
        continue

    if (len(R2)==0) or (rx==sh.nrows):
        if (MODE=='marks_table') and (len(cur_MARK_ROWS)==0):
            continue
        print(f'Ending {MODE} for {cur_subj}')


        if MODE=='marks_table':
            for cx in cur_cj_header:
                v = cur_cj_header[cx]
                if len(v)==0:
                    continue
                tm = date_match(v)
                if(tm):
                    cur_cj_header[cx] = {'Дата':tm, 'Тип оценки':cur_type}

                else:
                    cur_type = v

                attestat_cx = [
                    cx for cx in cur_cj_header 
                    if type(cur_cj_header[cx]) is dict 
                    and (cur_cj_header[cx]['Тип оценки']=='Аттестация')
                ]
                attestat_cx_sem = {
                    1:[cx for cx in attestat_cx if datestr_is_1sem(cur_cj_header[cx]['Дата'])],
                    2:[cx for cx in attestat_cx if not datestr_is_1sem(cur_cj_header[cx]['Дата'])]
                }

                attestat_cx_sem_dateRevOrd = {
                    sem:sorted(attestat_cx_sem[sem], key=lambda cx:cur_cj_header[cx]['Дата'])[::-1]
                    for sem in attestat_cx_sem
                }

            for row in cur_MARK_ROWS:
                R_base = {}
                for cj in cur_cj_header:
                    if type(cur_cj_header[cj]) is not dict:
                        R_base[cur_cj_header[cj]] = row[cj].value

                for cj in cur_cj_header:
                    v = row[cj].value
                    if type(cur_cj_header[cj]) is dict:
                        R = {k:R_base[k] for k in R_base}
                        for key in cur_cj_header[cj]:
                            R[key] = cur_cj_header[cj][key]
                        R['Оценка_исх'] = v
                        R['Предмет_исх'] = cur_subj
                        R['Предмет'] = cur_subj_sh

                        OUT_Structs.append(R)

                for sem in attestat_cx_sem_dateRevOrd:
                    for cxj in range(len(attestat_cx_sem_dateRevOrd[sem])):
                        cx = attestat_cx_sem_dateRevOrd[sem][cxj]

                        v = row[cx].value
                        if len(v)>0:
                            # we have a mark
                            R = {k:R_base[k] for k in R_base}

                            R['Тип оценки'] = f'Аттестация итог {sem}сем.'
                            R['Дата'] = cur_cj_header[cx]['Дата']

                            R['Оценка_исх'] = v
                            R['Предмет_исх'] = cur_subj
                            R['Предмет'] = cur_subj_sh

                            R['Число пересдач'] = len(attestat_cx_sem_dateRevOrd[sem]) - cxj - 1

                            R['Оценка_до_пересдач(атт)'] = str2mark_int(row[attestat_cx_sem_dateRevOrd[sem][-1]].value)

                            OUT_Structs.append(R)
                            # we only need one last mark
                            break
        
        cur_MARK_ROWS = []

                    
        MODE = None
        continue

    if MODE == 'marks_table':
        print('marks_table!')
        if len(R4)==0: # no professor
            continue
        cur_MARK_ROWS.append(sh.row(rx))


# %% Process all
for R in OUT_Structs:
    R['subjKey'] = R['Физическое лицо']+R['Учебная группа']+R['Учебная подгруппа']

all_subjKey = sorted(set([R['subjKey'] for R in OUT_Structs]))
all_subjKey_id = {all_subjKey[j]:j for j in range(len(all_subjKey))}


rowid=0
for R in OUT_Structs:
    R['Группа'] = R['Учебная группа']
    R['Класс_исх'] = R['Группа']
    R['Подгруппа'] = R['Учебная подгруппа']

    R['Класс'] = R['Группа'][3:5]
    R['Класс-сорт'] = R['Класс'] + '-' + R['Группа'][5:] + ' (' + R['Группа'][1:3] + ')'
    R['Подгруппа-сорт'] = R['Класс'] + '-' + R['Группа'][5:] + '/' + R['Учебная подгруппа'][:1] + ' (' + R['Группа'][1:3] + ')'
    R['Группа-сорт'] = R['Подгруппа-сорт']

    R['Ученик'] = ''.join([s[0] for s in R['Физическое лицо'].split(' ')]) + str(all_subjKey_id[R['subjKey']])

    R['Оценка'] = str2mark(R['Оценка_исх'])
    R['Оценка(без+-)'] = str2mark_int(R['Оценка_исх'])
    R['Оценка(с+-)'] = R['Оценка_исх']

    R['Класс-Тип оценки'] = R['Класс']+'-'+R['Тип оценки']

    t = R['Преподаватель'].split(' ')
    R['Преподаватель'] = ' '.join([t[0]] + [s[0]+'.' for s in t[1:]])

    rowid = rowid + 1
    R['RowId'] = rowid

# %%

OUT = [{OUT_Header[j]:OUT_Header[j] for j in range(len(OUT_Header))}] + OUT_Structs

# %%

import xlsxwriter

N_total = len(OUT_Structs)
missing_keys_n = {k:0 for k in OUT_Header}

wb = xlsxwriter.Workbook('out9.xlsx')
ws = wb.add_worksheet('Attestat')
for rj in range(len(OUT)):
    for cj in range(len(OUT_Header)):
        if OUT_Header[cj] in OUT[rj]:
            ws.write(rj,cj,OUT[rj][OUT_Header[cj]])
        else:
            missing_keys_n[OUT_Header[cj]] = missing_keys_n[OUT_Header[cj]] + 1
            #print(f"Missing key {OUT_Header[cj]}")
wb.close()
print('Saved')

for k in missing_keys_n:
    if missing_keys_n[k] > 0:
        print(f"Key {k} missing in {missing_keys_n[k]}/{N_total}")

# %%