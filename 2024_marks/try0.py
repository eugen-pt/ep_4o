import re
import xlrd


# %%

def date_match(v):
    return re.match(r'([0-9]{2})\.([0-9]{2})\.([0-9]{2})', v)

def str2mark(s):
    part = 0
    if '-' in s:
        part = -0.33
    if '+' in s:
        part = 0.33
    if 'на' in s:
        return 2
    if len(s)==0:
        return None

    return int(s.replace('-','').replace('+','')) + part

# %%
book = xlrd.open_workbook("src/Для аналитики 7.xls", encoding_override="cp1251")
print("The number of worksheets is {0}".format(book.nsheets))
print("Worksheet name(s): {0}".format(book.sheet_names()))
sh = book.sheet_by_index(0)

# %%

print("{0} {1} {2}".format(sh.name, sh.nrows, sh.ncols))
print("Cell D30 is {0}".format(sh.cell_value(rowx=29, colx=3)))

cur_subj = None
cur_cell_date_type = {}

OUT = [ ['Name', 'Group', 'subgroup', 'Subject', 'Professor', 'Type', 'Date', 'Theme', 'Mark', 'Mark_base', 'Mark_string']]
OUT = [ ['Ученик', 'Класс', 'Подгруппа', 'Предмет', 'Преподаватель', 'Тип оценки', 'Дата', 'Тема', 'Оценка', 'Оценка(без+-)', 'Оценка(с+-)']]

MODE = None

for rx in range(sh.nrows+1):
    if(rx<sh.nrows):
        R1 = sh.cell_value(rx, 0)
        R2 = sh.cell_value(rx, 1)
        R3 = sh.cell_value(rx, 2)
        R4 = sh.cell_value(rx, 3)
    if(R1=='Дисциплина'):
        cur_subj = sh.cell_value(rx,1)
        print(cur_subj)
        continue

    if(R2=='Физическое лицо'):
        cur_type = '?'
        cur_cell_date_type = {}
        for cx in range(4, sh.ncols):
            v = sh.cell_value(rx, cx)
            if len(v)==0:
                continue
            tm = date_match(v)
            if(tm):
                cur_cell_date_type[cx] = [v, cur_type]
            else:
                cur_type = v
        MODE = 'marks_table'
        print('Starting marks_table for %s' % cur_subj)
        cur_MARK_ROWS = []
        continue

    if R2=='Нагрузка':
        MODE = 'theme_table'
        cur_date_type_THEMES = {}
        print('Starting themes for %s' % cur_subj)
        continue

    if (len(R2)==0) or (rx==sh.nrows):
        if (MODE=='marks_table') and (len(cur_MARK_ROWS)==0):
            continue
        print(f'Ending {MODE} for {cur_subj}')
        if MODE=='theme_table':
            print('Parsing themes for %s' % cur_subj)
            #print(cur_cell_date_type)
            #print(cur_date_type_THEMES)
            for cx in cur_cell_date_type:
                date_ = cur_cell_date_type[cx][0]
                type_ = cur_cell_date_type[cx][1]
                if (
                    (date_ in cur_date_type_THEMES)
                    and (type_ in cur_date_type_THEMES[date_])
                    ):
                    cur_cell_date_type[cx].append( cur_date_type_THEMES[date_][type_] )
                else:
                    cur_cell_date_type[cx].append('--')
            #print(cur_cell_date_type)
            print(cur_MARK_ROWS)
            for jROW in cur_MARK_ROWS:
                
                table_row = jROW[1]
                for cx in cur_cell_date_type:

                    mark_s = table_row[cx].value
                    if len(mark_s)==0:
                        continue
                    
                    mark = str2mark(mark_s)

                    if mark is None:
                        continue

                    OUT_ROW = [j for j in jROW[0]]
                    OUT_ROW.append(cur_cell_date_type[cx][1])
                    OUT_ROW.append(cur_cell_date_type[cx][0])
                    OUT_ROW.append(cur_cell_date_type[cx][2])
                    OUT_ROW.append(mark)
                    OUT_ROW.append(round(mark))
                    OUT_ROW.append(mark_s)

                    OUT.append(OUT_ROW)
                    
        MODE = None
        continue

    if MODE == 'marks_table':
        print('marks_table!')
        if len(R4)==0:
            continue
        cur_MARK_ROWS.append([[R2, R3, R1, cur_subj, R4], sh.row(rx)])

    if MODE == 'theme_table':
        if R1 not in cur_date_type_THEMES:
            cur_date_type_THEMES[R1] = {}
        if (R2 in cur_date_type_THEMES[R1]) and (len(cur_date_type_THEMES[R1][R2]) > len(R3)):
            continue
        cur_date_type_THEMES[R1][R2] = R3
    #print(sh.row(rx))

# %%

import xlwt

wb = xlwt.Workbook()
ws = wb.add_sheet('Account List')
for rj in range(len(OUT)):
    for cj in range(len(OUT[rj])):
        ws.write(rj,cj,OUT[rj][cj])
wb.save('out.xls')
print('Saved')


# %%