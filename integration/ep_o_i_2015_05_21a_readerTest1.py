# -*- coding: utf-8 -*-
"""
Created on Thu May 21 10:19:15 2015

@author: ep
"""

fname = 'output_2015_05_17a.txt'
fname = 'output_2015_04_29e.txt'

with open(fname,'r') as f:
    LS = f.readlines()
    
LS = [s.replace('\r\n','').strip() for s in LS]    

class integTest:
    def __init__(self):
        self.bigMx = []
        self.testMx = []
        self.testMx_ixs = []
        self.inhMx = []
        self.inhMx_ixs = []
        
        self.inhibitions = []
        self.identifications = []
        self.minimizes = []
        
        self.min_tests = [] #test scores
        self.min_coefs = []
        self.min_trains = [] # train scores
        

def readMatrix(lj):
    mx = [map(int,LS[lj].split(' '))]
    for j in range(1,len(mx[0])):
        lj=lj+1
        mx.append(map(int,LS[lj].split(' ')))
    return (array(mx),lj)    
   
LOG_types = []
LOG_objs = []   
LOG_startLJs = []
   
LJ=15244
LJ=0
while(LJ<len(LS)):
    LOG_startLJs.append(LJ)
    L = LS[LJ]
#    while()
    Ls = L.split()
#    print(Ls)
    if(Ls[0]=='0' or Ls[0]=='1'):
        htype = 'mx'
        hobj,LJ = readMatrix(LJ)
        
    elif(Ls[0]=='indexes'):
        htype = 'indexes'
        L = L.replace(']','').replace('[','').replace(',','')
        Ls = L.split()
        hobj = map(int,Ls[2:])
        if(len(hobj)==0): # empty matrix as far as the indexes say -  needs to be logged by hand
            LOG_types.append(htype)
            LOG_objs.append(hobj)  
            LOG_startLJs.append(LJ)
            htype = 'mx'
            hobj = []
        else:  
            pass
    elif(Ls[0]=='inhibition'):
        htype = 'inhibition'
        hobj = array(map(float,Ls[2:]))
    elif(Ls[0]=='identification'):
        htype = 'identification'
        hobj = map(int,Ls[2:])
    elif(Ls[0]=='minimize'):
        htype = 'minimize'
        hobj = array(map(float,Ls[2:]))
    else:
        print(LJ)
        print(Ls)
        raise ValueError('??')
    LOG_types.append(htype)
    LOG_objs.append(hobj)  
    LJ=LJ+1
        
print(len(LOG_types))
have_ixs = 'indexes' in LOG_types

dLOG_types = {j:LOG_types[j] for j in range(len(LOG_types))}
dLOG_objs = {j:LOG_objs[j] for j in range(len(LOG_objs))}
dLOG_startLJs = {j:LOG_startLJs[j] for j in range(len(LOG_startLJs))}

#aaa
fordel = []

""" """


for j in range(len(dLOG_types)-1):
    if(dLOG_types[j] == 'mx')and (have_ixs==0):
        n=1
        i=j+1
        while(i in dLOG_types)and(dLOG_types[i]=='mx'):
            n=n+1
            i=i+1
        if(n>3):
            for i in range(n-3):
               fordel.append(j+i) 
    else:
        if(dLOG_types[j]==dLOG_types[j+1]):
            fordel.append(j)

""" """

print(len(fordel))

for j in fordel:
    try:
        del dLOG_types[j]
        del dLOG_objs[j]
        del dLOG_startLJs[j]
    except:
        pass

""" """
tix = sort(dLOG_types.keys())

LOG_types = [dLOG_types[j] for j in tix]
LOG_objs = [dLOG_objs[j] for j in tix]
LOG_startLJs = [dLOG_startLJs[j] for j in tix]

""" """

print('second lap..')

dLOG_types = {j:LOG_types[j] for j in range(len(LOG_types))}
dLOG_objs = {j:LOG_objs[j] for j in range(len(LOG_objs))}
dLOG_startLJs = {j:LOG_startLJs[j] for j in range(len(LOG_startLJs))}

fordel=[]

if(have_ixs):
    for j in range(len(dLOG_types)-1):
        if(dLOG_types[j] == 'mx')and   (dLOG_types[j+1] == 'indexes'):
             i=j+2
             n=1
             while(i<len(dLOG_types)-1) and(dLOG_types[i] == 'mx')and   (dLOG_types[i+1] == 'indexes'):
                 n=n+1
                 i=i+2
             if(n>2):
                 for t in range(2*(n-2)):
                     fordel.append(t+j)

""" """
print(len(fordel))
for j in fordel:
    print(j,dLOG_types[j],dLOG_objs[j])

for j in fordel:
    try:
        del dLOG_types[j]
        del dLOG_objs[j]
        del dLOG_startLJs[j]
    except:
        pass

""" """

tix = sort(dLOG_types.keys())

LOG_types = [dLOG_types[j] for j in tix]
LOG_objs = [dLOG_objs[j] for j in tix]
LOG_startLJs = [dLOG_startLJs[j] for j in tix]

print(len(LOG_types))

#aaa

RS = []

LJ=0
while(LJ<len(LOG_types)):
    R = integTest()
    if(LOG_types[LJ] != 'mx'):
        raise ValueError('not a mx to start from!')
        
    R.bigMx = LOG_objs[LJ]
    LJ=LJ+1
    if(LOG_types[LJ]=='indexes'):
        R.inhMx_ixs = LOG_objs[LJ]
        LJ=LJ+1
        if(LOG_types[LJ] != 'mx'):
            print(LJ)
            print(LS[LOG_startLJs[LJ-2]:LOG_startLJs[LJ+1]])
            print(LOG_objs[LJ-2:LJ+1])
            print(LOG_types[LJ-2:LJ+1])
            
            raise ValueError('not a mx to start from!')
        R.inhMx =  LOG_objs[LJ]  
        LJ=LJ+1
    elif(LOG_types[LJ]=='mx'):
        R.inhMx =  LOG_objs[LJ]  
        LJ=LJ+1
    else:
        print(LJ)
        print(LS[LOG_startLJs[LJ-4]:LOG_startLJs[LJ+1]])
        print(LOG_types[LJ-4:LJ+1])
        raise ValueError('not a mx or indexes to start a matrix')
        
    if(LOG_types[LJ]=='indexes'):
        R.testMx_ixs = LOG_objs[LJ]
        LJ=LJ+1
        if(LOG_types[LJ] != 'mx'):
            print(LJ)
            print(LS[LOG_startLJs[LJ-4]:LOG_startLJs[LJ+2]])
#            print(LOG_objs[LJ-4:LJ+1])
#            print(LOG_types[LJ-4:LJ+1])
            print({j:[LOG_types[LJ+j],LOG_objs[LJ+j]] for j in range(-4,1)})
            raise ValueError('not a mx to start from!')
        R.testMx =  LOG_objs[LJ]  
        LJ=LJ+1
    elif(LOG_types[LJ]=='mx'):
        R.testMx =  LOG_objs[LJ]  
        LJ=LJ+1
    else:
        print(LJ)
        print(LS[LOG_startLJs[LJ-4]:LOG_startLJs[LJ+1]])
        print(LOG_types[LJ-4:LJ+1])
        raise ValueError('not a mx or indexes to start a matrix')
    
    
    if(LJ>=len(LOG_types)):
        undone=1
        break
    while(LOG_types[LJ]!='mx'):
        if(LOG_types[LJ]=='inhibition') :
            R.inhibitions.append(LOG_objs[LJ])
        elif(LOG_types[LJ]=='identification') :   
            R.identifications.append(LOG_objs[LJ])
        elif(LOG_types[LJ]=='minimize') :   
            R.minimizes.append(LOG_objs[LJ])
            R.min_tests.append(LOG_objs[LJ][0])
            R.min_trains.append(LOG_objs[LJ][-1])
            R.min_coefs.append(LOG_objs[LJ][2:-1])
            
        else:
            print(LJ)
#            print(LS[LOG_startLJs[LJ-10]:LOG_startLJs[LJ+1]])
#            print(LOG_types[LJ-10:LJ+1])
            for j in range(-5,5):print(j,LOG_types[LJ+j],LOG_objs[LJ+j])
            raise ValueError('not inhibition, not identification,not minimize!')
        LJ=LJ+1
        if(LJ>=len(LOG_types)):
            undone=1
            break
    RS.append(R)     

R_meanTests = [mean(R.min_tests) for R in RS]
R_bigMXsize = [len(R.bigMx[0]) for R in RS]
R_inhMXsize = [len(R.inhMx[0]) for R in RS]
R_testMXsize = [len(R.testMx[0]) for R in RS]

""" """
#